var Vogue = (function () {
	'use strict';

	var classCallCheck = function (instance, Constructor) {
	  if (!(instance instanceof Constructor)) {
	    throw new TypeError("Cannot call a class as a function");
	  }
	};

	var createClass = function () {
	  function defineProperties(target, props) {
	    for (var i = 0; i < props.length; i++) {
	      var descriptor = props[i];
	      descriptor.enumerable = descriptor.enumerable || false;
	      descriptor.configurable = true;
	      if ("value" in descriptor) descriptor.writable = true;
	      Object.defineProperty(target, descriptor.key, descriptor);
	    }
	  }

	  return function (Constructor, protoProps, staticProps) {
	    if (protoProps) defineProperties(Constructor.prototype, protoProps);
	    if (staticProps) defineProperties(Constructor, staticProps);
	    return Constructor;
	  };
	}();

	var _class = function () {
	  function _class(theme) {
	    var _this = this;

	    classCallCheck(this, _class);

	    this.theme = theme;

	    $(document).on('shopify:section:load', function (event) {
	      return _this._addHeaderClasses();
	    });
	    $(document).on('shopify:section:load', function (event) {
	      return _this._homepage(event.target);
	    });
	    $(document).on('shopify:section:load', function (event) {
	      return _this._tagList(event.target);
	    });
	    $(document).on('shopify:section:load', function (event) {
	      return _this._blog(event.target, true);
	    });
	    $(document).on('shopify:section:unload', function (event) {
	      return _this._blog(event.target, false);
	    });
	    $(document).on('shopify:section:load', function (event) {
	      return _this._cart(event.target);
	    });
	    $(document).on('shopify:section:load', function (event) {
	      return _this._product(event.target, true);
	    });
	    $(document).on('shopify:section:unload', function (event) {
	      return _this._sideBar(event.target, false);
	    });
	    $(document).on('shopify:section:load', function (event) {
	      return _this._product(event.target, true);
	    });
	    $(document).on('shopify:section:unload', function (event) {
	      return _this._sideBar(event.target, false);
	    });
	  }

	  createClass(_class, [{
	    key: '_sideBar',
	    value: function _sideBar(section, load) {
	      if ($(section).attr('id') === 'shopify-section-header') {
	        if (load) {
	          this.theme.product.initCurrencyConverter();
	        } else {
	          this.theme.product.removeCurrencyConverter();
	        }
	      }
	    }
	  }, {
	    key: '_product',
	    value: function _product(section, load) {
	      if ($(section).attr('id') === 'shopify-section-page-product') {
	        if (load) {
	          this.theme.product.init();
	        } else {
	          this.theme.product.remove();
	        }
	      }
	    }
	  }, {
	    key: '_blog',
	    value: function _blog(section, load) {
	      if ($(section).attr('id') === 'shopify-section-page-blog') {
	        if (load) {
	          this.theme.blog.init();
	        } else {
	          this.theme.blog.remove();
	        }
	      }
	    }
	  }, {
	    key: '_homepage',
	    value: function _homepage(section) {
	      if ($(section).find('[data-home-section]').length) {
	        this.theme.home.updateSectionHeights();
	      }
	    }
	  }, {
	    key: '_cart',
	    value: function _cart(section) {
	      if ($(section).attr('id') === 'shopify-section-page-cart') {
	        this.theme.cart.init();
	      }
	    }
	  }, {
	    key: '_tagList',
	    value: function _tagList(section) {
	      if ($('.section-tags').length) {
	        this.theme.tagList.init();
	      }
	    }
	  }, {
	    key: '_addHeaderClasses',
	    value: function _addHeaderClasses() {
	      var hasSidebar = $('[data-header-sidebar]').data('header-sidebar');

	      $(document.body).toggleClass('sidebar-always-visible', hasSidebar);
	      $(document.body).toggleClass('sidebar-always-hidden', !hasSidebar);

	      if (window.Theme) Theme.hasSidebar = hasSidebar;
	    }
	  }]);
	  return _class;
	}();

	var index = debounce

	function debounce (fn, delay, at_start, guarantee) {
	  var timeout
	  var args
	  var self

	  return function debounced () {
	    self = this
	    args = Array.prototype.slice.call(arguments)

	    if (timeout && (at_start || guarantee)) {
	      return
	    } else if (!at_start) {
	      clear()

	      timeout = setTimeout(run, delay)
	      return timeout
	    }

	    timeout = setTimeout(clear, delay)
	    fn.apply(self, args)

	    function run () {
	      clear()
	      fn.apply(self, args)
	    }

	    function clear () {
	      clearTimeout(timeout)
	      timeout = null
	    }
	  }
	}

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var CurrencyConverter = function () {
	  function CurrencyConverter() {
	    _classCallCheck(this, CurrencyConverter);

	    /**
	     * @property {object} defaults                       - The default configuration for CurrencyConverter
	     * @property {string} defaults.switcherSelector      - CSS Selector for dropdown which controls currency conversion
	     * @property {string} defaults.priceSelector         - CSS Selector for elements containing prices
	     * @property {string} defaults.shopCurrency          - Shop's currency (Currency which prices are printed as)
	     * @property {string} defaults.defaultCurrency       - Theme's currency setting, or initial currency to show on the page
	     * @property {string} defaults.displayFormat         - `money_with_currency_format` or `money_format`
	     * @property {string} defaults.moneyFormat           - Shop's currency formatted using the display format
	     * @property {string} defaults.moneyFormatNoCurrency - Shop's currency formatted without showing currency
	     * @property {string} defaults.moneyFormatCurrency   - Shop's currency formatted showing currency
	     */
	    this.defaults = {
	      switcherSelector: '[data-currency-converter]',
	      priceSelector: '.money',
	      shopCurrency: '',
	      defaultCurrency: '',
	      displayFormat: '',
	      moneyFormat: '',
	      moneyFormatNoCurrency: '',
	      moneyFormatCurrency: ''
	    };

	    this.isInitialised = false;
	  }

	  _createClass(CurrencyConverter, [{
	    key: 'init',
	    value: function init(options) {
	      if (!window.Currency || this.isInitialised) return;

	      this.options = Object.assign({}, this.defaults, options);

	      window.Currency.format = this.options.displayFormat;
	      window.Currency.moneyFormats[this.options.shopCurrency].money_with_currency_format = this.options.moneyFormatCurrency;
	      window.Currency.moneyFormats[this.options.shopCurrency].money_format = this.options.moneyFormatNoCurrency;
	      window.Currency.currentCurrency = window.Currency.cookie.read() || this.options.defaultCurrency;

	      this.isInitialised = true;
	      this._current();
	    }

	    /**
	     * Change the currency to a new currency using an ISO currency code
	     *
	     * @param newCurrency
	     */

	  }, {
	    key: 'setCurrency',
	    value: function setCurrency(newCurrency) {
	      if (!this.isInitialised) return;

	      this._convertAll(window.Currency.currentCurrency, newCurrency);
	    }

	    /**
	     * Updates a price on the page to use the active Currency, and formatting
	     *
	     * @param priceEl {HTMLElement}
	     */

	  }, {
	    key: 'update',
	    value: function update(priceEl) {
	      if (!this.isInitialised) return;

	      var formattedPrice = window.Shopify.formatMoney(this._moneyValue(priceEl.innerHTML), this.options.moneyFormat);
	      var attributes = priceEl.attributes;

	      for (var attr = 0; attr < attributes.length; attr++) {
	        var attribute = attributes[attr];

	        if (attribute.name.indexOf('data-currency') === 0) {
	          priceEl.setAttribute(attribute.name, '');
	        }
	      }

	      priceEl.innerHTML = formattedPrice;
	      priceEl.removeAttribute('data-currency');
	      priceEl.setAttribute('data-currency-' + this.options.shopCurrency, formattedPrice);
	      priceEl.setAttribute('data-currency', this.options.shopCurrency);

	      this._convertEl(priceEl, this.options.shopCurrency, window.Currency.currentCurrency);
	    }

	    /**
	     * Update the currency switcher to the current currency
	     * @private
	     */

	  }, {
	    key: '_current',
	    value: function _current() {
	      var switchers = document.querySelectorAll(this.options.switcherSelector);
	      for (var i = 0; i < switchers.length; i++) {
	        var switcher = switchers[i];

	        var childrenEls = switcher.querySelectorAll('option');

	        for (var _i = 0; _i < childrenEls.length; _i++) {
	          var optionEl = childrenEls[_i];

	          if (optionEl.selected && optionEl.value !== window.Currency.currentCurrency) {
	            optionEl.selected = false;
	          }

	          if (optionEl.value === window.Currency.currentCurrency) {
	            optionEl.selected = true;
	          }
	        }
	      }

	      this._convertAll(this.options.shopCurrency, window.Currency.currentCurrency);
	    }

	    /**
	     * Converts formatted money to a number
	     *
	     * @param price {String}
	     * @returns {Number}
	     * @private
	     */

	  }, {
	    key: '_moneyValue',
	    value: function _moneyValue(price) {
	      return parseInt(price.replace(/[^0-9]/g, ''), 10);
	    }

	    /**
	     * Converts an individual price to the new format
	     *
	     * @param priceEl
	     * @param oldCurrency
	     * @param newCurrency
	     * @private
	     */

	  }, {
	    key: '_convertEl',
	    value: function _convertEl(priceEl, oldCurrency, newCurrency) {

	      // If the amount has already been converted, we leave it alone.
	      if (priceEl.getAttribute('data-currency') === newCurrency) {
	        return;
	      }

	      // If we are converting to a currency that we have saved, we will use the saved amount.
	      if (priceEl.getAttribute('data-currency-' + newCurrency)) {
	        priceEl.innerHTML = priceEl.getAttribute('data-currency-' + newCurrency);
	      } else {
	        // Converting to Y for the first time? Let's get to it!
	        var cents = 0.0;
	        var oldAmount = this._moneyValue(priceEl.innerHTML);
	        var oldFormat = window.Currency.moneyFormats[oldCurrency][this.options.displayFormat];

	        // Convert prices from float values to integers if needed, then convert
	        if (oldFormat.indexOf('amount_no_decimals') !== -1) {
	          cents = window.Currency.convert(oldAmount * 100, oldCurrency, newCurrency);
	        } else if (oldCurrency === 'JOD' || oldCurrency === 'KWD' || oldCurrency === 'BHD') {
	          cents = window.Currency.convert(oldAmount / 10, oldCurrency, newCurrency);
	        } else {
	          cents = window.Currency.convert(oldAmount, oldCurrency, newCurrency);
	        }

	        var newFormat = window.Currency.moneyFormats[newCurrency][this.options.displayFormat];
	        var newFormattedAmount = window.Currency.formatMoney(cents, newFormat);

	        priceEl.innerHTML = newFormattedAmount;
	        priceEl.setAttribute('data-currency-' + newCurrency, newFormattedAmount);
	      }

	      priceEl.setAttribute('data-currency', newCurrency);
	    }

	    /**
	     * Convert all prices on the page to the new currency
	     *
	     * @param oldCurrency
	     * @param newCurrency
	     * @private
	     */

	  }, {
	    key: '_convertAll',
	    value: function _convertAll(oldCurrency, newCurrency) {
	      var priceEls = document.querySelectorAll(this.options.priceSelector);
	      if (!priceEls) return;

	      window.Currency.currentCurrency = newCurrency;
	      window.Currency.cookie.write(newCurrency);

	      for (var i = 0; i < priceEls.length; i++) {
	        this._convertEl(priceEls[i], oldCurrency, newCurrency);
	      }
	    }
	  }]);

	  return CurrencyConverter;
	}();

	var CurrencyConverter$1 = new CurrencyConverter();

	var index$1 = CurrencyConverter$1;

	var PriceUpdater = function () {
	  function PriceUpdater() {
	    classCallCheck(this, PriceUpdater);

	    this.theme = window.Theme;
	    this.Shopify = window.Shopify;
	    this.Currency = window.Currency;

	    this.moneyFormat = this.theme.currency.moneyFormat;
	  }

	  createClass(PriceUpdater, [{
	    key: 'updatePrice',
	    value: function updatePrice($el, price) {
	      // Assign base shop based currency to data attributes and text
	      var formattedPrice = this.Shopify.formatMoney(price, this.moneyFormat);
	      $el.text(formattedPrice);

	      if (this.theme.currency.enable && this.Currency) {
	        index$1.update($el[0]);
	      }
	    }
	  }]);
	  return PriceUpdater;
	}();

	var Cart = function () {
	  function Cart() {
	    classCallCheck(this, Cart);

	    this.init();
	  }

	  createClass(Cart, [{
	    key: 'init',
	    value: function init() {
	      var _this = this;

	      this.theme = window.Theme;

	      var $shippingData = $('[data-shipping-calculator-data]');

	      if ($shippingData.length) {
	        try {
	          this.theme.shippingCalculator = JSON.parse($shippingData.text());
	        } catch (error) {}
	      }

	      this.Shopify = window.Shopify;

	      this.$cartForm = $('[data-cart-form]');
	      this.$shippingSubmit = $('[data-calculate-shipping]');
	      this.$cartSubtotal = $('[data-cart-subtotal]');

	      if (this.$cartForm.length) {
	        this._bindEvents();
	        this.PriceUpdater = new PriceUpdater();

	        if (this.theme.shippingCalculator && this.$shippingSubmit.length) {
	          this._initShipping();

	          this.Shopify.onError = function (errors) {
	            _this._handleErrors(errors);
	          };
	        }
	      }
	    }
	  }, {
	    key: '_bindEvents',
	    value: function _bindEvents() {
	      var _this2 = this;

	      $('[data-cart-item-increment]').on('click', function (event) {
	        _this2._adjustValue(event, 1);
	      });

	      $('[data-cart-item-decrement]').on('click', function (event) {
	        _this2._adjustValue(event, -1);
	      });

	      $('[data-product-quantity]').on('change', function (event) {
	        var itemData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	        event.preventDefault();

	        var item = itemData || _this2._getCartItem(event);

	        if (item.value > item.bounds.max) {
	          item.$input.val(item.bounds.max);
	          item.value = item.bounds.max;
	          _this2._alert(Theme.cart.itemStock.replace('{stock}', item.bounds.max));
	        }

	        _this2._updateItem(item);
	      });

	      $('[data-cart-notes]').on('change', index(function (event) {
	        var note = $(event.currentTarget).val();
	        _this2.Shopify.updateCartNote(note, function () {
	          // Without an empty call back, Shopify returns the amount of products
	          // that are present in the cart. It checks against it being a function.
	          return true;
	        });
	      }, 200));
	    }

	    /**
	     * Retrieve information about the cart item
	     *
	     * @param event
	     * @returns {Object}
	     *
	     */

	  }, {
	    key: '_getCartItem',
	    value: function _getCartItem(event) {
	      var $target = $(event.currentTarget);
	      var $productRow = $target.parents('[data-cart-item-row]');
	      var $input = $productRow.find('[data-product-quantity]');
	      var originalValue = parseInt($input.val(), 10);

	      return {
	        bounds: this._getBounds($input),
	        $target: $target,
	        $productRow: $productRow,
	        productTitle: $productRow.find('[data-title]').text().trim(),
	        $productPrice: $productRow.find('[data-item-price]'),
	        lineItem: $productRow.data('cart-item-index'),
	        $input: $input,
	        value: !isNaN(originalValue) ? originalValue : 1
	      };
	    }

	    /**
	     * Update cart quantities on the server level to make them persistent
	     *
	     * @param event
	     * @param adjustment
	     * @private
	     */

	  }, {
	    key: '_adjustValue',
	    value: function _adjustValue(event, adjustment) {
	      event.preventDefault();

	      var item = this._getCartItem(event);
	      var newQuantity = adjustment + item.value;
	      newQuantity = Math.max(newQuantity, item.bounds.min);
	      item.value = Math.min(newQuantity, item.bounds.max);

	      item.$input.val(item.value).trigger('change', item);
	    }

	    /**
	     * Sends a request to the server to update a single product
	     *
	     * @param item
	     * @private
	     */

	  }, {
	    key: '_updateItem',
	    value: function _updateItem(item) {
	      var _this3 = this;

	      $.ajax({
	        type: 'POST',
	        url: '/cart/change.js',
	        dataType: 'json',
	        data: {
	          quantity: item.value,
	          line: item.lineItem
	        },
	        success: function success(response) {
	          // TODO: Add a callback to give the client a visual that its been updated?
	          var cartTotal = response.total_price;
	          var prices = [];

	          prices.push({
	            label: _this3.$cartSubtotal,
	            price: cartTotal
	          });

	          if (item.value === 0) {
	            _this3._removeItem(item.lineItem, item.productTitle);
	          } else {
	            var itemTotal = response.items[item.lineItem - 1].line_price;
	            prices.push({
	              label: item.$productPrice,
	              price: itemTotal
	            });
	          }

	          _this3._updatePrice(prices);
	        }
	      });
	    }
	  }, {
	    key: '_updatePrice',
	    value: function _updatePrice(prices) {
	      for (var i = 0; i < prices.length; i++) {
	        var $label = prices[i].label;
	        this.PriceUpdater.updatePrice($label, prices[i].price);
	      }
	    }

	    /**
	     * Determine the range of available inventory that can be purchase
	     *
	     * @param $input
	     * @returns {Object}
	     *          Minimum and maximum range of the product that can be purchased
	     */

	  }, {
	    key: '_getBounds',
	    value: function _getBounds($input) {
	      var min = 0;

	      var max = $input.attr('data-product-inventory-max') ? parseInt($input.data('product-inventory-max'), 10) : Infinity;

	      return { min: min, max: max };
	    }

	    /**
	     * Remove an item from the DOM after it has been removed from the cart,
	     * and recalculate line item indexes.
	     *
	     * @param lineItem
	     * @param productTitle
	     */

	  }, {
	    key: '_removeItem',
	    value: function _removeItem(lineItem, productTitle) {
	      $('[data-cart-item-index=' + lineItem + ']').remove();
	      this._alert(Theme.cart.itemRemoved.replace('{productTitle}', productTitle));

	      // Recalculate cart row indexes
	      var $cartItems = $('[data-cart-item-row]');
	      for (var i = 0; i < $cartItems.length; i++) {
	        $cartItems.eq(i).attr('data-cart-item-index', i + 1);
	      }
	    }
	  }, {
	    key: '_initShipping',
	    value: function _initShipping() {
	      var _this4 = this;

	      this.$shippingResponse = $('[data-shipping-response]');
	      this.$shippingResponseMessage = $('[data-shipping-response-message]');
	      this.$shippingResponseRates = $('[data-shipping-response-rates]');

	      this.$shippingSubmit.on('click', function (event) {
	        event.preventDefault();
	        _this4._calculateShipping();
	      });

	      // Prevents hitting 'enter' in these fields from accidental submitting the cart
	      $('#address_zip').on('keypress', function (event) {
	        if (event.keyCode === 10 || event.keyCode === 13) {
	          event.preventDefault();
	          _this4.$shippingSubmit.trigger('click');
	        }
	      });

	      this._shippingCalculator();
	    }
	  }, {
	    key: '_shippingCalculator',
	    value: function _shippingCalculator() {
	      this.Shopify.Cart.ShippingCalculator.show({
	        submitButton: this.theme.shippingCalculator.submitButton,
	        submitButtonDisabled: this.theme.shippingCalculator.submitButtonDisabled,
	        wrapperId: 'shipping-calculator-response',
	        customerIsLoggedIn: this.theme.customerLoggedIn,
	        moneyFormat: this.theme.currency.moneyFormat
	      });
	    }
	  }, {
	    key: '_calculateShipping',
	    value: function _calculateShipping() {
	      var _this5 = this;

	      this.$shippingSubmit.text(this.theme.shippingCalculator.submitButtonDisabled).attr('disabled', true);

	      var shippingAddress = {};
	      shippingAddress.country = $('#address_country').val() || '';
	      shippingAddress.province = $('#address_province').val() || '';
	      shippingAddress.zip = $('#address_zip').val() || '';

	      //Creates an ajax request which returns shipping information
	      this.Shopify.getCartShippingRatesForDestination(shippingAddress, function (response, shippingAddress) {

	        var addressBase = [];

	        if (shippingAddress.zip.length) {
	          addressBase.push(shippingAddress.zip.trim());
	        }

	        if (shippingAddress.province.length) {
	          addressBase.push(shippingAddress.province);
	        }

	        if (shippingAddress.country.length) {
	          addressBase.push(shippingAddress.country);
	        }

	        var address = addressBase.join(', ');

	        // Hide the response so that it can be populated smoothly
	        _this5.$shippingResponse.toggleClass('visible', false);

	        // Empty out contents
	        _this5.$shippingResponseMessage.empty();
	        _this5.$shippingResponseRates.empty();

	        var responseText = '';

	        if (response.length > 1) {
	          var firstRate = _this5.Shopify.Cart.ShippingCalculator.formatRate(response[0].price);
	          responseText = _this5.theme.shippingCalculator.multiRates.replace('*address*', address).replace('*number_of_rates*', response.length).replace('*rate*', '<span data-money>' + firstRate + '</span>');
	        } else if (response.length === 1) {
	          responseText = _this5.theme.shippingCalculator.oneRate.replace('*address*', address);
	        } else {
	          responseText = _this5.theme.shippingCalculator.noRates;
	        }

	        _this5.$shippingResponseMessage.html(responseText);

	        for (var i = 0; i < response.length; i++) {
	          var rate = response[i];
	          var price = _this5.Shopify.Cart.ShippingCalculator.formatRate(rate.price);
	          var rateValues = _this5.theme.shippingCalculator.rateValues.replace('*rate_title*', rate.name).replace('*rate*', '<span data-money>' + price + '</span>');

	          _this5.$shippingResponseRates.append('<li>' + rateValues + '</li>');
	        }

	        // Reset the calculating button so it can be used again
	        _this5._resetShippingButton();

	        _this5.$shippingResponse.toggleClass('visible', true);
	      });
	    }
	  }, {
	    key: '_resetShippingButton',
	    value: function _resetShippingButton() {
	      this.$shippingSubmit.text(this.theme.shippingCalculator.submitButton).attr('disabled', false);
	    }
	  }, {
	    key: '_handleErrors',
	    value: function _handleErrors(errors) {
	      var errorMessage = $.parseJSON(errors.responseText);
	      errorMessage = this.theme.shippingCalculator.errorMessage.replace('*error_message*', errorMessage.zip);
	      this._alert(errorMessage);
	      this._resetShippingButton();
	    }
	  }, {
	    key: '_alert',
	    value: function _alert(message) {
	      $('[data-cart-message]').html('<div class="cart-message">' + message + '</div>');
	    }
	  }]);
	  return Cart;
	}();

	var Home = function () {
	  function Home() {
	    classCallCheck(this, Home);

	    this.sectionContent = '[data-home-section-content]';
	    this.gridSpacing = parseInt(window.Theme.gridSpacing, 10) || 0;

	    this._init();
	  }

	  createClass(Home, [{
	    key: '_init',
	    value: function _init() {
	      var _this = this;

	      this.updateSectionHeights();

	      $(window).on('resize', index(function () {
	        _this.updateSectionHeights();
	      }, 200));
	    }
	  }, {
	    key: 'updateSectionHeights',
	    value: function updateSectionHeights() {
	      var _this2 = this;

	      $('[data-home-section]').each(function (index, section) {
	        var $section = $(section);
	        var $content = $(_this2.sectionContent, $section);

	        if (!$content.length) return;
	        if ($section.hasClass('home-section-height-image-height')) return;

	        var contentHeight = Math.ceil($content.outerHeight(true));
	        var sectionBaseHeight = Math.ceil($section.outerHeight(true));

	        if (!$section.attr('data-original-height')) {
	          $section.attr('data-original-height', sectionBaseHeight + _this2.gridSpacing * 2);
	        }

	        var sectionHeight = parseInt($section.attr('data-original-height'), 10);

	        if (contentHeight > sectionHeight) {
	          $section.css('height', contentHeight);
	        } else {
	          $section.css('height', '');
	        }
	      });
	    }
	  }]);
	  return Home;
	}();

	var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var __moduleExports$1 = createCommonjsModule(function (module, exports) {
	/*!
	 * classie v1.0.1
	 * class helper functions
	 * from bonzo https://github.com/ded/bonzo
	 * MIT license
	 * 
	 * classie.has( elem, 'my-class' ) -> true/false
	 * classie.add( elem, 'my-new-class' )
	 * classie.remove( elem, 'my-unwanted-class' )
	 * classie.toggle( elem, 'my-class' )
	 */

	/*jshint browser: true, strict: true, undef: true, unused: true */
	/*global define: false, module: false */

	( function( window ) {

	'use strict';

	// class helper functions from bonzo https://github.com/ded/bonzo

	function classReg( className ) {
	  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
	}

	// classList support for class management
	// altho to be fair, the api sucks because it won't accept multiple classes at once
	var hasClass, addClass, removeClass;

	if ( 'classList' in document.documentElement ) {
	  hasClass = function( elem, c ) {
	    return elem.classList.contains( c );
	  };
	  addClass = function( elem, c ) {
	    elem.classList.add( c );
	  };
	  removeClass = function( elem, c ) {
	    elem.classList.remove( c );
	  };
	}
	else {
	  hasClass = function( elem, c ) {
	    return classReg( c ).test( elem.className );
	  };
	  addClass = function( elem, c ) {
	    if ( !hasClass( elem, c ) ) {
	      elem.className = elem.className + ' ' + c;
	    }
	  };
	  removeClass = function( elem, c ) {
	    elem.className = elem.className.replace( classReg( c ), ' ' );
	  };
	}

	function toggleClass( elem, c ) {
	  var fn = hasClass( elem, c ) ? removeClass : addClass;
	  fn( elem, c );
	}

	var classie = {
	  // full names
	  hasClass: hasClass,
	  addClass: addClass,
	  removeClass: removeClass,
	  toggleClass: toggleClass,
	  // short names
	  has: hasClass,
	  add: addClass,
	  remove: removeClass,
	  toggle: toggleClass
	};

	// transport
	if ( typeof define === 'function' && define.amd ) {
	  // AMD
	  define( classie );
	} else if ( typeof exports === 'object' ) {
	  // CommonJS
	  module.exports = classie;
	} else {
	  // browser global
	  window.classie = classie;
	}

	})( window );
	});

	var __moduleExports$2 = createCommonjsModule(function (module) {
	/*!
	 * EventEmitter v4.2.11 - git.io/ee
	 * Unlicense - http://unlicense.org/
	 * Oliver Caldwell - http://oli.me.uk/
	 * @preserve
	 */

	;(function () {
	    'use strict';

	    /**
	     * Class for managing events.
	     * Can be extended to provide event functionality in other classes.
	     *
	     * @class EventEmitter Manages event registering and emitting.
	     */
	    function EventEmitter() {}

	    // Shortcuts to improve speed and size
	    var proto = EventEmitter.prototype;
	    var exports = this;
	    var originalGlobalValue = exports.EventEmitter;

	    /**
	     * Finds the index of the listener for the event in its storage array.
	     *
	     * @param {Function[]} listeners Array of listeners to search through.
	     * @param {Function} listener Method to look for.
	     * @return {Number} Index of the specified listener, -1 if not found
	     * @api private
	     */
	    function indexOfListener(listeners, listener) {
	        var i = listeners.length;
	        while (i--) {
	            if (listeners[i].listener === listener) {
	                return i;
	            }
	        }

	        return -1;
	    }

	    /**
	     * Alias a method while keeping the context correct, to allow for overwriting of target method.
	     *
	     * @param {String} name The name of the target method.
	     * @return {Function} The aliased method
	     * @api private
	     */
	    function alias(name) {
	        return function aliasClosure() {
	            return this[name].apply(this, arguments);
	        };
	    }

	    /**
	     * Returns the listener array for the specified event.
	     * Will initialise the event object and listener arrays if required.
	     * Will return an object if you use a regex search. The object contains keys for each matched event. So /ba[rz]/ might return an object containing bar and baz. But only if you have either defined them with defineEvent or added some listeners to them.
	     * Each property in the object response is an array of listener functions.
	     *
	     * @param {String|RegExp} evt Name of the event to return the listeners from.
	     * @return {Function[]|Object} All listener functions for the event.
	     */
	    proto.getListeners = function getListeners(evt) {
	        var events = this._getEvents();
	        var response;
	        var key;

	        // Return a concatenated array of all matching events if
	        // the selector is a regular expression.
	        if (evt instanceof RegExp) {
	            response = {};
	            for (key in events) {
	                if (events.hasOwnProperty(key) && evt.test(key)) {
	                    response[key] = events[key];
	                }
	            }
	        }
	        else {
	            response = events[evt] || (events[evt] = []);
	        }

	        return response;
	    };

	    /**
	     * Takes a list of listener objects and flattens it into a list of listener functions.
	     *
	     * @param {Object[]} listeners Raw listener objects.
	     * @return {Function[]} Just the listener functions.
	     */
	    proto.flattenListeners = function flattenListeners(listeners) {
	        var flatListeners = [];
	        var i;

	        for (i = 0; i < listeners.length; i += 1) {
	            flatListeners.push(listeners[i].listener);
	        }

	        return flatListeners;
	    };

	    /**
	     * Fetches the requested listeners via getListeners but will always return the results inside an object. This is mainly for internal use but others may find it useful.
	     *
	     * @param {String|RegExp} evt Name of the event to return the listeners from.
	     * @return {Object} All listener functions for an event in an object.
	     */
	    proto.getListenersAsObject = function getListenersAsObject(evt) {
	        var listeners = this.getListeners(evt);
	        var response;

	        if (listeners instanceof Array) {
	            response = {};
	            response[evt] = listeners;
	        }

	        return response || listeners;
	    };

	    /**
	     * Adds a listener function to the specified event.
	     * The listener will not be added if it is a duplicate.
	     * If the listener returns true then it will be removed after it is called.
	     * If you pass a regular expression as the event name then the listener will be added to all events that match it.
	     *
	     * @param {String|RegExp} evt Name of the event to attach the listener to.
	     * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.addListener = function addListener(evt, listener) {
	        var listeners = this.getListenersAsObject(evt);
	        var listenerIsWrapped = typeof listener === 'object';
	        var key;

	        for (key in listeners) {
	            if (listeners.hasOwnProperty(key) && indexOfListener(listeners[key], listener) === -1) {
	                listeners[key].push(listenerIsWrapped ? listener : {
	                    listener: listener,
	                    once: false
	                });
	            }
	        }

	        return this;
	    };

	    /**
	     * Alias of addListener
	     */
	    proto.on = alias('addListener');

	    /**
	     * Semi-alias of addListener. It will add a listener that will be
	     * automatically removed after its first execution.
	     *
	     * @param {String|RegExp} evt Name of the event to attach the listener to.
	     * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.addOnceListener = function addOnceListener(evt, listener) {
	        return this.addListener(evt, {
	            listener: listener,
	            once: true
	        });
	    };

	    /**
	     * Alias of addOnceListener.
	     */
	    proto.once = alias('addOnceListener');

	    /**
	     * Defines an event name. This is required if you want to use a regex to add a listener to multiple events at once. If you don't do this then how do you expect it to know what event to add to? Should it just add to every possible match for a regex? No. That is scary and bad.
	     * You need to tell it what event names should be matched by a regex.
	     *
	     * @param {String} evt Name of the event to create.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.defineEvent = function defineEvent(evt) {
	        this.getListeners(evt);
	        return this;
	    };

	    /**
	     * Uses defineEvent to define multiple events.
	     *
	     * @param {String[]} evts An array of event names to define.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.defineEvents = function defineEvents(evts) {
	        for (var i = 0; i < evts.length; i += 1) {
	            this.defineEvent(evts[i]);
	        }
	        return this;
	    };

	    /**
	     * Removes a listener function from the specified event.
	     * When passed a regular expression as the event name, it will remove the listener from all events that match it.
	     *
	     * @param {String|RegExp} evt Name of the event to remove the listener from.
	     * @param {Function} listener Method to remove from the event.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.removeListener = function removeListener(evt, listener) {
	        var listeners = this.getListenersAsObject(evt);
	        var index;
	        var key;

	        for (key in listeners) {
	            if (listeners.hasOwnProperty(key)) {
	                index = indexOfListener(listeners[key], listener);

	                if (index !== -1) {
	                    listeners[key].splice(index, 1);
	                }
	            }
	        }

	        return this;
	    };

	    /**
	     * Alias of removeListener
	     */
	    proto.off = alias('removeListener');

	    /**
	     * Adds listeners in bulk using the manipulateListeners method.
	     * If you pass an object as the second argument you can add to multiple events at once. The object should contain key value pairs of events and listeners or listener arrays. You can also pass it an event name and an array of listeners to be added.
	     * You can also pass it a regular expression to add the array of listeners to all events that match it.
	     * Yeah, this function does quite a bit. That's probably a bad thing.
	     *
	     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add to multiple events at once.
	     * @param {Function[]} [listeners] An optional array of listener functions to add.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.addListeners = function addListeners(evt, listeners) {
	        // Pass through to manipulateListeners
	        return this.manipulateListeners(false, evt, listeners);
	    };

	    /**
	     * Removes listeners in bulk using the manipulateListeners method.
	     * If you pass an object as the second argument you can remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
	     * You can also pass it an event name and an array of listeners to be removed.
	     * You can also pass it a regular expression to remove the listeners from all events that match it.
	     *
	     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to remove from multiple events at once.
	     * @param {Function[]} [listeners] An optional array of listener functions to remove.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.removeListeners = function removeListeners(evt, listeners) {
	        // Pass through to manipulateListeners
	        return this.manipulateListeners(true, evt, listeners);
	    };

	    /**
	     * Edits listeners in bulk. The addListeners and removeListeners methods both use this to do their job. You should really use those instead, this is a little lower level.
	     * The first argument will determine if the listeners are removed (true) or added (false).
	     * If you pass an object as the second argument you can add/remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
	     * You can also pass it an event name and an array of listeners to be added/removed.
	     * You can also pass it a regular expression to manipulate the listeners of all events that match it.
	     *
	     * @param {Boolean} remove True if you want to remove listeners, false if you want to add.
	     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add/remove from multiple events at once.
	     * @param {Function[]} [listeners] An optional array of listener functions to add/remove.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.manipulateListeners = function manipulateListeners(remove, evt, listeners) {
	        var i;
	        var value;
	        var single = remove ? this.removeListener : this.addListener;
	        var multiple = remove ? this.removeListeners : this.addListeners;

	        // If evt is an object then pass each of its properties to this method
	        if (typeof evt === 'object' && !(evt instanceof RegExp)) {
	            for (i in evt) {
	                if (evt.hasOwnProperty(i) && (value = evt[i])) {
	                    // Pass the single listener straight through to the singular method
	                    if (typeof value === 'function') {
	                        single.call(this, i, value);
	                    }
	                    else {
	                        // Otherwise pass back to the multiple function
	                        multiple.call(this, i, value);
	                    }
	                }
	            }
	        }
	        else {
	            // So evt must be a string
	            // And listeners must be an array of listeners
	            // Loop over it and pass each one to the multiple method
	            i = listeners.length;
	            while (i--) {
	                single.call(this, evt, listeners[i]);
	            }
	        }

	        return this;
	    };

	    /**
	     * Removes all listeners from a specified event.
	     * If you do not specify an event then all listeners will be removed.
	     * That means every event will be emptied.
	     * You can also pass a regex to remove all events that match it.
	     *
	     * @param {String|RegExp} [evt] Optional name of the event to remove all listeners for. Will remove from every event if not passed.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.removeEvent = function removeEvent(evt) {
	        var type = typeof evt;
	        var events = this._getEvents();
	        var key;

	        // Remove different things depending on the state of evt
	        if (type === 'string') {
	            // Remove all listeners for the specified event
	            delete events[evt];
	        }
	        else if (evt instanceof RegExp) {
	            // Remove all events matching the regex.
	            for (key in events) {
	                if (events.hasOwnProperty(key) && evt.test(key)) {
	                    delete events[key];
	                }
	            }
	        }
	        else {
	            // Remove all listeners in all events
	            delete this._events;
	        }

	        return this;
	    };

	    /**
	     * Alias of removeEvent.
	     *
	     * Added to mirror the node API.
	     */
	    proto.removeAllListeners = alias('removeEvent');

	    /**
	     * Emits an event of your choice.
	     * When emitted, every listener attached to that event will be executed.
	     * If you pass the optional argument array then those arguments will be passed to every listener upon execution.
	     * Because it uses `apply`, your array of arguments will be passed as if you wrote them out separately.
	     * So they will not arrive within the array on the other side, they will be separate.
	     * You can also pass a regular expression to emit to all events that match it.
	     *
	     * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
	     * @param {Array} [args] Optional array of arguments to be passed to each listener.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.emitEvent = function emitEvent(evt, args) {
	        var listeners = this.getListenersAsObject(evt);
	        var listener;
	        var i;
	        var key;
	        var response;

	        for (key in listeners) {
	            if (listeners.hasOwnProperty(key)) {
	                i = listeners[key].length;

	                while (i--) {
	                    // If the listener returns true then it shall be removed from the event
	                    // The function is executed either with a basic call or an apply if there is an args array
	                    listener = listeners[key][i];

	                    if (listener.once === true) {
	                        this.removeListener(evt, listener.listener);
	                    }

	                    response = listener.listener.apply(this, args || []);

	                    if (response === this._getOnceReturnValue()) {
	                        this.removeListener(evt, listener.listener);
	                    }
	                }
	            }
	        }

	        return this;
	    };

	    /**
	     * Alias of emitEvent
	     */
	    proto.trigger = alias('emitEvent');

	    /**
	     * Subtly different from emitEvent in that it will pass its arguments on to the listeners, as opposed to taking a single array of arguments to pass on.
	     * As with emitEvent, you can pass a regex in place of the event name to emit to all events that match it.
	     *
	     * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
	     * @param {...*} Optional additional arguments to be passed to each listener.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.emit = function emit(evt) {
	        var args = Array.prototype.slice.call(arguments, 1);
	        return this.emitEvent(evt, args);
	    };

	    /**
	     * Sets the current value to check against when executing listeners. If a
	     * listeners return value matches the one set here then it will be removed
	     * after execution. This value defaults to true.
	     *
	     * @param {*} value The new value to check for when executing listeners.
	     * @return {Object} Current instance of EventEmitter for chaining.
	     */
	    proto.setOnceReturnValue = function setOnceReturnValue(value) {
	        this._onceReturnValue = value;
	        return this;
	    };

	    /**
	     * Fetches the current value to check against when executing listeners. If
	     * the listeners return value matches this one then it should be removed
	     * automatically. It will return true by default.
	     *
	     * @return {*|Boolean} The current value to check for or the default, true.
	     * @api private
	     */
	    proto._getOnceReturnValue = function _getOnceReturnValue() {
	        if (this.hasOwnProperty('_onceReturnValue')) {
	            return this._onceReturnValue;
	        }
	        else {
	            return true;
	        }
	    };

	    /**
	     * Fetches the events object and creates one if required.
	     *
	     * @return {Object} The events storage object.
	     * @api private
	     */
	    proto._getEvents = function _getEvents() {
	        return this._events || (this._events = {});
	    };

	    /**
	     * Reverts the global {@link EventEmitter} to its previous value and returns a reference to this version.
	     *
	     * @return {Function} Non conflicting EventEmitter class.
	     */
	    EventEmitter.noConflict = function noConflict() {
	        exports.EventEmitter = originalGlobalValue;
	        return EventEmitter;
	    };

	    // Expose the class either via AMD, CommonJS or the global object
	    if (typeof define === 'function' && define.amd) {
	        define(function () {
	            return EventEmitter;
	        });
	    }
	    else if (typeof module === 'object' && module.exports){
	        module.exports = EventEmitter;
	    }
	    else {
	        exports.EventEmitter = EventEmitter;
	    }
	}.call(commonjsGlobal));
	});

	var __moduleExports$3 = createCommonjsModule(function (module, exports) {
	/*!
	 * eventie v1.0.6
	 * event binding helper
	 *   eventie.bind( elem, 'click', myFn )
	 *   eventie.unbind( elem, 'click', myFn )
	 * MIT license
	 */

	/*jshint browser: true, undef: true, unused: true */
	/*global define: false, module: false */

	( function( window ) {

	'use strict';

	var docElem = document.documentElement;

	var bind = function() {};

	function getIEEvent( obj ) {
	  var event = window.event;
	  // add event.target
	  event.target = event.target || event.srcElement || obj;
	  return event;
	}

	if ( docElem.addEventListener ) {
	  bind = function( obj, type, fn ) {
	    obj.addEventListener( type, fn, false );
	  };
	} else if ( docElem.attachEvent ) {
	  bind = function( obj, type, fn ) {
	    obj[ type + fn ] = fn.handleEvent ?
	      function() {
	        var event = getIEEvent( obj );
	        fn.handleEvent.call( fn, event );
	      } :
	      function() {
	        var event = getIEEvent( obj );
	        fn.call( obj, event );
	      };
	    obj.attachEvent( "on" + type, obj[ type + fn ] );
	  };
	}

	var unbind = function() {};

	if ( docElem.removeEventListener ) {
	  unbind = function( obj, type, fn ) {
	    obj.removeEventListener( type, fn, false );
	  };
	} else if ( docElem.detachEvent ) {
	  unbind = function( obj, type, fn ) {
	    obj.detachEvent( "on" + type, obj[ type + fn ] );
	    try {
	      delete obj[ type + fn ];
	    } catch ( err ) {
	      // can't delete window object properties
	      obj[ type + fn ] = undefined;
	    }
	  };
	}

	var eventie = {
	  bind: bind,
	  unbind: unbind
	};

	// ----- module definition ----- //

	if ( typeof define === 'function' && define.amd ) {
	  // AMD
	  define( eventie );
	} else if ( typeof exports === 'object' ) {
	  // CommonJS
	  module.exports = eventie;
	} else {
	  // browser global
	  window.eventie = eventie;
	}

	})( window );
	});

	var __moduleExports$5 = createCommonjsModule(function (module, exports) {
	/*!
	 * getStyleProperty v1.0.4
	 * original by kangax
	 * http://perfectionkills.com/feature-testing-css-properties/
	 * MIT license
	 */

	/*jshint browser: true, strict: true, undef: true */
	/*global define: false, exports: false, module: false */

	( function( window ) {

	'use strict';

	var prefixes = 'Webkit Moz ms Ms O'.split(' ');
	var docElemStyle = document.documentElement.style;

	function getStyleProperty( propName ) {
	  if ( !propName ) {
	    return;
	  }

	  // test standard property first
	  if ( typeof docElemStyle[ propName ] === 'string' ) {
	    return propName;
	  }

	  // capitalize
	  propName = propName.charAt(0).toUpperCase() + propName.slice(1);

	  // test vendor specific properties
	  var prefixed;
	  for ( var i=0, len = prefixes.length; i < len; i++ ) {
	    prefixed = prefixes[i] + propName;
	    if ( typeof docElemStyle[ prefixed ] === 'string' ) {
	      return prefixed;
	    }
	  }
	}

	// transport
	if ( typeof define === 'function' && define.amd ) {
	  // AMD
	  define( function() {
	    return getStyleProperty;
	  });
	} else if ( typeof exports === 'object' ) {
	  // CommonJS for Component
	  module.exports = getStyleProperty;
	} else {
	  // browser global
	  window.getStyleProperty = getStyleProperty;
	}

	})( window );
	});

	var __moduleExports$4 = createCommonjsModule(function (module, exports) {
	/*!
	 * getSize v1.2.2
	 * measure size of elements
	 * MIT license
	 */

	/*jshint browser: true, strict: true, undef: true, unused: true */
	/*global define: false, exports: false, require: false, module: false, console: false */

	( function( window, undefined ) {

	'use strict';

	// -------------------------- helpers -------------------------- //

	// get a number from a string, not a percentage
	function getStyleSize( value ) {
	  var num = parseFloat( value );
	  // not a percent like '100%', and a number
	  var isValid = value.indexOf('%') === -1 && !isNaN( num );
	  return isValid && num;
	}

	function noop() {}

	var logError = typeof console === 'undefined' ? noop :
	  function( message ) {
	    console.error( message );
	  };

	// -------------------------- measurements -------------------------- //

	var measurements = [
	  'paddingLeft',
	  'paddingRight',
	  'paddingTop',
	  'paddingBottom',
	  'marginLeft',
	  'marginRight',
	  'marginTop',
	  'marginBottom',
	  'borderLeftWidth',
	  'borderRightWidth',
	  'borderTopWidth',
	  'borderBottomWidth'
	];

	function getZeroSize() {
	  var size = {
	    width: 0,
	    height: 0,
	    innerWidth: 0,
	    innerHeight: 0,
	    outerWidth: 0,
	    outerHeight: 0
	  };
	  for ( var i=0, len = measurements.length; i < len; i++ ) {
	    var measurement = measurements[i];
	    size[ measurement ] = 0;
	  }
	  return size;
	}



	function defineGetSize( getStyleProperty ) {

	// -------------------------- setup -------------------------- //

	var isSetup = false;

	var getStyle, boxSizingProp, isBoxSizeOuter;

	/**
	 * setup vars and functions
	 * do it on initial getSize(), rather than on script load
	 * For Firefox bug https://bugzilla.mozilla.org/show_bug.cgi?id=548397
	 */
	function setup() {
	  // setup once
	  if ( isSetup ) {
	    return;
	  }
	  isSetup = true;

	  var getComputedStyle = window.getComputedStyle;
	  getStyle = ( function() {
	    var getStyleFn = getComputedStyle ?
	      function( elem ) {
	        return getComputedStyle( elem, null );
	      } :
	      function( elem ) {
	        return elem.currentStyle;
	      };

	      return function getStyle( elem ) {
	        var style = getStyleFn( elem );
	        if ( !style ) {
	          logError( 'Style returned ' + style +
	            '. Are you running this code in a hidden iframe on Firefox? ' +
	            'See http://bit.ly/getsizebug1' );
	        }
	        return style;
	      };
	  })();

	  // -------------------------- box sizing -------------------------- //

	  boxSizingProp = getStyleProperty('boxSizing');

	  /**
	   * WebKit measures the outer-width on style.width on border-box elems
	   * IE & Firefox measures the inner-width
	   */
	  if ( boxSizingProp ) {
	    var div = document.createElement('div');
	    div.style.width = '200px';
	    div.style.padding = '1px 2px 3px 4px';
	    div.style.borderStyle = 'solid';
	    div.style.borderWidth = '1px 2px 3px 4px';
	    div.style[ boxSizingProp ] = 'border-box';

	    var body = document.body || document.documentElement;
	    body.appendChild( div );
	    var style = getStyle( div );

	    isBoxSizeOuter = getStyleSize( style.width ) === 200;
	    body.removeChild( div );
	  }

	}

	// -------------------------- getSize -------------------------- //

	function getSize( elem ) {
	  setup();

	  // use querySeletor if elem is string
	  if ( typeof elem === 'string' ) {
	    elem = document.querySelector( elem );
	  }

	  // do not proceed on non-objects
	  if ( !elem || typeof elem !== 'object' || !elem.nodeType ) {
	    return;
	  }

	  var style = getStyle( elem );

	  // if hidden, everything is 0
	  if ( style.display === 'none' ) {
	    return getZeroSize();
	  }

	  var size = {};
	  size.width = elem.offsetWidth;
	  size.height = elem.offsetHeight;

	  var isBorderBox = size.isBorderBox = !!( boxSizingProp &&
	    style[ boxSizingProp ] && style[ boxSizingProp ] === 'border-box' );

	  // get all measurements
	  for ( var i=0, len = measurements.length; i < len; i++ ) {
	    var measurement = measurements[i];
	    var value = style[ measurement ];
	    value = mungeNonPixel( elem, value );
	    var num = parseFloat( value );
	    // any 'auto', 'medium' value will be 0
	    size[ measurement ] = !isNaN( num ) ? num : 0;
	  }

	  var paddingWidth = size.paddingLeft + size.paddingRight;
	  var paddingHeight = size.paddingTop + size.paddingBottom;
	  var marginWidth = size.marginLeft + size.marginRight;
	  var marginHeight = size.marginTop + size.marginBottom;
	  var borderWidth = size.borderLeftWidth + size.borderRightWidth;
	  var borderHeight = size.borderTopWidth + size.borderBottomWidth;

	  var isBorderBoxSizeOuter = isBorderBox && isBoxSizeOuter;

	  // overwrite width and height if we can get it from style
	  var styleWidth = getStyleSize( style.width );
	  if ( styleWidth !== false ) {
	    size.width = styleWidth +
	      // add padding and border unless it's already including it
	      ( isBorderBoxSizeOuter ? 0 : paddingWidth + borderWidth );
	  }

	  var styleHeight = getStyleSize( style.height );
	  if ( styleHeight !== false ) {
	    size.height = styleHeight +
	      // add padding and border unless it's already including it
	      ( isBorderBoxSizeOuter ? 0 : paddingHeight + borderHeight );
	  }

	  size.innerWidth = size.width - ( paddingWidth + borderWidth );
	  size.innerHeight = size.height - ( paddingHeight + borderHeight );

	  size.outerWidth = size.width + marginWidth;
	  size.outerHeight = size.height + marginHeight;

	  return size;
	}

	// IE8 returns percent values, not pixels
	// taken from jQuery's curCSS
	function mungeNonPixel( elem, value ) {
	  // IE8 and has percent value
	  if ( window.getComputedStyle || value.indexOf('%') === -1 ) {
	    return value;
	  }
	  var style = elem.style;
	  // Remember the original values
	  var left = style.left;
	  var rs = elem.runtimeStyle;
	  var rsLeft = rs && rs.left;

	  // Put in the new values to get a computed value out
	  if ( rsLeft ) {
	    rs.left = elem.currentStyle.left;
	  }
	  style.left = value;
	  value = style.pixelLeft;

	  // Revert the changed values
	  style.left = left;
	  if ( rsLeft ) {
	    rs.left = rsLeft;
	  }

	  return value;
	}

	return getSize;

	}

	// transport
	if ( typeof define === 'function' && define.amd ) {
	  // AMD for RequireJS
	  define( [ 'get-style-property/get-style-property' ], defineGetSize );
	} else if ( typeof exports === 'object' ) {
	  // CommonJS for Component
	  module.exports = defineGetSize( __moduleExports$5 );
	} else {
	  // browser global
	  window.getSize = defineGetSize( window.getStyleProperty );
	}

	})( window );
	});

	var __moduleExports$7 = createCommonjsModule(function (module, exports) {
	/*!
	 * docReady v1.0.4
	 * Cross browser DOMContentLoaded event emitter
	 * MIT license
	 */

	/*jshint browser: true, strict: true, undef: true, unused: true*/
	/*global define: false, require: false, module: false */

	( function( window ) {

	'use strict';

	var document = window.document;
	// collection of functions to be triggered on ready
	var queue = [];

	function docReady( fn ) {
	  // throw out non-functions
	  if ( typeof fn !== 'function' ) {
	    return;
	  }

	  if ( docReady.isReady ) {
	    // ready now, hit it
	    fn();
	  } else {
	    // queue function when ready
	    queue.push( fn );
	  }
	}

	docReady.isReady = false;

	// triggered on various doc ready events
	function onReady( event ) {
	  // bail if already triggered or IE8 document is not ready just yet
	  var isIE8NotReady = event.type === 'readystatechange' && document.readyState !== 'complete';
	  if ( docReady.isReady || isIE8NotReady ) {
	    return;
	  }

	  trigger();
	}

	function trigger() {
	  docReady.isReady = true;
	  // process queue
	  for ( var i=0, len = queue.length; i < len; i++ ) {
	    var fn = queue[i];
	    fn();
	  }
	}

	function defineDocReady( eventie ) {
	  // trigger ready if page is ready
	  if ( document.readyState === 'complete' ) {
	    trigger();
	  } else {
	    // listen for events
	    eventie.bind( document, 'DOMContentLoaded', onReady );
	    eventie.bind( document, 'readystatechange', onReady );
	    eventie.bind( window, 'load', onReady );
	  }

	  return docReady;
	}

	// transport
	if ( typeof define === 'function' && define.amd ) {
	  // AMD
	  define( [ 'eventie/eventie' ], defineDocReady );
	} else if ( typeof exports === 'object' ) {
	  module.exports = defineDocReady( __moduleExports$3 );
	} else {
	  // browser global
	  window.docReady = defineDocReady( window.eventie );
	}

	})( window );
	});

	var __moduleExports$8 = createCommonjsModule(function (module, exports) {
	/**
	 * matchesSelector v1.0.3
	 * matchesSelector( element, '.selector' )
	 * MIT license
	 */

	/*jshint browser: true, strict: true, undef: true, unused: true */
	/*global define: false, module: false */

	( function( ElemProto ) {

	  'use strict';

	  var matchesMethod = ( function() {
	    // check for the standard method name first
	    if ( ElemProto.matches ) {
	      return 'matches';
	    }
	    // check un-prefixed
	    if ( ElemProto.matchesSelector ) {
	      return 'matchesSelector';
	    }
	    // check vendor prefixes
	    var prefixes = [ 'webkit', 'moz', 'ms', 'o' ];

	    for ( var i=0, len = prefixes.length; i < len; i++ ) {
	      var prefix = prefixes[i];
	      var method = prefix + 'MatchesSelector';
	      if ( ElemProto[ method ] ) {
	        return method;
	      }
	    }
	  })();

	  // ----- match ----- //

	  function match( elem, selector ) {
	    return elem[ matchesMethod ]( selector );
	  }

	  // ----- appendToFragment ----- //

	  function checkParent( elem ) {
	    // not needed if already has parent
	    if ( elem.parentNode ) {
	      return;
	    }
	    var fragment = document.createDocumentFragment();
	    fragment.appendChild( elem );
	  }

	  // ----- query ----- //

	  // fall back to using QSA
	  // thx @jonathantneal https://gist.github.com/3062955
	  function query( elem, selector ) {
	    // append to fragment if no parent
	    checkParent( elem );

	    // match elem with all selected elems of parent
	    var elems = elem.parentNode.querySelectorAll( selector );
	    for ( var i=0, len = elems.length; i < len; i++ ) {
	      // return true if match
	      if ( elems[i] === elem ) {
	        return true;
	      }
	    }
	    // otherwise return false
	    return false;
	  }

	  // ----- matchChild ----- //

	  function matchChild( elem, selector ) {
	    checkParent( elem );
	    return match( elem, selector );
	  }

	  // ----- matchesSelector ----- //

	  var matchesSelector;

	  if ( matchesMethod ) {
	    // IE9 supports matchesSelector, but doesn't work on orphaned elems
	    // check for that
	    var div = document.createElement('div');
	    var supportsOrphans = match( div, 'div' );
	    matchesSelector = supportsOrphans ? match : matchChild;
	  } else {
	    matchesSelector = query;
	  }

	  // transport
	  if ( typeof define === 'function' && define.amd ) {
	    // AMD
	    define( function() {
	      return matchesSelector;
	    });
	  } else if ( typeof exports === 'object' ) {
	    module.exports = matchesSelector;
	  }
	  else {
	    // browser global
	    window.matchesSelector = matchesSelector;
	  }

	})( Element.prototype );
	});

	var __moduleExports$6 = createCommonjsModule(function (module, exports) {
	/**
	 * Fizzy UI utils v1.0.1
	 * MIT license
	 */

	/*jshint browser: true, undef: true, unused: true, strict: true */

	( function( window, factory ) {
	  /*global define: false, module: false, require: false */
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'doc-ready/doc-ready',
	      'matches-selector/matches-selector'
	    ], function( docReady, matchesSelector ) {
	      return factory( window, docReady, matchesSelector );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$7,
	      __moduleExports$8
	    );
	  } else {
	    // browser global
	    window.fizzyUIUtils = factory(
	      window,
	      window.docReady,
	      window.matchesSelector
	    );
	  }

	}( window, function factory( window, docReady, matchesSelector ) {

	'use strict';

	var utils = {};

	// ----- extend ----- //

	// extends objects
	utils.extend = function( a, b ) {
	  for ( var prop in b ) {
	    a[ prop ] = b[ prop ];
	  }
	  return a;
	};

	// ----- modulo ----- //

	utils.modulo = function( num, div ) {
	  return ( ( num % div ) + div ) % div;
	};

	// ----- isArray ----- //
	  
	var objToString = Object.prototype.toString;
	utils.isArray = function( obj ) {
	  return objToString.call( obj ) == '[object Array]';
	};

	// ----- makeArray ----- //

	// turn element or nodeList into an array
	utils.makeArray = function( obj ) {
	  var ary = [];
	  if ( utils.isArray( obj ) ) {
	    // use object if already an array
	    ary = obj;
	  } else if ( obj && typeof obj.length == 'number' ) {
	    // convert nodeList to array
	    for ( var i=0, len = obj.length; i < len; i++ ) {
	      ary.push( obj[i] );
	    }
	  } else {
	    // array of single index
	    ary.push( obj );
	  }
	  return ary;
	};

	// ----- indexOf ----- //

	// index of helper cause IE8
	utils.indexOf = Array.prototype.indexOf ? function( ary, obj ) {
	    return ary.indexOf( obj );
	  } : function( ary, obj ) {
	    for ( var i=0, len = ary.length; i < len; i++ ) {
	      if ( ary[i] === obj ) {
	        return i;
	      }
	    }
	    return -1;
	  };

	// ----- removeFrom ----- //

	utils.removeFrom = function( ary, obj ) {
	  var index = utils.indexOf( ary, obj );
	  if ( index != -1 ) {
	    ary.splice( index, 1 );
	  }
	};

	// ----- isElement ----- //

	// http://stackoverflow.com/a/384380/182183
	utils.isElement = ( typeof HTMLElement == 'function' || typeof HTMLElement == 'object' ) ?
	  function isElementDOM2( obj ) {
	    return obj instanceof HTMLElement;
	  } :
	  function isElementQuirky( obj ) {
	    return obj && typeof obj == 'object' &&
	      obj.nodeType == 1 && typeof obj.nodeName == 'string';
	  };

	// ----- setText ----- //

	utils.setText = ( function() {
	  var setTextProperty;
	  function setText( elem, text ) {
	    // only check setTextProperty once
	    setTextProperty = setTextProperty || ( document.documentElement.textContent !== undefined ? 'textContent' : 'innerText' );
	    elem[ setTextProperty ] = text;
	  }
	  return setText;
	})();

	// ----- getParent ----- //

	utils.getParent = function( elem, selector ) {
	  while ( elem != document.body ) {
	    elem = elem.parentNode;
	    if ( matchesSelector( elem, selector ) ) {
	      return elem;
	    }
	  }
	};

	// ----- getQueryElement ----- //

	// use element as selector string
	utils.getQueryElement = function( elem ) {
	  if ( typeof elem == 'string' ) {
	    return document.querySelector( elem );
	  }
	  return elem;
	};

	// ----- handleEvent ----- //

	// enable .ontype to trigger from .addEventListener( elem, 'type' )
	utils.handleEvent = function( event ) {
	  var method = 'on' + event.type;
	  if ( this[ method ] ) {
	    this[ method ]( event );
	  }
	};

	// ----- filterFindElements ----- //

	utils.filterFindElements = function( elems, selector ) {
	  // make array of elems
	  elems = utils.makeArray( elems );
	  var ffElems = [];

	  for ( var i=0, len = elems.length; i < len; i++ ) {
	    var elem = elems[i];
	    // check that elem is an actual element
	    if ( !utils.isElement( elem ) ) {
	      continue;
	    }
	    // filter & find items if we have a selector
	    if ( selector ) {
	      // filter siblings
	      if ( matchesSelector( elem, selector ) ) {
	        ffElems.push( elem );
	      }
	      // find children
	      var childElems = elem.querySelectorAll( selector );
	      // concat childElems to filterFound array
	      for ( var j=0, jLen = childElems.length; j < jLen; j++ ) {
	        ffElems.push( childElems[j] );
	      }
	    } else {
	      ffElems.push( elem );
	    }
	  }

	  return ffElems;
	};

	// ----- debounceMethod ----- //

	utils.debounceMethod = function( _class, methodName, threshold ) {
	  // original method
	  var method = _class.prototype[ methodName ];
	  var timeoutName = methodName + 'Timeout';

	  _class.prototype[ methodName ] = function() {
	    var timeout = this[ timeoutName ];
	    if ( timeout ) {
	      clearTimeout( timeout );
	    }
	    var args = arguments;

	    var _this = this;
	    this[ timeoutName ] = setTimeout( function() {
	      method.apply( _this, args );
	      delete _this[ timeoutName ];
	    }, threshold || 100 );
	  };
	};

	// ----- htmlInit ----- //

	// http://jamesroberts.name/blog/2010/02/22/string-functions-for-javascript-trim-to-camel-case-to-dashed-and-to-underscore/
	utils.toDashed = function( str ) {
	  return str.replace( /(.)([A-Z])/g, function( match, $1, $2 ) {
	    return $1 + '-' + $2;
	  }).toLowerCase();
	};

	var console = window.console;
	/**
	 * allow user to initialize classes via .js-namespace class
	 * htmlInit( Widget, 'widgetName' )
	 * options are parsed from data-namespace-option attribute
	 */
	utils.htmlInit = function( WidgetClass, namespace ) {
	  docReady( function() {
	    var dashedNamespace = utils.toDashed( namespace );
	    var elems = document.querySelectorAll( '.js-' + dashedNamespace );
	    var dataAttr = 'data-' + dashedNamespace + '-options';

	    for ( var i=0, len = elems.length; i < len; i++ ) {
	      var elem = elems[i];
	      var attr = elem.getAttribute( dataAttr );
	      var options;
	      try {
	        options = attr && JSON.parse( attr );
	      } catch ( error ) {
	        // log error, do not initialize
	        if ( console ) {
	          console.error( 'Error parsing ' + dataAttr + ' on ' +
	            elem.nodeName.toLowerCase() + ( elem.id ? '#' + elem.id : '' ) + ': ' +
	            error );
	        }
	        continue;
	      }
	      // initialize
	      var instance = new WidgetClass( elem, options );
	      // make available via $().data('layoutname')
	      var jQuery = window.jQuery;
	      if ( jQuery ) {
	        jQuery.data( elem, namespace, instance );
	      }
	    }
	  });
	};

	// -----  ----- //

	return utils;

	}));
	});

	var __moduleExports$9 = createCommonjsModule(function (module, exports) {
	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'get-size/get-size'
	    ], function( getSize ) {
	      return factory( window, getSize );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$4
	    );
	  } else {
	    // browser global
	    window.Flickity = window.Flickity || {};
	    window.Flickity.Cell = factory(
	      window,
	      window.getSize
	    );
	  }

	}( window, function factory( window, getSize ) {

	'use strict';

	function Cell( elem, parent ) {
	  this.element = elem;
	  this.parent = parent;

	  this.create();
	}

	var isIE8 = 'attachEvent' in window;

	Cell.prototype.create = function() {
	  this.element.style.position = 'absolute';
	  // IE8 prevent child from changing focus http://stackoverflow.com/a/17525223/182183
	  if ( isIE8 ) {
	    this.element.setAttribute( 'unselectable', 'on' );
	  }
	  this.x = 0;
	  this.shift = 0;
	};

	Cell.prototype.destroy = function() {
	  // reset style
	  this.element.style.position = '';
	  var side = this.parent.originSide;
	  this.element.style[ side ] = '';
	};

	Cell.prototype.getSize = function() {
	  this.size = getSize( this.element );
	};

	Cell.prototype.setPosition = function( x ) {
	  this.x = x;
	  this.setDefaultTarget();
	  this.renderPosition( x );
	};

	Cell.prototype.setDefaultTarget = function() {
	  var marginProperty = this.parent.originSide == 'left' ? 'marginLeft' : 'marginRight';
	  this.target = this.x + this.size[ marginProperty ] +
	    this.size.width * this.parent.cellAlign;
	};

	Cell.prototype.renderPosition = function( x ) {
	  // render position of cell with in slider
	  var side = this.parent.originSide;
	  this.element.style[ side ] = this.parent.getPositionValue( x );
	};

	/**
	 * @param {Integer} factor - 0, 1, or -1
	**/
	Cell.prototype.wrapShift = function( shift ) {
	  this.shift = shift;
	  this.renderPosition( this.x + this.parent.slideableWidth * shift );
	};

	Cell.prototype.remove = function() {
	  this.element.parentNode.removeChild( this.element );
	};

	return Cell;

	}));
	});

	var __moduleExports$10 = createCommonjsModule(function (module, exports) {
	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'get-style-property/get-style-property',
	      'fizzy-ui-utils/utils'
	    ], function( getStyleProperty, utils ) {
	      return factory( window, getStyleProperty, utils );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$5,
	      __moduleExports$6
	    );
	  } else {
	    // browser global
	    window.Flickity = window.Flickity || {};
	    window.Flickity.animatePrototype = factory(
	      window,
	      window.getStyleProperty,
	      window.fizzyUIUtils
	    );
	  }

	}( window, function factory( window, getStyleProperty, utils ) {

	'use strict';

	// -------------------------- requestAnimationFrame -------------------------- //

	// https://gist.github.com/1866474

	var lastTime = 0;
	var prefixes = 'webkit moz ms o'.split(' ');
	// get unprefixed rAF and cAF, if present
	var requestAnimationFrame = window.requestAnimationFrame;
	var cancelAnimationFrame = window.cancelAnimationFrame;
	// loop through vendor prefixes and get prefixed rAF and cAF
	var prefix;
	for( var i = 0; i < prefixes.length; i++ ) {
	  if ( requestAnimationFrame && cancelAnimationFrame ) {
	    break;
	  }
	  prefix = prefixes[i];
	  requestAnimationFrame = requestAnimationFrame || window[ prefix + 'RequestAnimationFrame' ];
	  cancelAnimationFrame  = cancelAnimationFrame  || window[ prefix + 'CancelAnimationFrame' ] ||
	                            window[ prefix + 'CancelRequestAnimationFrame' ];
	}

	// fallback to setTimeout and clearTimeout if either request/cancel is not supported
	if ( !requestAnimationFrame || !cancelAnimationFrame )  {
	  requestAnimationFrame = function( callback ) {
	    var currTime = new Date().getTime();
	    var timeToCall = Math.max( 0, 16 - ( currTime - lastTime ) );
	    var id = window.setTimeout( function() {
	      callback( currTime + timeToCall );
	    }, timeToCall );
	    lastTime = currTime + timeToCall;
	    return id;
	  };

	  cancelAnimationFrame = function( id ) {
	    window.clearTimeout( id );
	  };
	}

	// -------------------------- animate -------------------------- //

	var proto = {};

	proto.startAnimation = function() {
	  if ( this.isAnimating ) {
	    return;
	  }

	  this.isAnimating = true;
	  this.restingFrames = 0;
	  this.animate();
	};

	proto.animate = function() {
	  this.applyDragForce();
	  this.applySelectedAttraction();

	  var previousX = this.x;

	  this.integratePhysics();
	  this.positionSlider();
	  this.settle( previousX );
	  // animate next frame
	  if ( this.isAnimating ) {
	    var _this = this;
	    requestAnimationFrame( function animateFrame() {
	      _this.animate();
	    });
	  }

	  /** /
	  // log animation frame rate
	  var now = new Date();
	  if ( this.then ) {
	    console.log( ~~( 1000 / (now-this.then)) + 'fps' )
	  }
	  this.then = now;
	  /**/
	};


	var transformProperty = getStyleProperty('transform');
	var is3d = !!getStyleProperty('perspective');

	proto.positionSlider = function() {
	  var x = this.x;
	  // wrap position around
	  if ( this.options.wrapAround && this.cells.length > 1 ) {
	    x = utils.modulo( x, this.slideableWidth );
	    x = x - this.slideableWidth;
	    this.shiftWrapCells( x );
	  }

	  x = x + this.cursorPosition;

	  // reverse if right-to-left and using transform
	  x = this.options.rightToLeft && transformProperty ? -x : x;

	  var value = this.getPositionValue( x );

	  if ( transformProperty ) {
	    // use 3D tranforms for hardware acceleration on iOS
	    // but use 2D when settled, for better font-rendering
	    this.slider.style[ transformProperty ] = is3d && this.isAnimating ?
	      'translate3d(' + value + ',0,0)' : 'translateX(' + value + ')';
	  } else {
	    this.slider.style[ this.originSide ] = value;
	  }
	};

	proto.positionSliderAtSelected = function() {
	  if ( !this.cells.length ) {
	    return;
	  }
	  var selectedCell = this.cells[ this.selectedIndex ];
	  this.x = -selectedCell.target;
	  this.positionSlider();
	};

	proto.getPositionValue = function( position ) {
	  if ( this.options.percentPosition ) {
	    // percent position, round to 2 digits, like 12.34%
	    return ( Math.round( ( position / this.size.innerWidth ) * 10000 ) * 0.01 )+ '%';
	  } else {
	    // pixel positioning
	    return Math.round( position ) + 'px';
	  }
	};

	proto.settle = function( previousX ) {
	  // keep track of frames where x hasn't moved
	  if ( !this.isPointerDown && Math.round( this.x * 100 ) == Math.round( previousX * 100 ) ) {
	    this.restingFrames++;
	  }
	  // stop animating if resting for 3 or more frames
	  if ( this.restingFrames > 2 ) {
	    this.isAnimating = false;
	    delete this.isFreeScrolling;
	    // render position with translateX when settled
	    if ( is3d ) {
	      this.positionSlider();
	    }
	    this.dispatchEvent('settle');
	  }
	};

	proto.shiftWrapCells = function( x ) {
	  // shift before cells
	  var beforeGap = this.cursorPosition + x;
	  this._shiftCells( this.beforeShiftCells, beforeGap, -1 );
	  // shift after cells
	  var afterGap = this.size.innerWidth - ( x + this.slideableWidth + this.cursorPosition );
	  this._shiftCells( this.afterShiftCells, afterGap, 1 );
	};

	proto._shiftCells = function( cells, gap, shift ) {
	  for ( var i=0, len = cells.length; i < len; i++ ) {
	    var cell = cells[i];
	    var cellShift = gap > 0 ? shift : 0;
	    cell.wrapShift( cellShift );
	    gap -= cell.size.outerWidth;
	  }
	};

	proto._unshiftCells = function( cells ) {
	  if ( !cells || !cells.length ) {
	    return;
	  }
	  for ( var i=0, len = cells.length; i < len; i++ ) {
	    cells[i].wrapShift( 0 );
	  }
	};

	// -------------------------- physics -------------------------- //

	proto.integratePhysics = function() {
	  this.velocity += this.accel;
	  this.x += this.velocity;
	  this.velocity *= this.getFrictionFactor();
	  // reset acceleration
	  this.accel = 0;
	};

	proto.applyForce = function( force ) {
	  this.accel += force;
	};

	proto.getFrictionFactor = function() {
	  return 1 - this.options[ this.isFreeScrolling ? 'freeScrollFriction' : 'friction' ];
	};


	proto.getRestingPosition = function() {
	  // my thanks to Steven Wittens, who simplified this math greatly
	  return this.x + this.velocity / ( 1 - this.getFrictionFactor() );
	};

	proto.applyDragForce = function() {
	  if ( !this.isPointerDown ) {
	    return;
	  }
	  // change the position to drag position by applying force
	  var dragVelocity = this.dragX - this.x;
	  var dragForce = dragVelocity - this.velocity;
	  this.applyForce( dragForce );
	};

	proto.applySelectedAttraction = function() {
	  // do not attract if pointer down or no cells
	  var len = this.cells.length;
	  if ( this.isPointerDown || this.isFreeScrolling || !len ) {
	    return;
	  }
	  var cell = this.cells[ this.selectedIndex ];
	  var wrap = this.options.wrapAround && len > 1 ?
	    this.slideableWidth * Math.floor( this.selectedIndex / len ) : 0;
	  var distance = ( cell.target + wrap ) * -1 - this.x;
	  var force = distance * this.options.selectedAttraction;
	  this.applyForce( force );
	};

	return proto;

	}));
	});

	var __moduleExports = createCommonjsModule(function (module, exports) {
	/**
	 * Flickity main
	 */

	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'classie/classie',
	      'eventEmitter/EventEmitter',
	      'eventie/eventie',
	      'get-size/get-size',
	      'fizzy-ui-utils/utils',
	      './cell',
	      './animate'
	    ], function( classie, EventEmitter, eventie, getSize, utils, Cell, animatePrototype ) {
	      return factory( window, classie, EventEmitter, eventie, getSize, utils, Cell, animatePrototype );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$1,
	      __moduleExports$2,
	      __moduleExports$3,
	      __moduleExports$4,
	      __moduleExports$6,
	      __moduleExports$9,
	      __moduleExports$10
	    );
	  } else {
	    // browser global
	    var _Flickity = window.Flickity;

	    window.Flickity = factory(
	      window,
	      window.classie,
	      window.EventEmitter,
	      window.eventie,
	      window.getSize,
	      window.fizzyUIUtils,
	      _Flickity.Cell,
	      _Flickity.animatePrototype
	    );
	  }

	}( window, function factory( window, classie, EventEmitter, eventie, getSize,
	  utils, Cell, animatePrototype ) {

	'use strict';

	// vars
	var jQuery = window.jQuery;
	var getComputedStyle = window.getComputedStyle;
	var console = window.console;

	function moveElements( elems, toElem ) {
	  elems = utils.makeArray( elems );
	  while ( elems.length ) {
	    toElem.appendChild( elems.shift() );
	  }
	}

	// -------------------------- Flickity -------------------------- //

	// globally unique identifiers
	var GUID = 0;
	// internal store of all Flickity intances
	var instances = {};

	function Flickity( element, options ) {
	  var queryElement = utils.getQueryElement( element );
	  if ( !queryElement ) {
	    if ( console ) {
	      console.error( 'Bad element for Flickity: ' + ( queryElement || element ) );
	    }
	    return;
	  }
	  this.element = queryElement;
	  // add jQuery
	  if ( jQuery ) {
	    this.$element = jQuery( this.element );
	  }
	  // options
	  this.options = utils.extend( {}, this.constructor.defaults );
	  this.option( options );

	  // kick things off
	  this._create();
	}

	Flickity.defaults = {
	  accessibility: true,
	  cellAlign: 'center',
	  // cellSelector: undefined,
	  // contain: false,
	  freeScrollFriction: 0.075, // friction when free-scrolling
	  friction: 0.28, // friction when selecting
	  // initialIndex: 0,
	  percentPosition: true,
	  resize: true,
	  selectedAttraction: 0.025,
	  setGallerySize: true
	  // watchCSS: false,
	  // wrapAround: false
	};

	// hash of methods triggered on _create()
	Flickity.createMethods = [];

	// inherit EventEmitter
	utils.extend( Flickity.prototype, EventEmitter.prototype );

	Flickity.prototype._create = function() {
	  // add id for Flickity.data
	  var id = this.guid = ++GUID;
	  this.element.flickityGUID = id; // expando
	  instances[ id ] = this; // associate via id
	  // initial properties
	  this.selectedIndex = 0;
	  // how many frames slider has been in same position
	  this.restingFrames = 0;
	  // initial physics properties
	  this.x = 0;
	  this.velocity = 0;
	  this.accel = 0;
	  this.originSide = this.options.rightToLeft ? 'right' : 'left';
	  // create viewport & slider
	  this.viewport = document.createElement('div');
	  this.viewport.className = 'flickity-viewport';
	  Flickity.setUnselectable( this.viewport );
	  this._createSlider();

	  if ( this.options.resize || this.options.watchCSS ) {
	    eventie.bind( window, 'resize', this );
	    this.isResizeBound = true;
	  }

	  for ( var i=0, len = Flickity.createMethods.length; i < len; i++ ) {
	    var method = Flickity.createMethods[i];
	    this[ method ]();
	  }

	  if ( this.options.watchCSS ) {
	    this.watchCSS();
	  } else {
	    this.activate();
	  }

	};

	/**
	 * set options
	 * @param {Object} opts
	 */
	Flickity.prototype.option = function( opts ) {
	  utils.extend( this.options, opts );
	};

	Flickity.prototype.activate = function() {
	  if ( this.isActive ) {
	    return;
	  }
	  this.isActive = true;
	  classie.add( this.element, 'flickity-enabled' );
	  if ( this.options.rightToLeft ) {
	    classie.add( this.element, 'flickity-rtl' );
	  }

	  this.getSize();
	  // move initial cell elements so they can be loaded as cells
	  var cellElems = this._filterFindCellElements( this.element.children );
	  moveElements( cellElems, this.slider );
	  this.viewport.appendChild( this.slider );
	  this.element.appendChild( this.viewport );
	  // get cells from children
	  this.reloadCells();

	  if ( this.options.accessibility ) {
	    // allow element to focusable
	    this.element.tabIndex = 0;
	    // listen for key presses
	    eventie.bind( this.element, 'keydown', this );
	  }

	  this.emit('activate');

	  var index;
	  var initialIndex = this.options.initialIndex;
	  if ( this.isInitActivated ) {
	    index = this.selectedIndex;
	  } else if ( initialIndex !== undefined ) {
	    index = this.cells[ initialIndex ] ? initialIndex : 0;
	  } else {
	    index = 0;
	  }
	  // select instantly
	  this.select( index, false, true );
	  // flag for initial activation, for using initialIndex
	  this.isInitActivated = true;
	};

	// slider positions the cells
	Flickity.prototype._createSlider = function() {
	  // slider element does all the positioning
	  var slider = document.createElement('div');
	  slider.className = 'flickity-slider';
	  slider.style[ this.originSide ] = 0;
	  this.slider = slider;
	};

	Flickity.prototype._filterFindCellElements = function( elems ) {
	  return utils.filterFindElements( elems, this.options.cellSelector );
	};

	// goes through all children
	Flickity.prototype.reloadCells = function() {
	  // collection of item elements
	  this.cells = this._makeCells( this.slider.children );
	  this.positionCells();
	  this._getWrapShiftCells();
	  this.setGallerySize();
	};

	/**
	 * turn elements into Flickity.Cells
	 * @param {Array or NodeList or HTMLElement} elems
	 * @returns {Array} items - collection of new Flickity Cells
	 */
	Flickity.prototype._makeCells = function( elems ) {
	  var cellElems = this._filterFindCellElements( elems );

	  // create new Flickity for collection
	  var cells = [];
	  for ( var i=0, len = cellElems.length; i < len; i++ ) {
	    var elem = cellElems[i];
	    var cell = new Cell( elem, this );
	    cells.push( cell );
	  }

	  return cells;
	};

	Flickity.prototype.getLastCell = function() {
	  return this.cells[ this.cells.length - 1 ];
	};

	// positions all cells
	Flickity.prototype.positionCells = function() {
	  // size all cells
	  this._sizeCells( this.cells );
	  // position all cells
	  this._positionCells( 0 );
	};

	/**
	 * position certain cells
	 * @param {Integer} index - which cell to start with
	 */
	Flickity.prototype._positionCells = function( index ) {
	  index = index || 0;
	  // also measure maxCellHeight
	  // start 0 if positioning all cells
	  this.maxCellHeight = index ? this.maxCellHeight || 0 : 0;
	  var cellX = 0;
	  // get cellX
	  if ( index > 0 ) {
	    var startCell = this.cells[ index - 1 ];
	    cellX = startCell.x + startCell.size.outerWidth;
	  }
	  var cell;
	  for ( var len = this.cells.length, i=index; i < len; i++ ) {
	    cell = this.cells[i];
	    cell.setPosition( cellX );
	    cellX += cell.size.outerWidth;
	    this.maxCellHeight = Math.max( cell.size.outerHeight, this.maxCellHeight );
	  }
	  // keep track of cellX for wrap-around
	  this.slideableWidth = cellX;
	  // contain cell target
	  this._containCells();
	};

	/**
	 * cell.getSize() on multiple cells
	 * @param {Array} cells
	 */
	Flickity.prototype._sizeCells = function( cells ) {
	  for ( var i=0, len = cells.length; i < len; i++ ) {
	    var cell = cells[i];
	    cell.getSize();
	  }
	};

	// alias _init for jQuery plugin .flickity()
	Flickity.prototype._init =
	Flickity.prototype.reposition = function() {
	  this.positionCells();
	  this.positionSliderAtSelected();
	};

	Flickity.prototype.getSize = function() {
	  this.size = getSize( this.element );
	  this.setCellAlign();
	  this.cursorPosition = this.size.innerWidth * this.cellAlign;
	};

	var cellAlignShorthands = {
	  // cell align, then based on origin side
	  center: {
	    left: 0.5,
	    right: 0.5
	  },
	  left: {
	    left: 0,
	    right: 1
	  },
	  right: {
	    right: 0,
	    left: 1
	  }
	};

	Flickity.prototype.setCellAlign = function() {
	  var shorthand = cellAlignShorthands[ this.options.cellAlign ];
	  this.cellAlign = shorthand ? shorthand[ this.originSide ] : this.options.cellAlign;
	};

	Flickity.prototype.setGallerySize = function() {
	  if ( this.options.setGallerySize ) {
	    this.viewport.style.height = this.maxCellHeight + 'px';
	  }
	};

	Flickity.prototype._getWrapShiftCells = function() {
	  // only for wrap-around
	  if ( !this.options.wrapAround ) {
	    return;
	  }
	  // unshift previous cells
	  this._unshiftCells( this.beforeShiftCells );
	  this._unshiftCells( this.afterShiftCells );
	  // get before cells
	  // initial gap
	  var gapX = this.cursorPosition;
	  var cellIndex = this.cells.length - 1;
	  this.beforeShiftCells = this._getGapCells( gapX, cellIndex, -1 );
	  // get after cells
	  // ending gap between last cell and end of gallery viewport
	  gapX = this.size.innerWidth - this.cursorPosition;
	  // start cloning at first cell, working forwards
	  this.afterShiftCells = this._getGapCells( gapX, 0, 1 );
	};

	Flickity.prototype._getGapCells = function( gapX, cellIndex, increment ) {
	  // keep adding cells until the cover the initial gap
	  var cells = [];
	  while ( gapX > 0 ) {
	    var cell = this.cells[ cellIndex ];
	    if ( !cell ) {
	      break;
	    }
	    cells.push( cell );
	    cellIndex += increment;
	    gapX -= cell.size.outerWidth;
	  }
	  return cells;
	};

	// ----- contain ----- //

	// contain cell targets so no excess sliding
	Flickity.prototype._containCells = function() {
	  if ( !this.options.contain || this.options.wrapAround || !this.cells.length ) {
	    return;
	  }
	  var startMargin = this.options.rightToLeft ? 'marginRight' : 'marginLeft';
	  var endMargin = this.options.rightToLeft ? 'marginLeft' : 'marginRight';
	  var firstCellStartMargin = this.cells[0].size[ startMargin ];
	  var lastCell = this.getLastCell();
	  var contentWidth = this.slideableWidth - lastCell.size[ endMargin ];
	  var endLimit = contentWidth - this.size.innerWidth * ( 1 - this.cellAlign );
	  // content is less than gallery size
	  var isContentSmaller = contentWidth < this.size.innerWidth;
	  // contain each cell target
	  for ( var i=0, len = this.cells.length; i < len; i++ ) {
	    var cell = this.cells[i];
	    // reset default target
	    cell.setDefaultTarget();
	    if ( isContentSmaller ) {
	      // all cells fit inside gallery
	      cell.target = contentWidth * this.cellAlign;
	    } else {
	      // contain to bounds
	      cell.target = Math.max( cell.target, this.cursorPosition + firstCellStartMargin );
	      cell.target = Math.min( cell.target, endLimit );
	    }
	  }
	};

	// -----  ----- //

	/**
	 * emits events via eventEmitter and jQuery events
	 * @param {String} type - name of event
	 * @param {Event} event - original event
	 * @param {Array} args - extra arguments
	 */
	Flickity.prototype.dispatchEvent = function( type, event, args ) {
	  var emitArgs = [ event ].concat( args );
	  this.emitEvent( type, emitArgs );

	  if ( jQuery && this.$element ) {
	    if ( event ) {
	      // create jQuery event
	      var $event = jQuery.Event( event );
	      $event.type = type;
	      this.$element.trigger( $event, args );
	    } else {
	      // just trigger with type if no event available
	      this.$element.trigger( type, args );
	    }
	  }
	};

	// -------------------------- select -------------------------- //

	/**
	 * @param {Integer} index - index of the cell
	 * @param {Boolean} isWrap - will wrap-around to last/first if at the end
	 * @param {Boolean} isInstant - will immediately set position at selected cell
	 */
	Flickity.prototype.select = function( index, isWrap, isInstant ) {
	  if ( !this.isActive ) {
	    return;
	  }
	  index = parseInt( index, 10 );
	  // wrap position so slider is within normal area
	  var len = this.cells.length;
	  if ( this.options.wrapAround && len > 1 ) {
	    if ( index < 0 ) {
	      this.x -= this.slideableWidth;
	    } else if ( index >= len ) {
	      this.x += this.slideableWidth;
	    }
	  }

	  if ( this.options.wrapAround || isWrap ) {
	    index = utils.modulo( index, len );
	  }
	  // bail if invalid index
	  if ( !this.cells[ index ] ) {
	    return;
	  }
	  this.selectedIndex = index;
	  this.setSelectedCell();
	  if ( isInstant ) {
	    this.positionSliderAtSelected();
	  } else {
	    this.startAnimation();
	  }
	  this.dispatchEvent('cellSelect');
	};

	Flickity.prototype.previous = function( isWrap ) {
	  this.select( this.selectedIndex - 1, isWrap );
	};

	Flickity.prototype.next = function( isWrap ) {
	  this.select( this.selectedIndex + 1, isWrap );
	};

	Flickity.prototype.setSelectedCell = function() {
	  this._removeSelectedCellClass();
	  this.selectedCell = this.cells[ this.selectedIndex ];
	  this.selectedElement = this.selectedCell.element;
	  classie.add( this.selectedElement, 'is-selected' );
	};

	Flickity.prototype._removeSelectedCellClass = function() {
	  if ( this.selectedCell ) {
	    classie.remove( this.selectedCell.element, 'is-selected' );
	  }
	};

	// -------------------------- get cells -------------------------- //

	/**
	 * get Flickity.Cell, given an Element
	 * @param {Element} elem
	 * @returns {Flickity.Cell} item
	 */
	Flickity.prototype.getCell = function( elem ) {
	  // loop through cells to get the one that matches
	  for ( var i=0, len = this.cells.length; i < len; i++ ) {
	    var cell = this.cells[i];
	    if ( cell.element == elem ) {
	      return cell;
	    }
	  }
	};

	/**
	 * get collection of Flickity.Cells, given Elements
	 * @param {Element, Array, NodeList} elems
	 * @returns {Array} cells - Flickity.Cells
	 */
	Flickity.prototype.getCells = function( elems ) {
	  elems = utils.makeArray( elems );
	  var cells = [];
	  for ( var i=0, len = elems.length; i < len; i++ ) {
	    var elem = elems[i];
	    var cell = this.getCell( elem );
	    if ( cell ) {
	      cells.push( cell );
	    }
	  }
	  return cells;
	};

	/**
	 * get cell elements
	 * @returns {Array} cellElems
	 */
	Flickity.prototype.getCellElements = function() {
	  var cellElems = [];
	  for ( var i=0, len = this.cells.length; i < len; i++ ) {
	    cellElems.push( this.cells[i].element );
	  }
	  return cellElems;
	};

	/**
	 * get parent cell from an element
	 * @param {Element} elem
	 * @returns {Flickit.Cell} cell
	 */
	Flickity.prototype.getParentCell = function( elem ) {
	  // first check if elem is cell
	  var cell = this.getCell( elem );
	  if ( cell ) {
	    return cell;
	  }
	  // try to get parent cell elem
	  elem = utils.getParent( elem, '.flickity-slider > *' );
	  return this.getCell( elem );
	};

	/**
	 * get cells adjacent to a cell
	 * @param {Integer} adjCount - number of adjacent cells
	 * @param {Integer} index - index of cell to start
	 * @returns {Array} cells - array of Flickity.Cells
	 */
	Flickity.prototype.getAdjacentCellElements = function( adjCount, index ) {
	  if ( !adjCount ) {
	    return [ this.selectedElement ];
	  }
	  index = index === undefined ? this.selectedIndex : index;

	  var len = this.cells.length;
	  if ( 1 + ( adjCount * 2 ) >= len ) {
	    return this.getCellElements();
	  }

	  var cellElems = [];
	  for ( var i = index - adjCount; i <= index + adjCount ; i++ ) {
	    var cellIndex = this.options.wrapAround ? utils.modulo( i, len ) : i;
	    var cell = this.cells[ cellIndex ];
	    if ( cell ) {
	      cellElems.push( cell.element );
	    }
	  }
	  return cellElems;
	};

	// -------------------------- events -------------------------- //

	Flickity.prototype.uiChange = function() {
	  this.emit('uiChange');
	};

	Flickity.prototype.childUIPointerDown = function( event ) {
	  this.emitEvent( 'childUIPointerDown', [ event ] );
	};

	// ----- resize ----- //

	Flickity.prototype.onresize = function() {
	  this.watchCSS();
	  this.resize();
	};

	utils.debounceMethod( Flickity, 'onresize', 150 );

	Flickity.prototype.resize = function() {
	  if ( !this.isActive ) {
	    return;
	  }
	  this.getSize();
	  // wrap values
	  if ( this.options.wrapAround ) {
	    this.x = utils.modulo( this.x, this.slideableWidth );
	  }
	  this.positionCells();
	  this._getWrapShiftCells();
	  this.setGallerySize();
	  this.positionSliderAtSelected();
	};

	var supportsConditionalCSS = Flickity.supportsConditionalCSS = ( function() {
	  var supports;
	  return function checkSupport() {
	    if ( supports !== undefined ) {
	      return supports;
	    }
	    if ( !getComputedStyle ) {
	      supports = false;
	      return;
	    }
	    // style body's :after and check that
	    var style = document.createElement('style');
	    var cssText = document.createTextNode('body:after { content: "foo"; display: none; }');
	    style.appendChild( cssText );
	    document.head.appendChild( style );
	    var afterContent = getComputedStyle( document.body, ':after' ).content;
	    // check if able to get :after content
	    supports = afterContent.indexOf('foo') != -1;
	    document.head.removeChild( style );
	    return supports;
	  };
	})();

	// watches the :after property, activates/deactivates
	Flickity.prototype.watchCSS = function() {
	  var watchOption = this.options.watchCSS;
	  if ( !watchOption ) {
	    return;
	  }
	  var supports = supportsConditionalCSS();
	  if ( !supports ) {
	    // activate if watch option is fallbackOn
	    var method = watchOption == 'fallbackOn' ? 'activate' : 'deactivate';
	    this[ method ]();
	    return;
	  }

	  var afterContent = getComputedStyle( this.element, ':after' ).content;
	  // activate if :after { content: 'flickity' }
	  if ( afterContent.indexOf('flickity') != -1 ) {
	    this.activate();
	  } else {
	    this.deactivate();
	  }
	};

	// ----- keydown ----- //

	// go previous/next if left/right keys pressed
	Flickity.prototype.onkeydown = function( event ) {
	  // only work if element is in focus
	  if ( !this.options.accessibility ||
	    ( document.activeElement && document.activeElement != this.element ) ) {
	    return;
	  }

	  if ( event.keyCode == 37 ) {
	    // go left
	    var leftMethod = this.options.rightToLeft ? 'next' : 'previous';
	    this.uiChange();
	    this[ leftMethod ]();
	  } else if ( event.keyCode == 39 ) {
	    // go right
	    var rightMethod = this.options.rightToLeft ? 'previous' : 'next';
	    this.uiChange();
	    this[ rightMethod ]();
	  }
	};

	// -------------------------- destroy -------------------------- //

	// deactivate all Flickity functionality, but keep stuff available
	Flickity.prototype.deactivate = function() {
	  if ( !this.isActive ) {
	    return;
	  }
	  classie.remove( this.element, 'flickity-enabled' );
	  classie.remove( this.element, 'flickity-rtl' );
	  // destroy cells
	  for ( var i=0, len = this.cells.length; i < len; i++ ) {
	    var cell = this.cells[i];
	    cell.destroy();
	  }
	  this._removeSelectedCellClass();
	  this.element.removeChild( this.viewport );
	  // move child elements back into element
	  moveElements( this.slider.children, this.element );
	  if ( this.options.accessibility ) {
	    this.element.removeAttribute('tabIndex');
	    eventie.unbind( this.element, 'keydown', this );
	  }
	  // set flags
	  this.isActive = false;
	  this.emit('deactivate');
	};

	Flickity.prototype.destroy = function() {
	  this.deactivate();
	  if ( this.isResizeBound ) {
	    eventie.unbind( window, 'resize', this );
	  }
	  this.emit('destroy');
	  if ( jQuery && this.$element ) {
	    jQuery.removeData( this.element, 'flickity' );
	  }
	  delete this.element.flickityGUID;
	  delete instances[ this.guid ];
	};

	// -------------------------- prototype -------------------------- //

	utils.extend( Flickity.prototype, animatePrototype );

	// -------------------------- extras -------------------------- //

	// quick check for IE8
	var isIE8 = 'attachEvent' in window;

	Flickity.setUnselectable = function( elem ) {
	  if ( !isIE8 ) {
	    return;
	  }
	  // IE8 prevent child from changing focus http://stackoverflow.com/a/17525223/182183
	  elem.setAttribute( 'unselectable', 'on' );
	};

	/**
	 * get Flickity instance from element
	 * @param {Element} elem
	 * @returns {Flickity}
	 */
	Flickity.data = function( elem ) {
	  elem = utils.getQueryElement( elem );
	  var id = elem && elem.flickityGUID;
	  return id && instances[ id ];
	};

	utils.htmlInit( Flickity, 'flickity' );

	if ( jQuery && jQuery.bridget ) {
	  jQuery.bridget( 'flickity', Flickity );
	}

	Flickity.Cell = Cell;

	return Flickity;

	}));
	});

	var __moduleExports$13 = createCommonjsModule(function (module, exports) {
	/*!
	 * Unipointer v1.1.0
	 * base class for doing one thing with pointer event
	 * MIT license
	 */

	/*jshint browser: true, undef: true, unused: true, strict: true */
	/*global define: false, module: false, require: false */

	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'eventEmitter/EventEmitter',
	      'eventie/eventie'
	    ], function( EventEmitter, eventie ) {
	      return factory( window, EventEmitter, eventie );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$2,
	      __moduleExports$3
	    );
	  } else {
	    // browser global
	    window.Unipointer = factory(
	      window,
	      window.EventEmitter,
	      window.eventie
	    );
	  }

	}( window, function factory( window, EventEmitter, eventie ) {

	'use strict';

	function noop() {}

	function Unipointer() {}

	// inherit EventEmitter
	Unipointer.prototype = new EventEmitter();

	Unipointer.prototype.bindStartEvent = function( elem ) {
	  this._bindStartEvent( elem, true );
	};

	Unipointer.prototype.unbindStartEvent = function( elem ) {
	  this._bindStartEvent( elem, false );
	};

	/**
	 * works as unbinder, as you can ._bindStart( false ) to unbind
	 * @param {Boolean} isBind - will unbind if falsey
	 */
	Unipointer.prototype._bindStartEvent = function( elem, isBind ) {
	  // munge isBind, default to true
	  isBind = isBind === undefined ? true : !!isBind;
	  var bindMethod = isBind ? 'bind' : 'unbind';

	  if ( window.navigator.pointerEnabled ) {
	    // W3C Pointer Events, IE11. See https://coderwall.com/p/mfreca
	    eventie[ bindMethod ]( elem, 'pointerdown', this );
	  } else if ( window.navigator.msPointerEnabled ) {
	    // IE10 Pointer Events
	    eventie[ bindMethod ]( elem, 'MSPointerDown', this );
	  } else {
	    // listen for both, for devices like Chrome Pixel
	    eventie[ bindMethod ]( elem, 'mousedown', this );
	    eventie[ bindMethod ]( elem, 'touchstart', this );
	  }
	};

	// trigger handler methods for events
	Unipointer.prototype.handleEvent = function( event ) {
	  var method = 'on' + event.type;
	  if ( this[ method ] ) {
	    this[ method ]( event );
	  }
	};

	// returns the touch that we're keeping track of
	Unipointer.prototype.getTouch = function( touches ) {
	  for ( var i=0, len = touches.length; i < len; i++ ) {
	    var touch = touches[i];
	    if ( touch.identifier == this.pointerIdentifier ) {
	      return touch;
	    }
	  }
	};

	// ----- start event ----- //

	Unipointer.prototype.onmousedown = function( event ) {
	  // dismiss clicks from right or middle buttons
	  var button = event.button;
	  if ( button && ( button !== 0 && button !== 1 ) ) {
	    return;
	  }
	  this._pointerDown( event, event );
	};

	Unipointer.prototype.ontouchstart = function( event ) {
	  this._pointerDown( event, event.changedTouches[0] );
	};

	Unipointer.prototype.onMSPointerDown =
	Unipointer.prototype.onpointerdown = function( event ) {
	  this._pointerDown( event, event );
	};

	/**
	 * pointer start
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 */
	Unipointer.prototype._pointerDown = function( event, pointer ) {
	  // dismiss other pointers
	  if ( this.isPointerDown ) {
	    return;
	  }

	  this.isPointerDown = true;
	  // save pointer identifier to match up touch events
	  this.pointerIdentifier = pointer.pointerId !== undefined ?
	    // pointerId for pointer events, touch.indentifier for touch events
	    pointer.pointerId : pointer.identifier;

	  this.pointerDown( event, pointer );
	};

	Unipointer.prototype.pointerDown = function( event, pointer ) {
	  this._bindPostStartEvents( event );
	  this.emitEvent( 'pointerDown', [ event, pointer ] );
	};

	// hash of events to be bound after start event
	var postStartEvents = {
	  mousedown: [ 'mousemove', 'mouseup' ],
	  touchstart: [ 'touchmove', 'touchend', 'touchcancel' ],
	  pointerdown: [ 'pointermove', 'pointerup', 'pointercancel' ],
	  MSPointerDown: [ 'MSPointerMove', 'MSPointerUp', 'MSPointerCancel' ]
	};

	Unipointer.prototype._bindPostStartEvents = function( event ) {
	  if ( !event ) {
	    return;
	  }
	  // get proper events to match start event
	  var events = postStartEvents[ event.type ];
	  // IE8 needs to be bound to document
	  var node = event.preventDefault ? window : document;
	  // bind events to node
	  for ( var i=0, len = events.length; i < len; i++ ) {
	    var evnt = events[i];
	    eventie.bind( node, evnt, this );
	  }
	  // save these arguments
	  this._boundPointerEvents = {
	    events: events,
	    node: node
	  };
	};

	Unipointer.prototype._unbindPostStartEvents = function() {
	  var args = this._boundPointerEvents;
	  // IE8 can trigger dragEnd twice, check for _boundEvents
	  if ( !args || !args.events ) {
	    return;
	  }

	  for ( var i=0, len = args.events.length; i < len; i++ ) {
	    var event = args.events[i];
	    eventie.unbind( args.node, event, this );
	  }
	  delete this._boundPointerEvents;
	};

	// ----- move event ----- //

	Unipointer.prototype.onmousemove = function( event ) {
	  this._pointerMove( event, event );
	};

	Unipointer.prototype.onMSPointerMove =
	Unipointer.prototype.onpointermove = function( event ) {
	  if ( event.pointerId == this.pointerIdentifier ) {
	    this._pointerMove( event, event );
	  }
	};

	Unipointer.prototype.ontouchmove = function( event ) {
	  var touch = this.getTouch( event.changedTouches );
	  if ( touch ) {
	    this._pointerMove( event, touch );
	  }
	};

	/**
	 * pointer move
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 * @private
	 */
	Unipointer.prototype._pointerMove = function( event, pointer ) {
	  this.pointerMove( event, pointer );
	};

	// public
	Unipointer.prototype.pointerMove = function( event, pointer ) {
	  this.emitEvent( 'pointerMove', [ event, pointer ] );
	};

	// ----- end event ----- //


	Unipointer.prototype.onmouseup = function( event ) {
	  this._pointerUp( event, event );
	};

	Unipointer.prototype.onMSPointerUp =
	Unipointer.prototype.onpointerup = function( event ) {
	  if ( event.pointerId == this.pointerIdentifier ) {
	    this._pointerUp( event, event );
	  }
	};

	Unipointer.prototype.ontouchend = function( event ) {
	  var touch = this.getTouch( event.changedTouches );
	  if ( touch ) {
	    this._pointerUp( event, touch );
	  }
	};

	/**
	 * pointer up
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 * @private
	 */
	Unipointer.prototype._pointerUp = function( event, pointer ) {
	  this._pointerDone();
	  this.pointerUp( event, pointer );
	};

	// public
	Unipointer.prototype.pointerUp = function( event, pointer ) {
	  this.emitEvent( 'pointerUp', [ event, pointer ] );
	};

	// ----- pointer done ----- //

	// triggered on pointer up & pointer cancel
	Unipointer.prototype._pointerDone = function() {
	  // reset properties
	  this.isPointerDown = false;
	  delete this.pointerIdentifier;
	  // remove events
	  this._unbindPostStartEvents();
	  this.pointerDone();
	};

	Unipointer.prototype.pointerDone = noop;

	// ----- pointer cancel ----- //

	Unipointer.prototype.onMSPointerCancel =
	Unipointer.prototype.onpointercancel = function( event ) {
	  if ( event.pointerId == this.pointerIdentifier ) {
	    this._pointerCancel( event, event );
	  }
	};

	Unipointer.prototype.ontouchcancel = function( event ) {
	  var touch = this.getTouch( event.changedTouches );
	  if ( touch ) {
	    this._pointerCancel( event, touch );
	  }
	};

	/**
	 * pointer cancel
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 * @private
	 */
	Unipointer.prototype._pointerCancel = function( event, pointer ) {
	  this._pointerDone();
	  this.pointerCancel( event, pointer );
	};

	// public
	Unipointer.prototype.pointerCancel = function( event, pointer ) {
	  this.emitEvent( 'pointerCancel', [ event, pointer ] );
	};

	// -----  ----- //

	// utility function for getting x/y cooridinates from event, because IE8
	Unipointer.getPointerPoint = function( pointer ) {
	  return {
	    x: pointer.pageX !== undefined ? pointer.pageX : pointer.clientX,
	    y: pointer.pageY !== undefined ? pointer.pageY : pointer.clientY
	  };
	};

	// -----  ----- //

	return Unipointer;

	}));
	});

	var __moduleExports$12 = createCommonjsModule(function (module, exports) {
	/*!
	 * Unidragger v1.1.5
	 * Draggable base class
	 * MIT license
	 */

	/*jshint browser: true, unused: true, undef: true, strict: true */

	( function( window, factory ) {
	  /*global define: false, module: false, require: false */
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'eventie/eventie',
	      'unipointer/unipointer'
	    ], function( eventie, Unipointer ) {
	      return factory( window, eventie, Unipointer );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$3,
	      __moduleExports$13
	    );
	  } else {
	    // browser global
	    window.Unidragger = factory(
	      window,
	      window.eventie,
	      window.Unipointer
	    );
	  }

	}( window, function factory( window, eventie, Unipointer ) {

	'use strict';

	// -----  ----- //

	function noop() {}

	// handle IE8 prevent default
	function preventDefaultEvent( event ) {
	  if ( event.preventDefault ) {
	    event.preventDefault();
	  } else {
	    event.returnValue = false;
	  }
	}

	// -------------------------- Unidragger -------------------------- //

	function Unidragger() {}

	// inherit Unipointer & EventEmitter
	Unidragger.prototype = new Unipointer();

	// ----- bind start ----- //

	Unidragger.prototype.bindHandles = function() {
	  this._bindHandles( true );
	};

	Unidragger.prototype.unbindHandles = function() {
	  this._bindHandles( false );
	};

	var navigator = window.navigator;
	/**
	 * works as unbinder, as you can .bindHandles( false ) to unbind
	 * @param {Boolean} isBind - will unbind if falsey
	 */
	Unidragger.prototype._bindHandles = function( isBind ) {
	  // munge isBind, default to true
	  isBind = isBind === undefined ? true : !!isBind;
	  // extra bind logic
	  var binderExtra;
	  if ( navigator.pointerEnabled ) {
	    binderExtra = function( handle ) {
	      // disable scrolling on the element
	      handle.style.touchAction = isBind ? 'none' : '';
	    };
	  } else if ( navigator.msPointerEnabled ) {
	    binderExtra = function( handle ) {
	      // disable scrolling on the element
	      handle.style.msTouchAction = isBind ? 'none' : '';
	    };
	  } else {
	    binderExtra = function() {
	      // TODO re-enable img.ondragstart when unbinding
	      if ( isBind ) {
	        disableImgOndragstart( handle );
	      }
	    };
	  }
	  // bind each handle
	  var bindMethod = isBind ? 'bind' : 'unbind';
	  for ( var i=0, len = this.handles.length; i < len; i++ ) {
	    var handle = this.handles[i];
	    this._bindStartEvent( handle, isBind );
	    binderExtra( handle );
	    eventie[ bindMethod ]( handle, 'click', this );
	  }
	};

	// remove default dragging interaction on all images in IE8
	// IE8 does its own drag thing on images, which messes stuff up

	function noDragStart() {
	  return false;
	}

	// TODO replace this with a IE8 test
	var isIE8 = 'attachEvent' in document.documentElement;

	// IE8 only
	var disableImgOndragstart = !isIE8 ? noop : function( handle ) {

	  if ( handle.nodeName == 'IMG' ) {
	    handle.ondragstart = noDragStart;
	  }

	  var images = handle.querySelectorAll('img');
	  for ( var i=0, len = images.length; i < len; i++ ) {
	    var img = images[i];
	    img.ondragstart = noDragStart;
	  }
	};

	// ----- start event ----- //

	/**
	 * pointer start
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 */
	Unidragger.prototype.pointerDown = function( event, pointer ) {
	  // dismiss range sliders
	  if ( event.target.nodeName == 'INPUT' && event.target.type == 'range' ) {
	    // reset pointerDown logic
	    this.isPointerDown = false;
	    delete this.pointerIdentifier;
	    return;
	  }

	  this._dragPointerDown( event, pointer );
	  // kludge to blur focused inputs in dragger
	  var focused = document.activeElement;
	  if ( focused && focused.blur ) {
	    focused.blur();
	  }
	  // bind move and end events
	  this._bindPostStartEvents( event );
	  // track scrolling
	  this.pointerDownScroll = Unidragger.getScrollPosition();
	  eventie.bind( window, 'scroll', this );

	  this.emitEvent( 'pointerDown', [ event, pointer ] );
	};

	// base pointer down logic
	Unidragger.prototype._dragPointerDown = function( event, pointer ) {
	  // track to see when dragging starts
	  this.pointerDownPoint = Unipointer.getPointerPoint( pointer );

	  // prevent default, unless touchstart or <select>
	  var isTouchstart = event.type == 'touchstart';
	  var targetNodeName = event.target.nodeName;
	  if ( !isTouchstart && targetNodeName != 'SELECT' ) {
	    preventDefaultEvent( event );
	  }
	};

	// ----- move event ----- //

	/**
	 * drag move
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 */
	Unidragger.prototype.pointerMove = function( event, pointer ) {
	  var moveVector = this._dragPointerMove( event, pointer );
	  this.emitEvent( 'pointerMove', [ event, pointer, moveVector ] );
	  this._dragMove( event, pointer, moveVector );
	};

	// base pointer move logic
	Unidragger.prototype._dragPointerMove = function( event, pointer ) {
	  var movePoint = Unipointer.getPointerPoint( pointer );
	  var moveVector = {
	    x: movePoint.x - this.pointerDownPoint.x,
	    y: movePoint.y - this.pointerDownPoint.y
	  };
	  // start drag if pointer has moved far enough to start drag
	  if ( !this.isDragging && this.hasDragStarted( moveVector ) ) {
	    this._dragStart( event, pointer );
	  }
	  return moveVector;
	};

	// condition if pointer has moved far enough to start drag
	Unidragger.prototype.hasDragStarted = function( moveVector ) {
	  return Math.abs( moveVector.x ) > 3 || Math.abs( moveVector.y ) > 3;
	};


	// ----- end event ----- //

	/**
	 * pointer up
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 */
	Unidragger.prototype.pointerUp = function( event, pointer ) {
	  this.emitEvent( 'pointerUp', [ event, pointer ] );
	  this._dragPointerUp( event, pointer );
	};

	Unidragger.prototype._dragPointerUp = function( event, pointer ) {
	  if ( this.isDragging ) {
	    this._dragEnd( event, pointer );
	  } else {
	    // pointer didn't move enough for drag to start
	    this._staticClick( event, pointer );
	  }
	};

	Unipointer.prototype.pointerDone = function() {
	  eventie.unbind( window, 'scroll', this );
	};

	// -------------------------- drag -------------------------- //

	// dragStart
	Unidragger.prototype._dragStart = function( event, pointer ) {
	  this.isDragging = true;
	  this.dragStartPoint = Unidragger.getPointerPoint( pointer );
	  // prevent clicks
	  this.isPreventingClicks = true;

	  this.dragStart( event, pointer );
	};

	Unidragger.prototype.dragStart = function( event, pointer ) {
	  this.emitEvent( 'dragStart', [ event, pointer ] );
	};

	// dragMove
	Unidragger.prototype._dragMove = function( event, pointer, moveVector ) {
	  // do not drag if not dragging yet
	  if ( !this.isDragging ) {
	    return;
	  }

	  this.dragMove( event, pointer, moveVector );
	};

	Unidragger.prototype.dragMove = function( event, pointer, moveVector ) {
	  preventDefaultEvent( event );
	  this.emitEvent( 'dragMove', [ event, pointer, moveVector ] );
	};

	// dragEnd
	Unidragger.prototype._dragEnd = function( event, pointer ) {
	  // set flags
	  this.isDragging = false;
	  // re-enable clicking async
	  var _this = this;
	  setTimeout( function() {
	    delete _this.isPreventingClicks;
	  });

	  this.dragEnd( event, pointer );
	};

	Unidragger.prototype.dragEnd = function( event, pointer ) {
	  this.emitEvent( 'dragEnd', [ event, pointer ] );
	};

	Unidragger.prototype.pointerDone = function() {
	  eventie.unbind( window, 'scroll', this );
	  delete this.pointerDownScroll;
	};

	// ----- onclick ----- //

	// handle all clicks and prevent clicks when dragging
	Unidragger.prototype.onclick = function( event ) {
	  if ( this.isPreventingClicks ) {
	    preventDefaultEvent( event );
	  }
	};

	// ----- staticClick ----- //

	// triggered after pointer down & up with no/tiny movement
	Unidragger.prototype._staticClick = function( event, pointer ) {
	  // ignore emulated mouse up clicks
	  if ( this.isIgnoringMouseUp && event.type == 'mouseup' ) {
	    return;
	  }

	  // allow click in <input>s and <textarea>s
	  var nodeName = event.target.nodeName;
	  if ( nodeName == 'INPUT' || nodeName == 'TEXTAREA' ) {
	    event.target.focus();
	  }
	  this.staticClick( event, pointer );

	  // set flag for emulated clicks 300ms after touchend
	  if ( event.type != 'mouseup' ) {
	    this.isIgnoringMouseUp = true;
	    var _this = this;
	    // reset flag after 300ms
	    setTimeout( function() {
	      delete _this.isIgnoringMouseUp;
	    }, 400 );
	  }
	};

	Unidragger.prototype.staticClick = function( event, pointer ) {
	  this.emitEvent( 'staticClick', [ event, pointer ] );
	};

	// ----- scroll ----- //

	Unidragger.prototype.onscroll = function() {
	  var scroll = Unidragger.getScrollPosition();
	  var scrollMoveX = this.pointerDownScroll.x - scroll.x;
	  var scrollMoveY = this.pointerDownScroll.y - scroll.y;
	  // cancel click/tap if scroll is too much
	  if ( Math.abs( scrollMoveX ) > 3 || Math.abs( scrollMoveY ) > 3 ) {
	    this._pointerDone();
	  }
	};

	// ----- utils ----- //

	Unidragger.getPointerPoint = function( pointer ) {
	  return {
	    x: pointer.pageX !== undefined ? pointer.pageX : pointer.clientX,
	    y: pointer.pageY !== undefined ? pointer.pageY : pointer.clientY
	  };
	};

	var isPageOffset = window.pageYOffset !== undefined;

	// get scroll in { x, y }
	Unidragger.getScrollPosition = function() {
	  return {
	    x: isPageOffset ? window.pageXOffset : document.body.scrollLeft,
	    y: isPageOffset ? window.pageYOffset : document.body.scrollTop
	  };
	};

	// -----  ----- //

	Unidragger.getPointerPoint = Unipointer.getPointerPoint;

	return Unidragger;

	}));
	});

	var __moduleExports$11 = createCommonjsModule(function (module, exports) {
	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'classie/classie',
	      'eventie/eventie',
	      './flickity',
	      'unidragger/unidragger',
	      'fizzy-ui-utils/utils'
	    ], function( classie, eventie, Flickity, Unidragger, utils ) {
	      return factory( window, classie, eventie, Flickity, Unidragger, utils );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$1,
	      __moduleExports$3,
	      __moduleExports,
	      __moduleExports$12,
	      __moduleExports$6
	    );
	  } else {
	    // browser global
	    window.Flickity = factory(
	      window,
	      window.classie,
	      window.eventie,
	      window.Flickity,
	      window.Unidragger,
	      window.fizzyUIUtils
	    );
	  }

	}( window, function factory( window, classie, eventie, Flickity, Unidragger, utils ) {

	'use strict';

	// handle IE8 prevent default
	function preventDefaultEvent( event ) {
	  if ( event.preventDefault ) {
	    event.preventDefault();
	  } else {
	    event.returnValue = false;
	  }
	}

	// ----- defaults ----- //

	utils.extend( Flickity.defaults, {
	  draggable: true
	});

	// ----- create ----- //

	Flickity.createMethods.push('_createDrag');

	// -------------------------- drag prototype -------------------------- //

	utils.extend( Flickity.prototype, Unidragger.prototype );

	// --------------------------  -------------------------- //

	Flickity.prototype._createDrag = function() {
	  this.on( 'activate', this.bindDrag );
	  this.on( 'uiChange', this._uiChangeDrag );
	  this.on( 'childUIPointerDown', this._childUIPointerDownDrag );
	  this.on( 'deactivate', this.unbindDrag );
	};

	Flickity.prototype.bindDrag = function() {
	  if ( !this.options.draggable || this.isDragBound ) {
	    return;
	  }
	  classie.add( this.element, 'is-draggable' );
	  this.handles = [ this.viewport ];
	  this.bindHandles();
	  this.isDragBound = true;
	};

	Flickity.prototype.unbindDrag = function() {
	  if ( !this.isDragBound ) {
	    return;
	  }
	  classie.remove( this.element, 'is-draggable' );
	  this.unbindHandles();
	  delete this.isDragBound;
	};

	Flickity.prototype._uiChangeDrag = function() {
	  delete this.isFreeScrolling;
	};

	Flickity.prototype._childUIPointerDownDrag = function( event ) {
	  preventDefaultEvent( event );
	  this.pointerDownFocus( event );
	};

	// -------------------------- pointer events -------------------------- //

	Flickity.prototype.pointerDown = function( event, pointer ) {
	  // dismiss range sliders
	  if ( event.target.nodeName == 'INPUT' && event.target.type == 'range' ) {
	    // reset pointerDown logic
	    this.isPointerDown = false;
	    delete this.pointerIdentifier;
	    return;
	  }

	  this._dragPointerDown( event, pointer );

	  // kludge to blur focused inputs in dragger
	  var focused = document.activeElement;
	  if ( focused && focused.blur && focused != this.element &&
	    // do not blur body for IE9 & 10, #117
	    focused != document.body ) {
	    focused.blur();
	  }
	  this.pointerDownFocus( event );
	  // stop if it was moving
	  this.dragX = this.x;
	  classie.add( this.viewport, 'is-pointer-down' );
	  // bind move and end events
	  this._bindPostStartEvents( event );
	  // track scrolling
	  this.pointerDownScroll = Unidragger.getScrollPosition();
	  eventie.bind( window, 'scroll', this );

	  this.dispatchEvent( 'pointerDown', event, [ pointer ] );
	};

	var touchStartEvents = {
	  touchstart: true,
	  MSPointerDown: true
	};

	var focusNodes = {
	  INPUT: true,
	  SELECT: true
	};

	Flickity.prototype.pointerDownFocus = function( event ) {
	  // focus element, if not touch, and its not an input or select
	  if ( !this.options.accessibility || touchStartEvents[ event.type ] ||
	      focusNodes[ event.target.nodeName ] ) {
	    return;
	  }
	  var prevScrollY = window.pageYOffset;
	  this.element.focus();
	  // hack to fix scroll jump after focus, #76
	  if ( window.pageYOffset != prevScrollY ) {
	    window.scrollTo( window.pageXOffset, prevScrollY );
	  }
	};

	// ----- move ----- //

	Flickity.prototype.hasDragStarted = function( moveVector ) {
	  return Math.abs( moveVector.x ) > 3;
	};

	// ----- up ----- //

	Flickity.prototype.pointerUp = function( event, pointer ) {
	  classie.remove( this.viewport, 'is-pointer-down' );
	  this.dispatchEvent( 'pointerUp', event, [ pointer ] );
	  this._dragPointerUp( event, pointer );
	};

	Flickity.prototype.pointerDone = function() {
	  eventie.unbind( window, 'scroll', this );
	  delete this.pointerDownScroll;
	};

	// -------------------------- dragging -------------------------- //

	Flickity.prototype.dragStart = function( event, pointer ) {
	  this.dragStartPosition = this.x;
	  this.startAnimation();
	  this.dispatchEvent( 'dragStart', event, [ pointer ] );
	};

	Flickity.prototype.dragMove = function( event, pointer, moveVector ) {
	  preventDefaultEvent( event );

	  this.previousDragX = this.dragX;
	  // reverse if right-to-left
	  var direction = this.options.rightToLeft ? -1 : 1;
	  var dragX = this.dragStartPosition + moveVector.x * direction;

	  if ( !this.options.wrapAround && this.cells.length ) {
	    // slow drag
	    var originBound = Math.max( -this.cells[0].target, this.dragStartPosition );
	    dragX = dragX > originBound ? ( dragX + originBound ) * 0.5 : dragX;
	    var endBound = Math.min( -this.getLastCell().target, this.dragStartPosition );
	    dragX = dragX < endBound ? ( dragX + endBound ) * 0.5 : dragX;
	  }

	  this.dragX = dragX;

	  this.dragMoveTime = new Date();
	  this.dispatchEvent( 'dragMove', event, [ pointer, moveVector ] );
	};

	Flickity.prototype.dragEnd = function( event, pointer ) {
	  if ( this.options.freeScroll ) {
	    this.isFreeScrolling = true;
	  }
	  // set selectedIndex based on where flick will end up
	  var index = this.dragEndRestingSelect();

	  if ( this.options.freeScroll && !this.options.wrapAround ) {
	    // if free-scroll & not wrap around
	    // do not free-scroll if going outside of bounding cells
	    // so bounding cells can attract slider, and keep it in bounds
	    var restingX = this.getRestingPosition();
	    this.isFreeScrolling = -restingX > this.cells[0].target &&
	      -restingX < this.getLastCell().target;
	  } else if ( !this.options.freeScroll && index == this.selectedIndex ) {
	    // boost selection if selected index has not changed
	    index += this.dragEndBoostSelect();
	  }
	  delete this.previousDragX;
	  // apply selection
	  // TODO refactor this, selecting here feels weird
	  this.select( index );
	  this.dispatchEvent( 'dragEnd', event, [ pointer ] );
	};

	Flickity.prototype.dragEndRestingSelect = function() {
	  var restingX = this.getRestingPosition();
	  // how far away from selected cell
	  var distance = Math.abs( this.getCellDistance( -restingX, this.selectedIndex ) );
	  // get closet resting going up and going down
	  var positiveResting = this._getClosestResting( restingX, distance, 1 );
	  var negativeResting = this._getClosestResting( restingX, distance, -1 );
	  // use closer resting for wrap-around
	  var index = positiveResting.distance < negativeResting.distance ?
	    positiveResting.index : negativeResting.index;
	  return index;
	};

	/**
	 * given resting X and distance to selected cell
	 * get the distance and index of the closest cell
	 * @param {Number} restingX - estimated post-flick resting position
	 * @param {Number} distance - distance to selected cell
	 * @param {Integer} increment - +1 or -1, going up or down
	 * @returns {Object} - { distance: {Number}, index: {Integer} }
	 */
	Flickity.prototype._getClosestResting = function( restingX, distance, increment ) {
	  var index = this.selectedIndex;
	  var minDistance = Infinity;
	  var condition = this.options.contain && !this.options.wrapAround ?
	    // if contain, keep going if distance is equal to minDistance
	    function( d, md ) { return d <= md; } : function( d, md ) { return d < md; };
	  while ( condition( distance, minDistance ) ) {
	    // measure distance to next cell
	    index += increment;
	    minDistance = distance;
	    distance = this.getCellDistance( -restingX, index );
	    if ( distance === null ) {
	      break;
	    }
	    distance = Math.abs( distance );
	  }
	  return {
	    distance: minDistance,
	    // selected was previous index
	    index: index - increment
	  };
	};

	/**
	 * measure distance between x and a cell target
	 * @param {Number} x
	 * @param {Integer} index - cell index
	 */
	Flickity.prototype.getCellDistance = function( x, index ) {
	  var len = this.cells.length;
	  // wrap around if at least 2 cells
	  var isWrapAround = this.options.wrapAround && len > 1;
	  var cellIndex = isWrapAround ? utils.modulo( index, len ) : index;
	  var cell = this.cells[ cellIndex ];
	  if ( !cell ) {
	    return null;
	  }
	  // add distance for wrap-around cells
	  var wrap = isWrapAround ? this.slideableWidth * Math.floor( index / len ) : 0;
	  return x - ( cell.target + wrap );
	};

	Flickity.prototype.dragEndBoostSelect = function() {
	  // do not boost if no previousDragX or dragMoveTime
	  if ( this.previousDragX === undefined || !this.dragMoveTime ||
	    // or if drag was held for 100 ms
	    new Date() - this.dragMoveTime > 100 ) {
	    return 0;
	  }

	  var distance = this.getCellDistance( -this.dragX, this.selectedIndex );
	  var delta = this.previousDragX - this.dragX;
	  if ( distance > 0 && delta > 0 ) {
	    // boost to next if moving towards the right, and positive velocity
	    return 1;
	  } else if ( distance < 0 && delta < 0 ) {
	    // boost to previous if moving towards the left, and negative velocity
	    return -1;
	  }
	  return 0;
	};

	// ----- staticClick ----- //

	Flickity.prototype.staticClick = function( event, pointer ) {
	  // get clickedCell, if cell was clicked
	  var clickedCell = this.getParentCell( event.target );
	  var cellElem = clickedCell && clickedCell.element;
	  var cellIndex = clickedCell && utils.indexOf( this.cells, clickedCell );
	  this.dispatchEvent( 'staticClick', event, [ pointer, cellElem, cellIndex ] );
	};

	// -----  ----- //

	return Flickity;

	}));
	});

	var __moduleExports$15 = createCommonjsModule(function (module, exports) {
	/*!
	 * Tap listener v1.1.2
	 * listens to taps
	 * MIT license
	 */

	/*jshint browser: true, unused: true, undef: true, strict: true */

	( function( window, factory ) {
	  // universal module definition
	  /*jshint strict: false*/ /*globals define, module, require */

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'unipointer/unipointer'
	    ], function( Unipointer ) {
	      return factory( window, Unipointer );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$13
	    );
	  } else {
	    // browser global
	    window.TapListener = factory(
	      window,
	      window.Unipointer
	    );
	  }

	}( window, function factory( window, Unipointer ) {

	'use strict';

	// --------------------------  TapListener -------------------------- //

	function TapListener( elem ) {
	  this.bindTap( elem );
	}

	// inherit Unipointer & EventEmitter
	TapListener.prototype = new Unipointer();

	/**
	 * bind tap event to element
	 * @param {Element} elem
	 */
	TapListener.prototype.bindTap = function( elem ) {
	  if ( !elem ) {
	    return;
	  }
	  this.unbindTap();
	  this.tapElement = elem;
	  this._bindStartEvent( elem, true );
	};

	TapListener.prototype.unbindTap = function() {
	  if ( !this.tapElement ) {
	    return;
	  }
	  this._bindStartEvent( this.tapElement, true );
	  delete this.tapElement;
	};

	var isPageOffset = window.pageYOffset !== undefined;
	/**
	 * pointer up
	 * @param {Event} event
	 * @param {Event or Touch} pointer
	 */
	TapListener.prototype.pointerUp = function( event, pointer ) {
	  // ignore emulated mouse up clicks
	  if ( this.isIgnoringMouseUp && event.type == 'mouseup' ) {
	    return;
	  }

	  var pointerPoint = Unipointer.getPointerPoint( pointer );
	  var boundingRect = this.tapElement.getBoundingClientRect();
	  // standard or IE8 scroll positions
	  var scrollX = isPageOffset ? window.pageXOffset : document.body.scrollLeft;
	  var scrollY = isPageOffset ? window.pageYOffset : document.body.scrollTop;
	  // calculate if pointer is inside tapElement
	  var isInside = pointerPoint.x >= boundingRect.left + scrollX &&
	    pointerPoint.x <= boundingRect.right + scrollX &&
	    pointerPoint.y >= boundingRect.top + scrollY &&
	    pointerPoint.y <= boundingRect.bottom + scrollY;
	  // trigger callback if pointer is inside element
	  if ( isInside ) {
	    this.emitEvent( 'tap', [ event, pointer ] );
	  }

	  // set flag for emulated clicks 300ms after touchend
	  if ( event.type != 'mouseup' ) {
	    this.isIgnoringMouseUp = true;
	    // reset flag after 300ms
	    setTimeout( function() {
	      delete this.isIgnoringMouseUp;
	    }.bind( this ), 320 );
	  }
	};

	TapListener.prototype.destroy = function() {
	  this.pointerDone();
	  this.unbindTap();
	};

	// -----  ----- //

	return TapListener;

	}));
	});

	var __moduleExports$14 = createCommonjsModule(function (module, exports) {
	// -------------------------- prev/next button -------------------------- //

	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'eventie/eventie',
	      './flickity',
	      'tap-listener/tap-listener',
	      'fizzy-ui-utils/utils'
	    ], function( eventie, Flickity, TapListener, utils ) {
	      return factory( window, eventie, Flickity, TapListener, utils );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$3,
	      __moduleExports,
	      __moduleExports$15,
	      __moduleExports$6
	    );
	  } else {
	    // browser global
	    factory(
	      window,
	      window.eventie,
	      window.Flickity,
	      window.TapListener,
	      window.fizzyUIUtils
	    );
	  }

	}( window, function factory( window, eventie, Flickity, TapListener, utils ) {

	'use strict';

	// ----- inline SVG support ----- //

	var svgURI = 'http://www.w3.org/2000/svg';

	// only check on demand, not on script load
	var supportsInlineSVG = ( function() {
	  var supports;
	  function checkSupport() {
	    if ( supports !== undefined ) {
	      return supports;
	    }
	    var div = document.createElement('div');
	    div.innerHTML = '<svg/>';
	    supports = ( div.firstChild && div.firstChild.namespaceURI ) == svgURI;
	    return supports;
	  }
	  return checkSupport;
	})();

	// -------------------------- PrevNextButton -------------------------- //

	function PrevNextButton( direction, parent ) {
	  this.direction = direction;
	  this.parent = parent;
	  this._create();
	}

	PrevNextButton.prototype = new TapListener();

	PrevNextButton.prototype._create = function() {
	  // properties
	  this.isEnabled = true;
	  this.isPrevious = this.direction == -1;
	  var leftDirection = this.parent.options.rightToLeft ? 1 : -1;
	  this.isLeft = this.direction == leftDirection;

	  var element = this.element = document.createElement('button');
	  element.className = 'flickity-prev-next-button';
	  element.className += this.isPrevious ? ' previous' : ' next';
	  // prevent button from submitting form http://stackoverflow.com/a/10836076/182183
	  element.setAttribute( 'type', 'button' );
	  // init as disabled
	  this.disable();

	  element.setAttribute( 'aria-label', this.isPrevious ? 'previous' : 'next' );

	  Flickity.setUnselectable( element );
	  // create arrow
	  if ( supportsInlineSVG() ) {
	    var svg = this.createSVG();
	    element.appendChild( svg );
	  } else {
	    // SVG not supported, set button text
	    this.setArrowText();
	    element.className += ' no-svg';
	  }
	  // update on select
	  var _this = this;
	  this.onCellSelect = function() {
	    _this.update();
	  };
	  this.parent.on( 'cellSelect', this.onCellSelect );
	  // tap
	  this.on( 'tap', this.onTap );
	  // pointerDown
	  this.on( 'pointerDown', function onPointerDown( button, event ) {
	    _this.parent.childUIPointerDown( event );
	  });
	};

	PrevNextButton.prototype.activate = function() {
	  this.bindTap( this.element );
	  // click events from keyboard
	  eventie.bind( this.element, 'click', this );
	  // add to DOM
	  this.parent.element.appendChild( this.element );
	};

	PrevNextButton.prototype.deactivate = function() {
	  // remove from DOM
	  this.parent.element.removeChild( this.element );
	  // do regular TapListener destroy
	  TapListener.prototype.destroy.call( this );
	  // click events from keyboard
	  eventie.unbind( this.element, 'click', this );
	};

	PrevNextButton.prototype.createSVG = function() {
	  var svg = document.createElementNS( svgURI, 'svg');
	  svg.setAttribute( 'viewBox', '0 0 100 100' );
	  var path = document.createElementNS( svgURI, 'path');
	  var pathMovements = getArrowMovements( this.parent.options.arrowShape );
	  path.setAttribute( 'd', pathMovements );
	  path.setAttribute( 'class', 'arrow' );
	  // rotate arrow
	  if ( !this.isLeft ) {
	    path.setAttribute( 'transform', 'translate(100, 100) rotate(180) ' );
	  }
	  svg.appendChild( path );
	  return svg;
	};

	// get SVG path movmement
	function getArrowMovements( shape ) {
	  // use shape as movement if string
	  if ( typeof shape == 'string' ) {
	    return shape;
	  }
	  // create movement string
	  return 'M ' + shape.x0 + ',50' +
	    ' L ' + shape.x1 + ',' + ( shape.y1 + 50 ) +
	    ' L ' + shape.x2 + ',' + ( shape.y2 + 50 ) +
	    ' L ' + shape.x3 + ',50 ' +
	    ' L ' + shape.x2 + ',' + ( 50 - shape.y2 ) +
	    ' L ' + shape.x1 + ',' + ( 50 - shape.y1 ) +
	    ' Z';
	}

	PrevNextButton.prototype.setArrowText = function() {
	  var parentOptions = this.parent.options;
	  var arrowText = this.isLeft ? parentOptions.leftArrowText : parentOptions.rightArrowText;
	  utils.setText( this.element, arrowText );
	};

	PrevNextButton.prototype.onTap = function() {
	  if ( !this.isEnabled ) {
	    return;
	  }
	  this.parent.uiChange();
	  var method = this.isPrevious ? 'previous' : 'next';
	  this.parent[ method ]();
	};

	PrevNextButton.prototype.handleEvent = utils.handleEvent;

	PrevNextButton.prototype.onclick = function() {
	  // only allow clicks from keyboard
	  var focused = document.activeElement;
	  if ( focused && focused == this.element ) {
	    this.onTap();
	  }
	};

	// -----  ----- //

	PrevNextButton.prototype.enable = function() {
	  if ( this.isEnabled ) {
	    return;
	  }
	  this.element.disabled = false;
	  this.isEnabled = true;
	};

	PrevNextButton.prototype.disable = function() {
	  if ( !this.isEnabled ) {
	    return;
	  }
	  this.element.disabled = true;
	  this.isEnabled = false;
	};

	PrevNextButton.prototype.update = function() {
	  // index of first or last cell, if previous or next
	  var cells = this.parent.cells;
	  // enable is wrapAround and at least 2 cells
	  if ( this.parent.options.wrapAround && cells.length > 1 ) {
	    this.enable();
	    return;
	  }
	  var lastIndex = cells.length ? cells.length - 1 : 0;
	  var boundIndex = this.isPrevious ? 0 : lastIndex;
	  var method = this.parent.selectedIndex == boundIndex ? 'disable' : 'enable';
	  this[ method ]();
	};

	PrevNextButton.prototype.destroy = function() {
	  this.deactivate();
	};

	// -------------------------- Flickity prototype -------------------------- //

	utils.extend( Flickity.defaults, {
	  prevNextButtons: true,
	  leftArrowText: '‹',
	  rightArrowText: '›',
	  arrowShape: {
	    x0: 10,
	    x1: 60, y1: 50,
	    x2: 70, y2: 40,
	    x3: 30
	  }
	});

	Flickity.createMethods.push('_createPrevNextButtons');

	Flickity.prototype._createPrevNextButtons = function() {
	  if ( !this.options.prevNextButtons ) {
	    return;
	  }

	  this.prevButton = new PrevNextButton( -1, this );
	  this.nextButton = new PrevNextButton( 1, this );

	  this.on( 'activate', this.activatePrevNextButtons );
	};

	Flickity.prototype.activatePrevNextButtons = function() {
	  this.prevButton.activate();
	  this.nextButton.activate();
	  this.on( 'deactivate', this.deactivatePrevNextButtons );
	};

	Flickity.prototype.deactivatePrevNextButtons = function() {
	  this.prevButton.deactivate();
	  this.nextButton.deactivate();
	  this.off( 'deactivate', this.deactivatePrevNextButtons );
	};

	// --------------------------  -------------------------- //

	Flickity.PrevNextButton = PrevNextButton;

	return Flickity;

	}));
	});

	var __moduleExports$16 = createCommonjsModule(function (module, exports) {
	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'eventie/eventie',
	      './flickity',
	      'tap-listener/tap-listener',
	      'fizzy-ui-utils/utils'
	    ], function( eventie, Flickity, TapListener, utils ) {
	      return factory( window, eventie, Flickity, TapListener, utils );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$3,
	      __moduleExports,
	      __moduleExports$15,
	      __moduleExports$6
	    );
	  } else {
	    // browser global
	    factory(
	      window,
	      window.eventie,
	      window.Flickity,
	      window.TapListener,
	      window.fizzyUIUtils
	    );
	  }

	}( window, function factory( window, eventie, Flickity, TapListener, utils ) {

	// -------------------------- PageDots -------------------------- //

	'use strict';

	function PageDots( parent ) {
	  this.parent = parent;
	  this._create();
	}

	PageDots.prototype = new TapListener();

	PageDots.prototype._create = function() {
	  // create holder element
	  this.holder = document.createElement('ol');
	  this.holder.className = 'flickity-page-dots';
	  Flickity.setUnselectable( this.holder );
	  // create dots, array of elements
	  this.dots = [];
	  // update on select
	  var _this = this;
	  this.onCellSelect = function() {
	    _this.updateSelected();
	  };
	  this.parent.on( 'cellSelect', this.onCellSelect );
	  // tap
	  this.on( 'tap', this.onTap );
	  // pointerDown
	  this.on( 'pointerDown', function onPointerDown( button, event ) {
	    _this.parent.childUIPointerDown( event );
	  });
	};

	PageDots.prototype.activate = function() {
	  this.setDots();
	  this.bindTap( this.holder );
	  // add to DOM
	  this.parent.element.appendChild( this.holder );
	};

	PageDots.prototype.deactivate = function() {
	  // remove from DOM
	  this.parent.element.removeChild( this.holder );
	  TapListener.prototype.destroy.call( this );
	};

	PageDots.prototype.setDots = function() {
	  // get difference between number of cells and number of dots
	  var delta = this.parent.cells.length - this.dots.length;
	  if ( delta > 0 ) {
	    this.addDots( delta );
	  } else if ( delta < 0 ) {
	    this.removeDots( -delta );
	  }
	};

	PageDots.prototype.addDots = function( count ) {
	  var fragment = document.createDocumentFragment();
	  var newDots = [];
	  while ( count ) {
	    var dot = document.createElement('li');
	    dot.className = 'dot';
	    fragment.appendChild( dot );
	    newDots.push( dot );
	    count--;
	  }
	  this.holder.appendChild( fragment );
	  this.dots = this.dots.concat( newDots );
	};

	PageDots.prototype.removeDots = function( count ) {
	  // remove from this.dots collection
	  var removeDots = this.dots.splice( this.dots.length - count, count );
	  // remove from DOM
	  for ( var i=0, len = removeDots.length; i < len; i++ ) {
	    var dot = removeDots[i];
	    this.holder.removeChild( dot );
	  }
	};

	PageDots.prototype.updateSelected = function() {
	  // remove selected class on previous
	  if ( this.selectedDot ) {
	    this.selectedDot.className = 'dot';
	  }
	  // don't proceed if no dots
	  if ( !this.dots.length ) {
	    return;
	  }
	  this.selectedDot = this.dots[ this.parent.selectedIndex ];
	  this.selectedDot.className = 'dot is-selected';
	};

	PageDots.prototype.onTap = function( event ) {
	  var target = event.target;
	  // only care about dot clicks
	  if ( target.nodeName != 'LI' ) {
	    return;
	  }

	  this.parent.uiChange();
	  var index = utils.indexOf( this.dots, target );
	  this.parent.select( index );
	};

	PageDots.prototype.destroy = function() {
	  this.deactivate();
	};

	Flickity.PageDots = PageDots;

	// -------------------------- Flickity -------------------------- //

	utils.extend( Flickity.defaults, {
	  pageDots: true
	});

	Flickity.createMethods.push('_createPageDots');

	Flickity.prototype._createPageDots = function() {
	  if ( !this.options.pageDots ) {
	    return;
	  }
	  this.pageDots = new PageDots( this );
	  this.on( 'activate', this.activatePageDots );
	  this.on( 'cellAddedRemoved', this.onCellAddedRemovedPageDots );
	  this.on( 'deactivate', this.deactivatePageDots );
	};

	Flickity.prototype.activatePageDots = function() {
	  this.pageDots.activate();
	};

	Flickity.prototype.onCellAddedRemovedPageDots = function() {
	  this.pageDots.setDots();
	};

	Flickity.prototype.deactivatePageDots = function() {
	  this.pageDots.deactivate();
	};

	// -----  ----- //

	Flickity.PageDots = PageDots;

	return Flickity;

	}));
	});

	var __moduleExports$17 = createCommonjsModule(function (module, exports) {
	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'eventEmitter/EventEmitter',
	      'eventie/eventie',
	      'fizzy-ui-utils/utils',
	      './flickity'
	    ], function( EventEmitter, eventie, utils, Flickity ) {
	      return factory( EventEmitter, eventie, utils, Flickity );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      __moduleExports$2,
	      __moduleExports$3,
	      __moduleExports$6,
	      __moduleExports
	    );
	  } else {
	    // browser global
	    factory(
	      window.EventEmitter,
	      window.eventie,
	      window.fizzyUIUtils,
	      window.Flickity
	    );
	  }

	}( window, function factory( EventEmitter, eventie, utils, Flickity ) {

	'use strict';

	// -------------------------- Page Visibility -------------------------- //
	// https://developer.mozilla.org/en-US/docs/Web/Guide/User_experience/Using_the_Page_Visibility_API

	var hiddenProperty, visibilityEvent;
	if ( 'hidden' in document ) {
	  hiddenProperty = 'hidden';
	  visibilityEvent = 'visibilitychange';
	} else if ( 'webkitHidden' in document ) {
	  hiddenProperty = 'webkitHidden';
	  visibilityEvent = 'webkitvisibilitychange';
	}

	// -------------------------- Player -------------------------- //

	function Player( parent ) {
	  this.parent = parent;
	  this.state = 'stopped';
	  // visibility change event handler
	  if ( visibilityEvent ) {
	    var _this = this;
	    this.onVisibilityChange = function() {
	      _this.visibilityChange();
	    };
	  }
	}

	Player.prototype = new EventEmitter();

	// start play
	Player.prototype.play = function() {
	  if ( this.state == 'playing' ) {
	    return;
	  }
	  this.state = 'playing';
	  // listen to visibility change
	  if ( visibilityEvent ) {
	    document.addEventListener( visibilityEvent, this.onVisibilityChange, false );
	  }
	  // start ticking
	  this.tick();
	};

	Player.prototype.tick = function() {
	  // do not tick if not playing
	  if ( this.state != 'playing' ) {
	    return;
	  }

	  var time = this.parent.options.autoPlay;
	  // default to 3 seconds
	  time = typeof time == 'number' ? time : 3000;
	  var _this = this;
	  // HACK: reset ticks if stopped and started within interval
	  this.clear();
	  this.timeout = setTimeout( function() {
	    _this.parent.next( true );
	    _this.tick();
	  }, time );
	};

	Player.prototype.stop = function() {
	  this.state = 'stopped';
	  this.clear();
	  // remove visibility change event
	  if ( visibilityEvent ) {
	    document.removeEventListener( visibilityEvent, this.onVisibilityChange, false );
	  }
	};

	Player.prototype.clear = function() {
	  clearTimeout( this.timeout );
	};

	Player.prototype.pause = function() {
	  if ( this.state == 'playing' ) {
	    this.state = 'paused';
	    this.clear();
	  }
	};

	Player.prototype.unpause = function() {
	  // re-start play if in unpaused state
	  if ( this.state == 'paused' ) {
	    this.play();
	  }
	};

	// pause if page visibility is hidden, unpause if visible
	Player.prototype.visibilityChange = function() {
	  var isHidden = document[ hiddenProperty ];
	  this[ isHidden ? 'pause' : 'unpause' ]();
	};

	// -------------------------- Flickity -------------------------- //

	utils.extend( Flickity.defaults, {
	  pauseAutoPlayOnHover: true
	});

	Flickity.createMethods.push('_createPlayer');

	Flickity.prototype._createPlayer = function() {
	  this.player = new Player( this );

	  this.on( 'activate', this.activatePlayer );
	  this.on( 'uiChange', this.stopPlayer );
	  this.on( 'pointerDown', this.stopPlayer );
	  this.on( 'deactivate', this.deactivatePlayer );
	};

	Flickity.prototype.activatePlayer = function() {
	  if ( !this.options.autoPlay ) {
	    return;
	  }
	  this.player.play();
	  eventie.bind( this.element, 'mouseenter', this );
	  this.isMouseenterBound = true;
	};

	// Player API, don't hate the ... thanks I know where the door is

	Flickity.prototype.playPlayer = function() {
	  this.player.play();
	};

	Flickity.prototype.stopPlayer = function() {
	  this.player.stop();
	};

	Flickity.prototype.pausePlayer = function() {
	  this.player.pause();
	};

	Flickity.prototype.unpausePlayer = function() {
	  this.player.unpause();
	};

	Flickity.prototype.deactivatePlayer = function() {
	  this.player.stop();
	  if ( this.isMouseenterBound ) {
	    eventie.unbind( this.element, 'mouseenter', this );
	    delete this.isMouseenterBound;
	  }
	};

	// ----- mouseenter/leave ----- //

	// pause auto-play on hover
	Flickity.prototype.onmouseenter = function() {
	  if ( !this.options.pauseAutoPlayOnHover ) {
	    return;
	  }
	  this.player.pause();
	  eventie.bind( this.element, 'mouseleave', this );
	};

	// resume auto-play on hover off
	Flickity.prototype.onmouseleave = function() {
	  this.player.unpause();
	  eventie.unbind( this.element, 'mouseleave', this );
	};

	// -----  ----- //

	Flickity.Player = Player;

	return Flickity;

	}));
	});

	var __moduleExports$18 = createCommonjsModule(function (module, exports) {
	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      './flickity',
	      'fizzy-ui-utils/utils'
	    ], function( Flickity, utils ) {
	      return factory( window, Flickity, utils );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports,
	      __moduleExports$6
	    );
	  } else {
	    // browser global
	    factory(
	      window,
	      window.Flickity,
	      window.fizzyUIUtils
	    );
	  }

	}( window, function factory( window, Flickity, utils ) {

	'use strict';

	// append cells to a document fragment
	function getCellsFragment( cells ) {
	  var fragment = document.createDocumentFragment();
	  for ( var i=0, len = cells.length; i < len; i++ ) {
	    var cell = cells[i];
	    fragment.appendChild( cell.element );
	  }
	  return fragment;
	}

	// -------------------------- add/remove cell prototype -------------------------- //

	/**
	 * Insert, prepend, or append cells
	 * @param {Element, Array, NodeList} elems
	 * @param {Integer} index
	 */
	Flickity.prototype.insert = function( elems, index ) {
	  var cells = this._makeCells( elems );
	  if ( !cells || !cells.length ) {
	    return;
	  }
	  var len = this.cells.length;
	  // default to append
	  index = index === undefined ? len : index;
	  // add cells with document fragment
	  var fragment = getCellsFragment( cells );
	  // append to slider
	  var isAppend = index == len;
	  if ( isAppend ) {
	    this.slider.appendChild( fragment );
	  } else {
	    var insertCellElement = this.cells[ index ].element;
	    this.slider.insertBefore( fragment, insertCellElement );
	  }
	  // add to this.cells
	  if ( index === 0 ) {
	    // prepend, add to start
	    this.cells = cells.concat( this.cells );
	  } else if ( isAppend ) {
	    // append, add to end
	    this.cells = this.cells.concat( cells );
	  } else {
	    // insert in this.cells
	    var endCells = this.cells.splice( index, len - index );
	    this.cells = this.cells.concat( cells ).concat( endCells );
	  }

	  this._sizeCells( cells );

	  var selectedIndexDelta = index > this.selectedIndex ? 0 : cells.length;
	  this._cellAddedRemoved( index, selectedIndexDelta );
	};

	Flickity.prototype.append = function( elems ) {
	  this.insert( elems, this.cells.length );
	};

	Flickity.prototype.prepend = function( elems ) {
	  this.insert( elems, 0 );
	};

	/**
	 * Remove cells
	 * @param {Element, Array, NodeList} elems
	 */
	Flickity.prototype.remove = function( elems ) {
	  var cells = this.getCells( elems );
	  var selectedIndexDelta = 0;
	  var i, len, cell;
	  // calculate selectedIndexDelta, easier if done in seperate loop
	  for ( i=0, len = cells.length; i < len; i++ ) {
	    cell = cells[i];
	    var wasBefore = utils.indexOf( this.cells, cell ) < this.selectedIndex;
	    selectedIndexDelta -= wasBefore ? 1 : 0;
	  }

	  for ( i=0, len = cells.length; i < len; i++ ) {
	    cell = cells[i];
	    cell.remove();
	    // remove item from collection
	    utils.removeFrom( this.cells, cell );
	  }

	  if ( cells.length ) {
	    // update stuff
	    this._cellAddedRemoved( 0, selectedIndexDelta );
	  }
	};

	// updates when cells are added or removed
	Flickity.prototype._cellAddedRemoved = function( changedCellIndex, selectedIndexDelta ) {
	  selectedIndexDelta = selectedIndexDelta || 0;
	  this.selectedIndex += selectedIndexDelta;
	  this.selectedIndex = Math.max( 0, Math.min( this.cells.length - 1, this.selectedIndex ) );

	  this.emitEvent( 'cellAddedRemoved', [ changedCellIndex, selectedIndexDelta ] );
	  this.cellChange( changedCellIndex, true );
	};

	/**
	 * logic to be run after a cell's size changes
	 * @param {Element} elem - cell's element
	 */
	Flickity.prototype.cellSizeChange = function( elem ) {
	  var cell = this.getCell( elem );
	  if ( !cell ) {
	    return;
	  }
	  cell.getSize();

	  var index = utils.indexOf( this.cells, cell );
	  this.cellChange( index );
	};

	/**
	 * logic any time a cell is changed: added, removed, or size changed
	 * @param {Integer} changedCellIndex - index of the changed cell, optional
	 */
	Flickity.prototype.cellChange = function( changedCellIndex, isPositioningSlider ) {
	  var prevSlideableWidth = this.slideableWidth;
	  this._positionCells( changedCellIndex );
	  this._getWrapShiftCells();
	  this.setGallerySize();
	  // position slider
	  if ( this.options.freeScroll ) {
	    // shift x by change in slideableWidth
	    // TODO fix position shifts when prepending w/ freeScroll
	    var deltaX = prevSlideableWidth - this.slideableWidth;
	    this.x += deltaX * this.cellAlign;
	    this.positionSlider();
	  } else {
	    // do not position slider after lazy load
	    if ( isPositioningSlider ) {
	      this.positionSliderAtSelected();
	    }
	    this.select( this.selectedIndex );
	  }
	};

	// -----  ----- //

	return Flickity;

	}));
	});

	var __moduleExports$19 = createCommonjsModule(function (module, exports) {
	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'classie/classie',
	      'eventie/eventie',
	      './flickity',
	      'fizzy-ui-utils/utils'
	    ], function( classie, eventie, Flickity, utils ) {
	      return factory( window, classie, eventie, Flickity, utils );
	    });
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$1,
	      __moduleExports$3,
	      __moduleExports,
	      __moduleExports$6
	    );
	  } else {
	    // browser global
	    factory(
	      window,
	      window.classie,
	      window.eventie,
	      window.Flickity,
	      window.fizzyUIUtils
	    );
	  }

	}( window, function factory( window, classie, eventie, Flickity, utils ) {
	'use strict';

	Flickity.createMethods.push('_createLazyload');

	Flickity.prototype._createLazyload = function() {
	  this.on( 'cellSelect', this.lazyLoad );
	};

	Flickity.prototype.lazyLoad = function() {
	  var lazyLoad = this.options.lazyLoad;
	  if ( !lazyLoad ) {
	    return;
	  }
	  // get adjacent cells, use lazyLoad option for adjacent count
	  var adjCount = typeof lazyLoad == 'number' ? lazyLoad : 0;
	  var cellElems = this.getAdjacentCellElements( adjCount );
	  // get lazy images in those cells
	  var lazyImages = [];
	  for ( var i=0, len = cellElems.length; i < len; i++ ) {
	    var cellElem = cellElems[i];
	    var lazyCellImages = getCellLazyImages( cellElem );
	    lazyImages = lazyImages.concat( lazyCellImages );
	  }
	  // load lazy images
	  for ( i=0, len = lazyImages.length; i < len; i++ ) {
	    var img = lazyImages[i];
	    new LazyLoader( img, this );
	  }
	};

	function getCellLazyImages( cellElem ) {
	  // check if cell element is lazy image
	  if ( cellElem.nodeName == 'IMG' &&
	    cellElem.getAttribute('data-flickity-lazyload') ) {
	    return [ cellElem ];
	  }
	  // select lazy images in cell
	  var imgs = cellElem.querySelectorAll('img[data-flickity-lazyload]');
	  return utils.makeArray( imgs );
	}

	// -------------------------- LazyLoader -------------------------- //

	/**
	 * class to handle loading images
	 */
	function LazyLoader( img, flickity ) {
	  this.img = img;
	  this.flickity = flickity;
	  this.load();
	}

	LazyLoader.prototype.handleEvent = utils.handleEvent;

	LazyLoader.prototype.load = function() {
	  eventie.bind( this.img, 'load', this );
	  eventie.bind( this.img, 'error', this );
	  // load image
	  this.img.src = this.img.getAttribute('data-flickity-lazyload');
	  // remove attr
	  this.img.removeAttribute('data-flickity-lazyload');
	};

	LazyLoader.prototype.onload = function( event ) {
	  this.complete( event, 'flickity-lazyloaded' );
	};

	LazyLoader.prototype.onerror = function( event ) {
	  this.complete( event, 'flickity-lazyerror' );
	};

	LazyLoader.prototype.complete = function( event, className ) {
	  // unbind events
	  eventie.unbind( this.img, 'load', this );
	  eventie.unbind( this.img, 'error', this );

	  var cell = this.flickity.getParentCell( this.img );
	  var cellElem = cell && cell.element;
	  this.flickity.cellSizeChange( cellElem );

	  classie.add( this.img, className );
	  this.flickity.dispatchEvent( 'lazyLoad', event, cellElem );
	};

	// -----  ----- //

	Flickity.LazyLoader = LazyLoader;

	return Flickity;

	}));
	});

	var index$2 = createCommonjsModule(function (module, exports) {
	/*!
	 * Flickity v1.2.1
	 * Touch, responsive, flickable galleries
	 *
	 * Licensed GPLv3 for open source use
	 * or Flickity Commercial License for commercial use
	 *
	 * http://flickity.metafizzy.co
	 * Copyright 2015 Metafizzy
	 */

	( function( window, factory ) {
	  'use strict';
	  // universal module definition

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      './flickity',
	      './drag',
	      './prev-next-button',
	      './page-dots',
	      './player',
	      './add-remove-cell',
	      './lazyload'
	    ], factory );
	  } else if ( typeof exports == 'object' ) {
	    // CommonJS
	    module.exports = factory(
	      __moduleExports,
	      __moduleExports$11,
	      __moduleExports$14,
	      __moduleExports$16,
	      __moduleExports$17,
	      __moduleExports$18,
	      __moduleExports$19
	    );
	  }

	})( window, function factory( Flickity ) {
	  /*jshint strict: false*/
	  return Flickity;
	});
	});

	/**
	 * Locates an SVG icon based on its tag and returns the shape from the first `<path>`
	 * @param iconName
	 * @returns {*}
	 */
	function svgPath(iconName) {
	  var $icon = $('#' + iconName);
	  var $path = $icon.find('path').eq(0);

	  return $path.attr('d');
	}

	var __moduleExports$20 = createCommonjsModule(function (module) {
	/**
	 * EvEmitter v1.0.3
	 * Lil' event emitter
	 * MIT License
	 */

	/* jshint unused: true, undef: true, strict: true */

	( function( global, factory ) {
	  // universal module definition
	  /* jshint strict: false */ /* globals define, module, window */
	  if ( typeof define == 'function' && define.amd ) {
	    // AMD - RequireJS
	    define( factory );
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS - Browserify, Webpack
	    module.exports = factory();
	  } else {
	    // Browser globals
	    global.EvEmitter = factory();
	  }

	}( typeof window != 'undefined' ? window : commonjsGlobal, function() {

	"use strict";

	function EvEmitter() {}

	var proto = EvEmitter.prototype;

	proto.on = function( eventName, listener ) {
	  if ( !eventName || !listener ) {
	    return;
	  }
	  // set events hash
	  var events = this._events = this._events || {};
	  // set listeners array
	  var listeners = events[ eventName ] = events[ eventName ] || [];
	  // only add once
	  if ( listeners.indexOf( listener ) == -1 ) {
	    listeners.push( listener );
	  }

	  return this;
	};

	proto.once = function( eventName, listener ) {
	  if ( !eventName || !listener ) {
	    return;
	  }
	  // add event
	  this.on( eventName, listener );
	  // set once flag
	  // set onceEvents hash
	  var onceEvents = this._onceEvents = this._onceEvents || {};
	  // set onceListeners object
	  var onceListeners = onceEvents[ eventName ] = onceEvents[ eventName ] || {};
	  // set flag
	  onceListeners[ listener ] = true;

	  return this;
	};

	proto.off = function( eventName, listener ) {
	  var listeners = this._events && this._events[ eventName ];
	  if ( !listeners || !listeners.length ) {
	    return;
	  }
	  var index = listeners.indexOf( listener );
	  if ( index != -1 ) {
	    listeners.splice( index, 1 );
	  }

	  return this;
	};

	proto.emitEvent = function( eventName, args ) {
	  var listeners = this._events && this._events[ eventName ];
	  if ( !listeners || !listeners.length ) {
	    return;
	  }
	  var i = 0;
	  var listener = listeners[i];
	  args = args || [];
	  // once stuff
	  var onceListeners = this._onceEvents && this._onceEvents[ eventName ];

	  while ( listener ) {
	    var isOnce = onceListeners && onceListeners[ listener ];
	    if ( isOnce ) {
	      // remove listener
	      // remove before trigger to prevent recursion
	      this.off( eventName, listener );
	      // unset once flag
	      delete onceListeners[ listener ];
	    }
	    // trigger listener
	    listener.apply( this, args );
	    // get next listener
	    i += isOnce ? 0 : 1;
	    listener = listeners[i];
	  }

	  return this;
	};

	return EvEmitter;

	}));
	});

	var imagesloaded = createCommonjsModule(function (module) {
	/*!
	 * imagesLoaded v4.1.1
	 * JavaScript is all like "You images are done yet or what?"
	 * MIT License
	 */

	( function( window, factory ) { 'use strict';
	  // universal module definition

	  /*global define: false, module: false, require: false */

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'ev-emitter/ev-emitter'
	    ], function( EvEmitter ) {
	      return factory( window, EvEmitter );
	    });
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$20
	    );
	  } else {
	    // browser global
	    window.imagesLoaded = factory(
	      window,
	      window.EvEmitter
	    );
	  }

	})( window,

	// --------------------------  factory -------------------------- //

	function factory( window, EvEmitter ) {

	'use strict';

	var $ = window.jQuery;
	var console = window.console;

	// -------------------------- helpers -------------------------- //

	// extend objects
	function extend( a, b ) {
	  for ( var prop in b ) {
	    a[ prop ] = b[ prop ];
	  }
	  return a;
	}

	// turn element or nodeList into an array
	function makeArray( obj ) {
	  var ary = [];
	  if ( Array.isArray( obj ) ) {
	    // use object if already an array
	    ary = obj;
	  } else if ( typeof obj.length == 'number' ) {
	    // convert nodeList to array
	    for ( var i=0; i < obj.length; i++ ) {
	      ary.push( obj[i] );
	    }
	  } else {
	    // array of single index
	    ary.push( obj );
	  }
	  return ary;
	}

	// -------------------------- imagesLoaded -------------------------- //

	/**
	 * @param {Array, Element, NodeList, String} elem
	 * @param {Object or Function} options - if function, use as callback
	 * @param {Function} onAlways - callback function
	 */
	function ImagesLoaded( elem, options, onAlways ) {
	  // coerce ImagesLoaded() without new, to be new ImagesLoaded()
	  if ( !( this instanceof ImagesLoaded ) ) {
	    return new ImagesLoaded( elem, options, onAlways );
	  }
	  // use elem as selector string
	  if ( typeof elem == 'string' ) {
	    elem = document.querySelectorAll( elem );
	  }

	  this.elements = makeArray( elem );
	  this.options = extend( {}, this.options );

	  if ( typeof options == 'function' ) {
	    onAlways = options;
	  } else {
	    extend( this.options, options );
	  }

	  if ( onAlways ) {
	    this.on( 'always', onAlways );
	  }

	  this.getImages();

	  if ( $ ) {
	    // add jQuery Deferred object
	    this.jqDeferred = new $.Deferred();
	  }

	  // HACK check async to allow time to bind listeners
	  setTimeout( function() {
	    this.check();
	  }.bind( this ));
	}

	ImagesLoaded.prototype = Object.create( EvEmitter.prototype );

	ImagesLoaded.prototype.options = {};

	ImagesLoaded.prototype.getImages = function() {
	  this.images = [];

	  // filter & find items if we have an item selector
	  this.elements.forEach( this.addElementImages, this );
	};

	/**
	 * @param {Node} element
	 */
	ImagesLoaded.prototype.addElementImages = function( elem ) {
	  // filter siblings
	  if ( elem.nodeName == 'IMG' ) {
	    this.addImage( elem );
	  }
	  // get background image on element
	  if ( this.options.background === true ) {
	    this.addElementBackgroundImages( elem );
	  }

	  // find children
	  // no non-element nodes, #143
	  var nodeType = elem.nodeType;
	  if ( !nodeType || !elementNodeTypes[ nodeType ] ) {
	    return;
	  }
	  var childImgs = elem.querySelectorAll('img');
	  // concat childElems to filterFound array
	  for ( var i=0; i < childImgs.length; i++ ) {
	    var img = childImgs[i];
	    this.addImage( img );
	  }

	  // get child background images
	  if ( typeof this.options.background == 'string' ) {
	    var children = elem.querySelectorAll( this.options.background );
	    for ( i=0; i < children.length; i++ ) {
	      var child = children[i];
	      this.addElementBackgroundImages( child );
	    }
	  }
	};

	var elementNodeTypes = {
	  1: true,
	  9: true,
	  11: true
	};

	ImagesLoaded.prototype.addElementBackgroundImages = function( elem ) {
	  var style = getComputedStyle( elem );
	  if ( !style ) {
	    // Firefox returns null if in a hidden iframe https://bugzil.la/548397
	    return;
	  }
	  // get url inside url("...")
	  var reURL = /url\((['"])?(.*?)\1\)/gi;
	  var matches = reURL.exec( style.backgroundImage );
	  while ( matches !== null ) {
	    var url = matches && matches[2];
	    if ( url ) {
	      this.addBackground( url, elem );
	    }
	    matches = reURL.exec( style.backgroundImage );
	  }
	};

	/**
	 * @param {Image} img
	 */
	ImagesLoaded.prototype.addImage = function( img ) {
	  var loadingImage = new LoadingImage( img );
	  this.images.push( loadingImage );
	};

	ImagesLoaded.prototype.addBackground = function( url, elem ) {
	  var background = new Background( url, elem );
	  this.images.push( background );
	};

	ImagesLoaded.prototype.check = function() {
	  var _this = this;
	  this.progressedCount = 0;
	  this.hasAnyBroken = false;
	  // complete if no images
	  if ( !this.images.length ) {
	    this.complete();
	    return;
	  }

	  function onProgress( image, elem, message ) {
	    // HACK - Chrome triggers event before object properties have changed. #83
	    setTimeout( function() {
	      _this.progress( image, elem, message );
	    });
	  }

	  this.images.forEach( function( loadingImage ) {
	    loadingImage.once( 'progress', onProgress );
	    loadingImage.check();
	  });
	};

	ImagesLoaded.prototype.progress = function( image, elem, message ) {
	  this.progressedCount++;
	  this.hasAnyBroken = this.hasAnyBroken || !image.isLoaded;
	  // progress event
	  this.emitEvent( 'progress', [ this, image, elem ] );
	  if ( this.jqDeferred && this.jqDeferred.notify ) {
	    this.jqDeferred.notify( this, image );
	  }
	  // check if completed
	  if ( this.progressedCount == this.images.length ) {
	    this.complete();
	  }

	  if ( this.options.debug && console ) {
	    console.log( 'progress: ' + message, image, elem );
	  }
	};

	ImagesLoaded.prototype.complete = function() {
	  var eventName = this.hasAnyBroken ? 'fail' : 'done';
	  this.isComplete = true;
	  this.emitEvent( eventName, [ this ] );
	  this.emitEvent( 'always', [ this ] );
	  if ( this.jqDeferred ) {
	    var jqMethod = this.hasAnyBroken ? 'reject' : 'resolve';
	    this.jqDeferred[ jqMethod ]( this );
	  }
	};

	// --------------------------  -------------------------- //

	function LoadingImage( img ) {
	  this.img = img;
	}

	LoadingImage.prototype = Object.create( EvEmitter.prototype );

	LoadingImage.prototype.check = function() {
	  // If complete is true and browser supports natural sizes,
	  // try to check for image status manually.
	  var isComplete = this.getIsImageComplete();
	  if ( isComplete ) {
	    // report based on naturalWidth
	    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
	    return;
	  }

	  // If none of the checks above matched, simulate loading on detached element.
	  this.proxyImage = new Image();
	  this.proxyImage.addEventListener( 'load', this );
	  this.proxyImage.addEventListener( 'error', this );
	  // bind to image as well for Firefox. #191
	  this.img.addEventListener( 'load', this );
	  this.img.addEventListener( 'error', this );
	  this.proxyImage.src = this.img.src;
	};

	LoadingImage.prototype.getIsImageComplete = function() {
	  return this.img.complete && this.img.naturalWidth !== undefined;
	};

	LoadingImage.prototype.confirm = function( isLoaded, message ) {
	  this.isLoaded = isLoaded;
	  this.emitEvent( 'progress', [ this, this.img, message ] );
	};

	// ----- events ----- //

	// trigger specified handler for event type
	LoadingImage.prototype.handleEvent = function( event ) {
	  var method = 'on' + event.type;
	  if ( this[ method ] ) {
	    this[ method ]( event );
	  }
	};

	LoadingImage.prototype.onload = function() {
	  this.confirm( true, 'onload' );
	  this.unbindEvents();
	};

	LoadingImage.prototype.onerror = function() {
	  this.confirm( false, 'onerror' );
	  this.unbindEvents();
	};

	LoadingImage.prototype.unbindEvents = function() {
	  this.proxyImage.removeEventListener( 'load', this );
	  this.proxyImage.removeEventListener( 'error', this );
	  this.img.removeEventListener( 'load', this );
	  this.img.removeEventListener( 'error', this );
	};

	// -------------------------- Background -------------------------- //

	function Background( url, element ) {
	  this.url = url;
	  this.element = element;
	  this.img = new Image();
	}

	// inherit LoadingImage prototype
	Background.prototype = Object.create( LoadingImage.prototype );

	Background.prototype.check = function() {
	  this.img.addEventListener( 'load', this );
	  this.img.addEventListener( 'error', this );
	  this.img.src = this.url;
	  // check if image is already complete
	  var isComplete = this.getIsImageComplete();
	  if ( isComplete ) {
	    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
	    this.unbindEvents();
	  }
	};

	Background.prototype.unbindEvents = function() {
	  this.img.removeEventListener( 'load', this );
	  this.img.removeEventListener( 'error', this );
	};

	Background.prototype.confirm = function( isLoaded, message ) {
	  this.isLoaded = isLoaded;
	  this.emitEvent( 'progress', [ this, this.element, message ] );
	};

	// -------------------------- jQuery -------------------------- //

	ImagesLoaded.makeJQueryPlugin = function( jQuery ) {
	  jQuery = jQuery || window.jQuery;
	  if ( !jQuery ) {
	    return;
	  }
	  // set local variable
	  $ = jQuery;
	  // $().imagesLoaded()
	  $.fn.imagesLoaded = function( options, callback ) {
	    var instance = new ImagesLoaded( this, options, callback );
	    return instance.jqDeferred.promise( $(this) );
	  };
	};
	// try making plugin
	ImagesLoaded.makeJQueryPlugin();

	// --------------------------  -------------------------- //

	return ImagesLoaded;

	});
	});

	var Lookbook = function () {
	  function Lookbook() {
	    classCallCheck(this, Lookbook);

	    this.$body = $(document.body);

	    this.slideShowSelector = '[data-lookbook-slider]';
	    this.$slideShows = $(this.slideShowSelector);

	    if (this.$slideShows) {
	      this._initSlideShows();
	    }
	  }

	  createClass(Lookbook, [{
	    key: '_initSlideShows',
	    value: function _initSlideShows() {
	      var _this = this;

	      // arrowShape is drawn on a 100x100 ViewBox, only need the left arrow
	      var arrowShape = svgPath('icon-flickity-arrow');

	      var flickityOptions = {
	        autoPlay: 0,
	        cellSelector: '.lookbook-slide',
	        cellAlign: 'center',
	        pageDots: false,
	        prevNextButtons: true,
	        wrapAround: true,
	        arrowShape: arrowShape
	      };

	      var slideShows = document.querySelectorAll(this.slideShowSelector);

	      var _loop = function _loop(index) {
	        /*
	          Initialises the slider after every image in that slideshow has been loaded
	           TODO: Can we initialise the slideshow when one image has loaded, and then add images into the slideshow as they load?
	         */
	        imagesloaded(slideShows[index], function () {
	          var slideShow = new index$2(slideShows[index], flickityOptions);
	          _this._bindSlideShow(slideShow);
	        });
	      };

	      for (var index = 0; index < slideShows.length; index++) {
	        _loop(index);
	      }

	      this._bindSlideShows();
	    }
	  }, {
	    key: '_bindSlideShow',
	    value: function _bindSlideShow(slideShow) {
	      var _this2 = this;

	      var $slideShow = $(slideShow.element);
	      var $viewport = $slideShow.find('.flickity-viewport');

	      /*
	       On slide change, find the new height of the slide and set it as the
	       height of the slider
	        TODO: On upgrade to Flickity 2, change this to `select`
	       */
	      slideShow.on('cellSelect', function () {
	        $slideShow.trigger('heightUpdate');
	      });

	      $slideShow.on('heightUpdate', function () {
	        _this2._setSlideHeight($viewport, $(slideShow.selectedElement));
	      });

	      // Sets the Slider to the height of the first slide
	      $slideShow.trigger('heightUpdate');
	    }
	  }, {
	    key: '_bindSlideShows',
	    value: function _bindSlideShows() {
	      var _this3 = this;

	      $(window).on('resize', index(function () {
	        _this3.$slideShows.trigger('heightUpdate');
	      }, 200));
	    }
	  }, {
	    key: '_setSlideHeight',
	    value: function _setSlideHeight($viewport, $selectedElement) {
	      $viewport.height($selectedElement.height());
	    }
	  }]);
	  return Lookbook;
	}();

	// Account pages js
	var Account = function () {
	  function Account() {
	    classCallCheck(this, Account);


	    // Deal with showing / hiding elements on login page
	    var $formWrapper = $('[data-login-form]');
	    if ($formWrapper) {
	      $formWrapper.on('click tap', '[data-toggle-passwordform]', function () {
	        $formWrapper.toggleClass('show-password-form');
	      });
	    }

	    this.$customerAddresses = $('[data-address-id]');
	    if (this.$customerAddresses) {
	      this.addressPage();
	    }
	  }

	  createClass(Account, [{
	    key: 'addressPage',
	    value: function addressPage() {
	      var $addressEditLinks = $('[data-edit-address]');
	      if ($addressEditLinks) {
	        $addressEditLinks.on('click tap', function (event) {
	          var itemId = $(event.target).attr('data-edit-address');
	          $('[data-address-id="' + itemId + '"]').toggle();
	        });
	      }

	      var $addressDeleteLinks = $('[data-delete-address]');
	      if ($addressDeleteLinks) {
	        $addressDeleteLinks.on('click tap', function (event) {
	          var itemId = $(event.target).attr('data-delete-address');
	          Shopify.CustomerAddress.destroy(itemId);
	        });
	      }

	      this.$customerAddresses.each(function () {
	        var id = $(this).attr('data-address-id');

	        // Initiate provinces for the New Address form
	        new Shopify.CountryProvinceSelector('customer-addr-' + id + '-country', 'customer-addr-' + id + '-province', { hideElement: 'address-province-container-' + id });
	      });
	    }
	  }]);
	  return Account;
	}();

	var __moduleExports$22 = createCommonjsModule(function (module) {
	/*!
	 * getSize v2.0.2
	 * measure size of elements
	 * MIT license
	 */

	/*jshint browser: true, strict: true, undef: true, unused: true */
	/*global define: false, module: false, console: false */

	( function( window, factory ) {
	  'use strict';

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( function() {
	      return factory();
	    });
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS
	    module.exports = factory();
	  } else {
	    // browser global
	    window.getSize = factory();
	  }

	})( window, function factory() {
	'use strict';

	// -------------------------- helpers -------------------------- //

	// get a number from a string, not a percentage
	function getStyleSize( value ) {
	  var num = parseFloat( value );
	  // not a percent like '100%', and a number
	  var isValid = value.indexOf('%') == -1 && !isNaN( num );
	  return isValid && num;
	}

	function noop() {}

	var logError = typeof console == 'undefined' ? noop :
	  function( message ) {
	    console.error( message );
	  };

	// -------------------------- measurements -------------------------- //

	var measurements = [
	  'paddingLeft',
	  'paddingRight',
	  'paddingTop',
	  'paddingBottom',
	  'marginLeft',
	  'marginRight',
	  'marginTop',
	  'marginBottom',
	  'borderLeftWidth',
	  'borderRightWidth',
	  'borderTopWidth',
	  'borderBottomWidth'
	];

	var measurementsLength = measurements.length;

	function getZeroSize() {
	  var size = {
	    width: 0,
	    height: 0,
	    innerWidth: 0,
	    innerHeight: 0,
	    outerWidth: 0,
	    outerHeight: 0
	  };
	  for ( var i=0; i < measurementsLength; i++ ) {
	    var measurement = measurements[i];
	    size[ measurement ] = 0;
	  }
	  return size;
	}

	// -------------------------- getStyle -------------------------- //

	/**
	 * getStyle, get style of element, check for Firefox bug
	 * https://bugzilla.mozilla.org/show_bug.cgi?id=548397
	 */
	function getStyle( elem ) {
	  var style = getComputedStyle( elem );
	  if ( !style ) {
	    logError( 'Style returned ' + style +
	      '. Are you running this code in a hidden iframe on Firefox? ' +
	      'See http://bit.ly/getsizebug1' );
	  }
	  return style;
	}

	// -------------------------- setup -------------------------- //

	var isSetup = false;

	var isBoxSizeOuter;

	/**
	 * setup
	 * check isBoxSizerOuter
	 * do on first getSize() rather than on page load for Firefox bug
	 */
	function setup() {
	  // setup once
	  if ( isSetup ) {
	    return;
	  }
	  isSetup = true;

	  // -------------------------- box sizing -------------------------- //

	  /**
	   * WebKit measures the outer-width on style.width on border-box elems
	   * IE & Firefox<29 measures the inner-width
	   */
	  var div = document.createElement('div');
	  div.style.width = '200px';
	  div.style.padding = '1px 2px 3px 4px';
	  div.style.borderStyle = 'solid';
	  div.style.borderWidth = '1px 2px 3px 4px';
	  div.style.boxSizing = 'border-box';

	  var body = document.body || document.documentElement;
	  body.appendChild( div );
	  var style = getStyle( div );

	  getSize.isBoxSizeOuter = isBoxSizeOuter = getStyleSize( style.width ) == 200;
	  body.removeChild( div );

	}

	// -------------------------- getSize -------------------------- //

	function getSize( elem ) {
	  setup();

	  // use querySeletor if elem is string
	  if ( typeof elem == 'string' ) {
	    elem = document.querySelector( elem );
	  }

	  // do not proceed on non-objects
	  if ( !elem || typeof elem != 'object' || !elem.nodeType ) {
	    return;
	  }

	  var style = getStyle( elem );

	  // if hidden, everything is 0
	  if ( style.display == 'none' ) {
	    return getZeroSize();
	  }

	  var size = {};
	  size.width = elem.offsetWidth;
	  size.height = elem.offsetHeight;

	  var isBorderBox = size.isBorderBox = style.boxSizing == 'border-box';

	  // get all measurements
	  for ( var i=0; i < measurementsLength; i++ ) {
	    var measurement = measurements[i];
	    var value = style[ measurement ];
	    var num = parseFloat( value );
	    // any 'auto', 'medium' value will be 0
	    size[ measurement ] = !isNaN( num ) ? num : 0;
	  }

	  var paddingWidth = size.paddingLeft + size.paddingRight;
	  var paddingHeight = size.paddingTop + size.paddingBottom;
	  var marginWidth = size.marginLeft + size.marginRight;
	  var marginHeight = size.marginTop + size.marginBottom;
	  var borderWidth = size.borderLeftWidth + size.borderRightWidth;
	  var borderHeight = size.borderTopWidth + size.borderBottomWidth;

	  var isBorderBoxSizeOuter = isBorderBox && isBoxSizeOuter;

	  // overwrite width and height if we can get it from style
	  var styleWidth = getStyleSize( style.width );
	  if ( styleWidth !== false ) {
	    size.width = styleWidth +
	      // add padding and border unless it's already including it
	      ( isBorderBoxSizeOuter ? 0 : paddingWidth + borderWidth );
	  }

	  var styleHeight = getStyleSize( style.height );
	  if ( styleHeight !== false ) {
	    size.height = styleHeight +
	      // add padding and border unless it's already including it
	      ( isBorderBoxSizeOuter ? 0 : paddingHeight + borderHeight );
	  }

	  size.innerWidth = size.width - ( paddingWidth + borderWidth );
	  size.innerHeight = size.height - ( paddingHeight + borderHeight );

	  size.outerWidth = size.width + marginWidth;
	  size.outerHeight = size.height + marginHeight;

	  return size;
	}

	return getSize;

	});
	});

	var __moduleExports$24 = createCommonjsModule(function (module) {
	/**
	 * matchesSelector v2.0.1
	 * matchesSelector( element, '.selector' )
	 * MIT license
	 */

	/*jshint browser: true, strict: true, undef: true, unused: true */

	( function( window, factory ) {
	  /*global define: false, module: false */
	  'use strict';
	  // universal module definition
	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( factory );
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS
	    module.exports = factory();
	  } else {
	    // browser global
	    window.matchesSelector = factory();
	  }

	}( window, function factory() {
	  'use strict';

	  var matchesMethod = ( function() {
	    var ElemProto = Element.prototype;
	    // check for the standard method name first
	    if ( ElemProto.matches ) {
	      return 'matches';
	    }
	    // check un-prefixed
	    if ( ElemProto.matchesSelector ) {
	      return 'matchesSelector';
	    }
	    // check vendor prefixes
	    var prefixes = [ 'webkit', 'moz', 'ms', 'o' ];

	    for ( var i=0; i < prefixes.length; i++ ) {
	      var prefix = prefixes[i];
	      var method = prefix + 'MatchesSelector';
	      if ( ElemProto[ method ] ) {
	        return method;
	      }
	    }
	  })();

	  return function matchesSelector( elem, selector ) {
	    return elem[ matchesMethod ]( selector );
	  };

	}));
	});

	var __moduleExports$23 = createCommonjsModule(function (module) {
	/**
	 * Fizzy UI utils v2.0.3
	 * MIT license
	 */

	/*jshint browser: true, undef: true, unused: true, strict: true */

	( function( window, factory ) {
	  // universal module definition
	  /*jshint strict: false */ /*globals define, module, require */

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	      'desandro-matches-selector/matches-selector'
	    ], function( matchesSelector ) {
	      return factory( window, matchesSelector );
	    });
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      __moduleExports$24
	    );
	  } else {
	    // browser global
	    window.fizzyUIUtils = factory(
	      window,
	      window.matchesSelector
	    );
	  }

	}( window, function factory( window, matchesSelector ) {

	'use strict';

	var utils = {};

	// ----- extend ----- //

	// extends objects
	utils.extend = function( a, b ) {
	  for ( var prop in b ) {
	    a[ prop ] = b[ prop ];
	  }
	  return a;
	};

	// ----- modulo ----- //

	utils.modulo = function( num, div ) {
	  return ( ( num % div ) + div ) % div;
	};

	// ----- makeArray ----- //

	// turn element or nodeList into an array
	utils.makeArray = function( obj ) {
	  var ary = [];
	  if ( Array.isArray( obj ) ) {
	    // use object if already an array
	    ary = obj;
	  } else if ( obj && typeof obj.length == 'number' ) {
	    // convert nodeList to array
	    for ( var i=0; i < obj.length; i++ ) {
	      ary.push( obj[i] );
	    }
	  } else {
	    // array of single index
	    ary.push( obj );
	  }
	  return ary;
	};

	// ----- removeFrom ----- //

	utils.removeFrom = function( ary, obj ) {
	  var index = ary.indexOf( obj );
	  if ( index != -1 ) {
	    ary.splice( index, 1 );
	  }
	};

	// ----- getParent ----- //

	utils.getParent = function( elem, selector ) {
	  while ( elem != document.body ) {
	    elem = elem.parentNode;
	    if ( matchesSelector( elem, selector ) ) {
	      return elem;
	    }
	  }
	};

	// ----- getQueryElement ----- //

	// use element as selector string
	utils.getQueryElement = function( elem ) {
	  if ( typeof elem == 'string' ) {
	    return document.querySelector( elem );
	  }
	  return elem;
	};

	// ----- handleEvent ----- //

	// enable .ontype to trigger from .addEventListener( elem, 'type' )
	utils.handleEvent = function( event ) {
	  var method = 'on' + event.type;
	  if ( this[ method ] ) {
	    this[ method ]( event );
	  }
	};

	// ----- filterFindElements ----- //

	utils.filterFindElements = function( elems, selector ) {
	  // make array of elems
	  elems = utils.makeArray( elems );
	  var ffElems = [];

	  elems.forEach( function( elem ) {
	    // check that elem is an actual element
	    if ( !( elem instanceof HTMLElement ) ) {
	      return;
	    }
	    // add elem if no selector
	    if ( !selector ) {
	      ffElems.push( elem );
	      return;
	    }
	    // filter & find items if we have a selector
	    // filter
	    if ( matchesSelector( elem, selector ) ) {
	      ffElems.push( elem );
	    }
	    // find children
	    var childElems = elem.querySelectorAll( selector );
	    // concat childElems to filterFound array
	    for ( var i=0; i < childElems.length; i++ ) {
	      ffElems.push( childElems[i] );
	    }
	  });

	  return ffElems;
	};

	// ----- debounceMethod ----- //

	utils.debounceMethod = function( _class, methodName, threshold ) {
	  // original method
	  var method = _class.prototype[ methodName ];
	  var timeoutName = methodName + 'Timeout';

	  _class.prototype[ methodName ] = function() {
	    var timeout = this[ timeoutName ];
	    if ( timeout ) {
	      clearTimeout( timeout );
	    }
	    var args = arguments;

	    var _this = this;
	    this[ timeoutName ] = setTimeout( function() {
	      method.apply( _this, args );
	      delete _this[ timeoutName ];
	    }, threshold || 100 );
	  };
	};

	// ----- docReady ----- //

	utils.docReady = function( callback ) {
	  var readyState = document.readyState;
	  if ( readyState == 'complete' || readyState == 'interactive' ) {
	    // do async to allow for other scripts to run. metafizzy/flickity#441
	    setTimeout( callback );
	  } else {
	    document.addEventListener( 'DOMContentLoaded', callback );
	  }
	};

	// ----- htmlInit ----- //

	// http://jamesroberts.name/blog/2010/02/22/string-functions-for-javascript-trim-to-camel-case-to-dashed-and-to-underscore/
	utils.toDashed = function( str ) {
	  return str.replace( /(.)([A-Z])/g, function( match, $1, $2 ) {
	    return $1 + '-' + $2;
	  }).toLowerCase();
	};

	var console = window.console;
	/**
	 * allow user to initialize classes via [data-namespace] or .js-namespace class
	 * htmlInit( Widget, 'widgetName' )
	 * options are parsed from data-namespace-options
	 */
	utils.htmlInit = function( WidgetClass, namespace ) {
	  utils.docReady( function() {
	    var dashedNamespace = utils.toDashed( namespace );
	    var dataAttr = 'data-' + dashedNamespace;
	    var dataAttrElems = document.querySelectorAll( '[' + dataAttr + ']' );
	    var jsDashElems = document.querySelectorAll( '.js-' + dashedNamespace );
	    var elems = utils.makeArray( dataAttrElems )
	      .concat( utils.makeArray( jsDashElems ) );
	    var dataOptionsAttr = dataAttr + '-options';
	    var jQuery = window.jQuery;

	    elems.forEach( function( elem ) {
	      var attr = elem.getAttribute( dataAttr ) ||
	        elem.getAttribute( dataOptionsAttr );
	      var options;
	      try {
	        options = attr && JSON.parse( attr );
	      } catch ( error ) {
	        // log error, do not initialize
	        if ( console ) {
	          console.error( 'Error parsing ' + dataAttr + ' on ' + elem.className +
	          ': ' + error );
	        }
	        return;
	      }
	      // initialize
	      var instance = new WidgetClass( elem, options );
	      // make available via $().data('namespace')
	      if ( jQuery ) {
	        jQuery.data( elem, namespace, instance );
	      }
	    });

	  });
	};

	// -----  ----- //

	return utils;

	}));
	});

	var __moduleExports$25 = createCommonjsModule(function (module) {
	/**
	 * Outlayer Item
	 */

	( function( window, factory ) {
	  // universal module definition
	  /* jshint strict: false */ /* globals define, module, require */
	  if ( typeof define == 'function' && define.amd ) {
	    // AMD - RequireJS
	    define( [
	        'ev-emitter/ev-emitter',
	        'get-size/get-size'
	      ],
	      factory
	    );
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS - Browserify, Webpack
	    module.exports = factory(
	      __moduleExports$20,
	      __moduleExports$22
	    );
	  } else {
	    // browser global
	    window.Outlayer = {};
	    window.Outlayer.Item = factory(
	      window.EvEmitter,
	      window.getSize
	    );
	  }

	}( window, function factory( EvEmitter, getSize ) {
	'use strict';

	// ----- helpers ----- //

	function isEmptyObj( obj ) {
	  for ( var prop in obj ) {
	    return false;
	  }
	  prop = null;
	  return true;
	}

	// -------------------------- CSS3 support -------------------------- //


	var docElemStyle = document.documentElement.style;

	var transitionProperty = typeof docElemStyle.transition == 'string' ?
	  'transition' : 'WebkitTransition';
	var transformProperty = typeof docElemStyle.transform == 'string' ?
	  'transform' : 'WebkitTransform';

	var transitionEndEvent = {
	  WebkitTransition: 'webkitTransitionEnd',
	  transition: 'transitionend'
	}[ transitionProperty ];

	// cache all vendor properties that could have vendor prefix
	var vendorProperties = {
	  transform: transformProperty,
	  transition: transitionProperty,
	  transitionDuration: transitionProperty + 'Duration',
	  transitionProperty: transitionProperty + 'Property',
	  transitionDelay: transitionProperty + 'Delay'
	};

	// -------------------------- Item -------------------------- //

	function Item( element, layout ) {
	  if ( !element ) {
	    return;
	  }

	  this.element = element;
	  // parent layout class, i.e. Masonry, Isotope, or Packery
	  this.layout = layout;
	  this.position = {
	    x: 0,
	    y: 0
	  };

	  this._create();
	}

	// inherit EvEmitter
	var proto = Item.prototype = Object.create( EvEmitter.prototype );
	proto.constructor = Item;

	proto._create = function() {
	  // transition objects
	  this._transn = {
	    ingProperties: {},
	    clean: {},
	    onEnd: {}
	  };

	  this.css({
	    position: 'absolute'
	  });
	};

	// trigger specified handler for event type
	proto.handleEvent = function( event ) {
	  var method = 'on' + event.type;
	  if ( this[ method ] ) {
	    this[ method ]( event );
	  }
	};

	proto.getSize = function() {
	  this.size = getSize( this.element );
	};

	/**
	 * apply CSS styles to element
	 * @param {Object} style
	 */
	proto.css = function( style ) {
	  var elemStyle = this.element.style;

	  for ( var prop in style ) {
	    // use vendor property if available
	    var supportedProp = vendorProperties[ prop ] || prop;
	    elemStyle[ supportedProp ] = style[ prop ];
	  }
	};

	 // measure position, and sets it
	proto.getPosition = function() {
	  var style = getComputedStyle( this.element );
	  var isOriginLeft = this.layout._getOption('originLeft');
	  var isOriginTop = this.layout._getOption('originTop');
	  var xValue = style[ isOriginLeft ? 'left' : 'right' ];
	  var yValue = style[ isOriginTop ? 'top' : 'bottom' ];
	  // convert percent to pixels
	  var layoutSize = this.layout.size;
	  var x = xValue.indexOf('%') != -1 ?
	    ( parseFloat( xValue ) / 100 ) * layoutSize.width : parseInt( xValue, 10 );
	  var y = yValue.indexOf('%') != -1 ?
	    ( parseFloat( yValue ) / 100 ) * layoutSize.height : parseInt( yValue, 10 );

	  // clean up 'auto' or other non-integer values
	  x = isNaN( x ) ? 0 : x;
	  y = isNaN( y ) ? 0 : y;
	  // remove padding from measurement
	  x -= isOriginLeft ? layoutSize.paddingLeft : layoutSize.paddingRight;
	  y -= isOriginTop ? layoutSize.paddingTop : layoutSize.paddingBottom;

	  this.position.x = x;
	  this.position.y = y;
	};

	// set settled position, apply padding
	proto.layoutPosition = function() {
	  var layoutSize = this.layout.size;
	  var style = {};
	  var isOriginLeft = this.layout._getOption('originLeft');
	  var isOriginTop = this.layout._getOption('originTop');

	  // x
	  var xPadding = isOriginLeft ? 'paddingLeft' : 'paddingRight';
	  var xProperty = isOriginLeft ? 'left' : 'right';
	  var xResetProperty = isOriginLeft ? 'right' : 'left';

	  var x = this.position.x + layoutSize[ xPadding ];
	  // set in percentage or pixels
	  style[ xProperty ] = this.getXValue( x );
	  // reset other property
	  style[ xResetProperty ] = '';

	  // y
	  var yPadding = isOriginTop ? 'paddingTop' : 'paddingBottom';
	  var yProperty = isOriginTop ? 'top' : 'bottom';
	  var yResetProperty = isOriginTop ? 'bottom' : 'top';

	  var y = this.position.y + layoutSize[ yPadding ];
	  // set in percentage or pixels
	  style[ yProperty ] = this.getYValue( y );
	  // reset other property
	  style[ yResetProperty ] = '';

	  this.css( style );
	  this.emitEvent( 'layout', [ this ] );
	};

	proto.getXValue = function( x ) {
	  var isHorizontal = this.layout._getOption('horizontal');
	  return this.layout.options.percentPosition && !isHorizontal ?
	    ( ( x / this.layout.size.width ) * 100 ) + '%' : x + 'px';
	};

	proto.getYValue = function( y ) {
	  var isHorizontal = this.layout._getOption('horizontal');
	  return this.layout.options.percentPosition && isHorizontal ?
	    ( ( y / this.layout.size.height ) * 100 ) + '%' : y + 'px';
	};

	proto._transitionTo = function( x, y ) {
	  this.getPosition();
	  // get current x & y from top/left
	  var curX = this.position.x;
	  var curY = this.position.y;

	  var compareX = parseInt( x, 10 );
	  var compareY = parseInt( y, 10 );
	  var didNotMove = compareX === this.position.x && compareY === this.position.y;

	  // save end position
	  this.setPosition( x, y );

	  // if did not move and not transitioning, just go to layout
	  if ( didNotMove && !this.isTransitioning ) {
	    this.layoutPosition();
	    return;
	  }

	  var transX = x - curX;
	  var transY = y - curY;
	  var transitionStyle = {};
	  transitionStyle.transform = this.getTranslate( transX, transY );

	  this.transition({
	    to: transitionStyle,
	    onTransitionEnd: {
	      transform: this.layoutPosition
	    },
	    isCleaning: true
	  });
	};

	proto.getTranslate = function( x, y ) {
	  // flip cooridinates if origin on right or bottom
	  var isOriginLeft = this.layout._getOption('originLeft');
	  var isOriginTop = this.layout._getOption('originTop');
	  x = isOriginLeft ? x : -x;
	  y = isOriginTop ? y : -y;
	  return 'translate3d(' + x + 'px, ' + y + 'px, 0)';
	};

	// non transition + transform support
	proto.goTo = function( x, y ) {
	  this.setPosition( x, y );
	  this.layoutPosition();
	};

	proto.moveTo = proto._transitionTo;

	proto.setPosition = function( x, y ) {
	  this.position.x = parseInt( x, 10 );
	  this.position.y = parseInt( y, 10 );
	};

	// ----- transition ----- //

	/**
	 * @param {Object} style - CSS
	 * @param {Function} onTransitionEnd
	 */

	// non transition, just trigger callback
	proto._nonTransition = function( args ) {
	  this.css( args.to );
	  if ( args.isCleaning ) {
	    this._removeStyles( args.to );
	  }
	  for ( var prop in args.onTransitionEnd ) {
	    args.onTransitionEnd[ prop ].call( this );
	  }
	};

	/**
	 * proper transition
	 * @param {Object} args - arguments
	 *   @param {Object} to - style to transition to
	 *   @param {Object} from - style to start transition from
	 *   @param {Boolean} isCleaning - removes transition styles after transition
	 *   @param {Function} onTransitionEnd - callback
	 */
	proto.transition = function( args ) {
	  // redirect to nonTransition if no transition duration
	  if ( !parseFloat( this.layout.options.transitionDuration ) ) {
	    this._nonTransition( args );
	    return;
	  }

	  var _transition = this._transn;
	  // keep track of onTransitionEnd callback by css property
	  for ( var prop in args.onTransitionEnd ) {
	    _transition.onEnd[ prop ] = args.onTransitionEnd[ prop ];
	  }
	  // keep track of properties that are transitioning
	  for ( prop in args.to ) {
	    _transition.ingProperties[ prop ] = true;
	    // keep track of properties to clean up when transition is done
	    if ( args.isCleaning ) {
	      _transition.clean[ prop ] = true;
	    }
	  }

	  // set from styles
	  if ( args.from ) {
	    this.css( args.from );
	    // force redraw. http://blog.alexmaccaw.com/css-transitions
	    var h = this.element.offsetHeight;
	    // hack for JSHint to hush about unused var
	    h = null;
	  }
	  // enable transition
	  this.enableTransition( args.to );
	  // set styles that are transitioning
	  this.css( args.to );

	  this.isTransitioning = true;

	};

	// dash before all cap letters, including first for
	// WebkitTransform => -webkit-transform
	function toDashedAll( str ) {
	  return str.replace( /([A-Z])/g, function( $1 ) {
	    return '-' + $1.toLowerCase();
	  });
	}

	var transitionProps = 'opacity,' + toDashedAll( transformProperty );

	proto.enableTransition = function(/* style */) {
	  // HACK changing transitionProperty during a transition
	  // will cause transition to jump
	  if ( this.isTransitioning ) {
	    return;
	  }

	  // make `transition: foo, bar, baz` from style object
	  // HACK un-comment this when enableTransition can work
	  // while a transition is happening
	  // var transitionValues = [];
	  // for ( var prop in style ) {
	  //   // dash-ify camelCased properties like WebkitTransition
	  //   prop = vendorProperties[ prop ] || prop;
	  //   transitionValues.push( toDashedAll( prop ) );
	  // }
	  // munge number to millisecond, to match stagger
	  var duration = this.layout.options.transitionDuration;
	  duration = typeof duration == 'number' ? duration + 'ms' : duration;
	  // enable transition styles
	  this.css({
	    transitionProperty: transitionProps,
	    transitionDuration: duration,
	    transitionDelay: this.staggerDelay || 0
	  });
	  // listen for transition end event
	  this.element.addEventListener( transitionEndEvent, this, false );
	};

	// ----- events ----- //

	proto.onwebkitTransitionEnd = function( event ) {
	  this.ontransitionend( event );
	};

	proto.onotransitionend = function( event ) {
	  this.ontransitionend( event );
	};

	// properties that I munge to make my life easier
	var dashedVendorProperties = {
	  '-webkit-transform': 'transform'
	};

	proto.ontransitionend = function( event ) {
	  // disregard bubbled events from children
	  if ( event.target !== this.element ) {
	    return;
	  }
	  var _transition = this._transn;
	  // get property name of transitioned property, convert to prefix-free
	  var propertyName = dashedVendorProperties[ event.propertyName ] || event.propertyName;

	  // remove property that has completed transitioning
	  delete _transition.ingProperties[ propertyName ];
	  // check if any properties are still transitioning
	  if ( isEmptyObj( _transition.ingProperties ) ) {
	    // all properties have completed transitioning
	    this.disableTransition();
	  }
	  // clean style
	  if ( propertyName in _transition.clean ) {
	    // clean up style
	    this.element.style[ event.propertyName ] = '';
	    delete _transition.clean[ propertyName ];
	  }
	  // trigger onTransitionEnd callback
	  if ( propertyName in _transition.onEnd ) {
	    var onTransitionEnd = _transition.onEnd[ propertyName ];
	    onTransitionEnd.call( this );
	    delete _transition.onEnd[ propertyName ];
	  }

	  this.emitEvent( 'transitionEnd', [ this ] );
	};

	proto.disableTransition = function() {
	  this.removeTransitionStyles();
	  this.element.removeEventListener( transitionEndEvent, this, false );
	  this.isTransitioning = false;
	};

	/**
	 * removes style property from element
	 * @param {Object} style
	**/
	proto._removeStyles = function( style ) {
	  // clean up transition styles
	  var cleanStyle = {};
	  for ( var prop in style ) {
	    cleanStyle[ prop ] = '';
	  }
	  this.css( cleanStyle );
	};

	var cleanTransitionStyle = {
	  transitionProperty: '',
	  transitionDuration: '',
	  transitionDelay: ''
	};

	proto.removeTransitionStyles = function() {
	  // remove transition
	  this.css( cleanTransitionStyle );
	};

	// ----- stagger ----- //

	proto.stagger = function( delay ) {
	  delay = isNaN( delay ) ? 0 : delay;
	  this.staggerDelay = delay + 'ms';
	};

	// ----- show/hide/remove ----- //

	// remove element from DOM
	proto.removeElem = function() {
	  this.element.parentNode.removeChild( this.element );
	  // remove display: none
	  this.css({ display: '' });
	  this.emitEvent( 'remove', [ this ] );
	};

	proto.remove = function() {
	  // just remove element if no transition support or no transition
	  if ( !transitionProperty || !parseFloat( this.layout.options.transitionDuration ) ) {
	    this.removeElem();
	    return;
	  }

	  // start transition
	  this.once( 'transitionEnd', function() {
	    this.removeElem();
	  });
	  this.hide();
	};

	proto.reveal = function() {
	  delete this.isHidden;
	  // remove display: none
	  this.css({ display: '' });

	  var options = this.layout.options;

	  var onTransitionEnd = {};
	  var transitionEndProperty = this.getHideRevealTransitionEndProperty('visibleStyle');
	  onTransitionEnd[ transitionEndProperty ] = this.onRevealTransitionEnd;

	  this.transition({
	    from: options.hiddenStyle,
	    to: options.visibleStyle,
	    isCleaning: true,
	    onTransitionEnd: onTransitionEnd
	  });
	};

	proto.onRevealTransitionEnd = function() {
	  // check if still visible
	  // during transition, item may have been hidden
	  if ( !this.isHidden ) {
	    this.emitEvent('reveal');
	  }
	};

	/**
	 * get style property use for hide/reveal transition end
	 * @param {String} styleProperty - hiddenStyle/visibleStyle
	 * @returns {String}
	 */
	proto.getHideRevealTransitionEndProperty = function( styleProperty ) {
	  var optionStyle = this.layout.options[ styleProperty ];
	  // use opacity
	  if ( optionStyle.opacity ) {
	    return 'opacity';
	  }
	  // get first property
	  for ( var prop in optionStyle ) {
	    return prop;
	  }
	};

	proto.hide = function() {
	  // set flag
	  this.isHidden = true;
	  // remove display: none
	  this.css({ display: '' });

	  var options = this.layout.options;

	  var onTransitionEnd = {};
	  var transitionEndProperty = this.getHideRevealTransitionEndProperty('hiddenStyle');
	  onTransitionEnd[ transitionEndProperty ] = this.onHideTransitionEnd;

	  this.transition({
	    from: options.visibleStyle,
	    to: options.hiddenStyle,
	    // keep hidden stuff hidden
	    isCleaning: true,
	    onTransitionEnd: onTransitionEnd
	  });
	};

	proto.onHideTransitionEnd = function() {
	  // check if still hidden
	  // during transition, item may have been un-hidden
	  if ( this.isHidden ) {
	    this.css({ display: 'none' });
	    this.emitEvent('hide');
	  }
	};

	proto.destroy = function() {
	  this.css({
	    position: '',
	    left: '',
	    right: '',
	    top: '',
	    bottom: '',
	    transition: '',
	    transform: ''
	  });
	};

	return Item;

	}));
	});

	var __moduleExports$21 = createCommonjsModule(function (module) {
	/*!
	 * Outlayer v2.1.0
	 * the brains and guts of a layout library
	 * MIT license
	 */

	( function( window, factory ) {
	  'use strict';
	  // universal module definition
	  /* jshint strict: false */ /* globals define, module, require */
	  if ( typeof define == 'function' && define.amd ) {
	    // AMD - RequireJS
	    define( [
	        'ev-emitter/ev-emitter',
	        'get-size/get-size',
	        'fizzy-ui-utils/utils',
	        './item'
	      ],
	      function( EvEmitter, getSize, utils, Item ) {
	        return factory( window, EvEmitter, getSize, utils, Item);
	      }
	    );
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS - Browserify, Webpack
	    module.exports = factory(
	      window,
	      __moduleExports$20,
	      __moduleExports$22,
	      __moduleExports$23,
	      __moduleExports$25
	    );
	  } else {
	    // browser global
	    window.Outlayer = factory(
	      window,
	      window.EvEmitter,
	      window.getSize,
	      window.fizzyUIUtils,
	      window.Outlayer.Item
	    );
	  }

	}( window, function factory( window, EvEmitter, getSize, utils, Item ) {
	'use strict';

	// ----- vars ----- //

	var console = window.console;
	var jQuery = window.jQuery;
	var noop = function() {};

	// -------------------------- Outlayer -------------------------- //

	// globally unique identifiers
	var GUID = 0;
	// internal store of all Outlayer intances
	var instances = {};


	/**
	 * @param {Element, String} element
	 * @param {Object} options
	 * @constructor
	 */
	function Outlayer( element, options ) {
	  var queryElement = utils.getQueryElement( element );
	  if ( !queryElement ) {
	    if ( console ) {
	      console.error( 'Bad element for ' + this.constructor.namespace +
	        ': ' + ( queryElement || element ) );
	    }
	    return;
	  }
	  this.element = queryElement;
	  // add jQuery
	  if ( jQuery ) {
	    this.$element = jQuery( this.element );
	  }

	  // options
	  this.options = utils.extend( {}, this.constructor.defaults );
	  this.option( options );

	  // add id for Outlayer.getFromElement
	  var id = ++GUID;
	  this.element.outlayerGUID = id; // expando
	  instances[ id ] = this; // associate via id

	  // kick it off
	  this._create();

	  var isInitLayout = this._getOption('initLayout');
	  if ( isInitLayout ) {
	    this.layout();
	  }
	}

	// settings are for internal use only
	Outlayer.namespace = 'outlayer';
	Outlayer.Item = Item;

	// default options
	Outlayer.defaults = {
	  containerStyle: {
	    position: 'relative'
	  },
	  initLayout: true,
	  originLeft: true,
	  originTop: true,
	  resize: true,
	  resizeContainer: true,
	  // item options
	  transitionDuration: '0.4s',
	  hiddenStyle: {
	    opacity: 0,
	    transform: 'scale(0.001)'
	  },
	  visibleStyle: {
	    opacity: 1,
	    transform: 'scale(1)'
	  }
	};

	var proto = Outlayer.prototype;
	// inherit EvEmitter
	utils.extend( proto, EvEmitter.prototype );

	/**
	 * set options
	 * @param {Object} opts
	 */
	proto.option = function( opts ) {
	  utils.extend( this.options, opts );
	};

	/**
	 * get backwards compatible option value, check old name
	 */
	proto._getOption = function( option ) {
	  var oldOption = this.constructor.compatOptions[ option ];
	  return oldOption && this.options[ oldOption ] !== undefined ?
	    this.options[ oldOption ] : this.options[ option ];
	};

	Outlayer.compatOptions = {
	  // currentName: oldName
	  initLayout: 'isInitLayout',
	  horizontal: 'isHorizontal',
	  layoutInstant: 'isLayoutInstant',
	  originLeft: 'isOriginLeft',
	  originTop: 'isOriginTop',
	  resize: 'isResizeBound',
	  resizeContainer: 'isResizingContainer'
	};

	proto._create = function() {
	  // get items from children
	  this.reloadItems();
	  // elements that affect layout, but are not laid out
	  this.stamps = [];
	  this.stamp( this.options.stamp );
	  // set container style
	  utils.extend( this.element.style, this.options.containerStyle );

	  // bind resize method
	  var canBindResize = this._getOption('resize');
	  if ( canBindResize ) {
	    this.bindResize();
	  }
	};

	// goes through all children again and gets bricks in proper order
	proto.reloadItems = function() {
	  // collection of item elements
	  this.items = this._itemize( this.element.children );
	};


	/**
	 * turn elements into Outlayer.Items to be used in layout
	 * @param {Array or NodeList or HTMLElement} elems
	 * @returns {Array} items - collection of new Outlayer Items
	 */
	proto._itemize = function( elems ) {

	  var itemElems = this._filterFindItemElements( elems );
	  var Item = this.constructor.Item;

	  // create new Outlayer Items for collection
	  var items = [];
	  for ( var i=0; i < itemElems.length; i++ ) {
	    var elem = itemElems[i];
	    var item = new Item( elem, this );
	    items.push( item );
	  }

	  return items;
	};

	/**
	 * get item elements to be used in layout
	 * @param {Array or NodeList or HTMLElement} elems
	 * @returns {Array} items - item elements
	 */
	proto._filterFindItemElements = function( elems ) {
	  return utils.filterFindElements( elems, this.options.itemSelector );
	};

	/**
	 * getter method for getting item elements
	 * @returns {Array} elems - collection of item elements
	 */
	proto.getItemElements = function() {
	  return this.items.map( function( item ) {
	    return item.element;
	  });
	};

	// ----- init & layout ----- //

	/**
	 * lays out all items
	 */
	proto.layout = function() {
	  this._resetLayout();
	  this._manageStamps();

	  // don't animate first layout
	  var layoutInstant = this._getOption('layoutInstant');
	  var isInstant = layoutInstant !== undefined ?
	    layoutInstant : !this._isLayoutInited;
	  this.layoutItems( this.items, isInstant );

	  // flag for initalized
	  this._isLayoutInited = true;
	};

	// _init is alias for layout
	proto._init = proto.layout;

	/**
	 * logic before any new layout
	 */
	proto._resetLayout = function() {
	  this.getSize();
	};


	proto.getSize = function() {
	  this.size = getSize( this.element );
	};

	/**
	 * get measurement from option, for columnWidth, rowHeight, gutter
	 * if option is String -> get element from selector string, & get size of element
	 * if option is Element -> get size of element
	 * else use option as a number
	 *
	 * @param {String} measurement
	 * @param {String} size - width or height
	 * @private
	 */
	proto._getMeasurement = function( measurement, size ) {
	  var option = this.options[ measurement ];
	  var elem;
	  if ( !option ) {
	    // default to 0
	    this[ measurement ] = 0;
	  } else {
	    // use option as an element
	    if ( typeof option == 'string' ) {
	      elem = this.element.querySelector( option );
	    } else if ( option instanceof HTMLElement ) {
	      elem = option;
	    }
	    // use size of element, if element
	    this[ measurement ] = elem ? getSize( elem )[ size ] : option;
	  }
	};

	/**
	 * layout a collection of item elements
	 * @api public
	 */
	proto.layoutItems = function( items, isInstant ) {
	  items = this._getItemsForLayout( items );

	  this._layoutItems( items, isInstant );

	  this._postLayout();
	};

	/**
	 * get the items to be laid out
	 * you may want to skip over some items
	 * @param {Array} items
	 * @returns {Array} items
	 */
	proto._getItemsForLayout = function( items ) {
	  return items.filter( function( item ) {
	    return !item.isIgnored;
	  });
	};

	/**
	 * layout items
	 * @param {Array} items
	 * @param {Boolean} isInstant
	 */
	proto._layoutItems = function( items, isInstant ) {
	  this._emitCompleteOnItems( 'layout', items );

	  if ( !items || !items.length ) {
	    // no items, emit event with empty array
	    return;
	  }

	  var queue = [];

	  items.forEach( function( item ) {
	    // get x/y object from method
	    var position = this._getItemLayoutPosition( item );
	    // enqueue
	    position.item = item;
	    position.isInstant = isInstant || item.isLayoutInstant;
	    queue.push( position );
	  }, this );

	  this._processLayoutQueue( queue );
	};

	/**
	 * get item layout position
	 * @param {Outlayer.Item} item
	 * @returns {Object} x and y position
	 */
	proto._getItemLayoutPosition = function( /* item */ ) {
	  return {
	    x: 0,
	    y: 0
	  };
	};

	/**
	 * iterate over array and position each item
	 * Reason being - separating this logic prevents 'layout invalidation'
	 * thx @paul_irish
	 * @param {Array} queue
	 */
	proto._processLayoutQueue = function( queue ) {
	  this.updateStagger();
	  queue.forEach( function( obj, i ) {
	    this._positionItem( obj.item, obj.x, obj.y, obj.isInstant, i );
	  }, this );
	};

	// set stagger from option in milliseconds number
	proto.updateStagger = function() {
	  var stagger = this.options.stagger;
	  if ( stagger === null || stagger === undefined ) {
	    this.stagger = 0;
	    return;
	  }
	  this.stagger = getMilliseconds( stagger );
	  return this.stagger;
	};

	/**
	 * Sets position of item in DOM
	 * @param {Outlayer.Item} item
	 * @param {Number} x - horizontal position
	 * @param {Number} y - vertical position
	 * @param {Boolean} isInstant - disables transitions
	 */
	proto._positionItem = function( item, x, y, isInstant, i ) {
	  if ( isInstant ) {
	    // if not transition, just set CSS
	    item.goTo( x, y );
	  } else {
	    item.stagger( i * this.stagger );
	    item.moveTo( x, y );
	  }
	};

	/**
	 * Any logic you want to do after each layout,
	 * i.e. size the container
	 */
	proto._postLayout = function() {
	  this.resizeContainer();
	};

	proto.resizeContainer = function() {
	  var isResizingContainer = this._getOption('resizeContainer');
	  if ( !isResizingContainer ) {
	    return;
	  }
	  var size = this._getContainerSize();
	  if ( size ) {
	    this._setContainerMeasure( size.width, true );
	    this._setContainerMeasure( size.height, false );
	  }
	};

	/**
	 * Sets width or height of container if returned
	 * @returns {Object} size
	 *   @param {Number} width
	 *   @param {Number} height
	 */
	proto._getContainerSize = noop;

	/**
	 * @param {Number} measure - size of width or height
	 * @param {Boolean} isWidth
	 */
	proto._setContainerMeasure = function( measure, isWidth ) {
	  if ( measure === undefined ) {
	    return;
	  }

	  var elemSize = this.size;
	  // add padding and border width if border box
	  if ( elemSize.isBorderBox ) {
	    measure += isWidth ? elemSize.paddingLeft + elemSize.paddingRight +
	      elemSize.borderLeftWidth + elemSize.borderRightWidth :
	      elemSize.paddingBottom + elemSize.paddingTop +
	      elemSize.borderTopWidth + elemSize.borderBottomWidth;
	  }

	  measure = Math.max( measure, 0 );
	  this.element.style[ isWidth ? 'width' : 'height' ] = measure + 'px';
	};

	/**
	 * emit eventComplete on a collection of items events
	 * @param {String} eventName
	 * @param {Array} items - Outlayer.Items
	 */
	proto._emitCompleteOnItems = function( eventName, items ) {
	  var _this = this;
	  function onComplete() {
	    _this.dispatchEvent( eventName + 'Complete', null, [ items ] );
	  }

	  var count = items.length;
	  if ( !items || !count ) {
	    onComplete();
	    return;
	  }

	  var doneCount = 0;
	  function tick() {
	    doneCount++;
	    if ( doneCount == count ) {
	      onComplete();
	    }
	  }

	  // bind callback
	  items.forEach( function( item ) {
	    item.once( eventName, tick );
	  });
	};

	/**
	 * emits events via EvEmitter and jQuery events
	 * @param {String} type - name of event
	 * @param {Event} event - original event
	 * @param {Array} args - extra arguments
	 */
	proto.dispatchEvent = function( type, event, args ) {
	  // add original event to arguments
	  var emitArgs = event ? [ event ].concat( args ) : args;
	  this.emitEvent( type, emitArgs );

	  if ( jQuery ) {
	    // set this.$element
	    this.$element = this.$element || jQuery( this.element );
	    if ( event ) {
	      // create jQuery event
	      var $event = jQuery.Event( event );
	      $event.type = type;
	      this.$element.trigger( $event, args );
	    } else {
	      // just trigger with type if no event available
	      this.$element.trigger( type, args );
	    }
	  }
	};

	// -------------------------- ignore & stamps -------------------------- //


	/**
	 * keep item in collection, but do not lay it out
	 * ignored items do not get skipped in layout
	 * @param {Element} elem
	 */
	proto.ignore = function( elem ) {
	  var item = this.getItem( elem );
	  if ( item ) {
	    item.isIgnored = true;
	  }
	};

	/**
	 * return item to layout collection
	 * @param {Element} elem
	 */
	proto.unignore = function( elem ) {
	  var item = this.getItem( elem );
	  if ( item ) {
	    delete item.isIgnored;
	  }
	};

	/**
	 * adds elements to stamps
	 * @param {NodeList, Array, Element, or String} elems
	 */
	proto.stamp = function( elems ) {
	  elems = this._find( elems );
	  if ( !elems ) {
	    return;
	  }

	  this.stamps = this.stamps.concat( elems );
	  // ignore
	  elems.forEach( this.ignore, this );
	};

	/**
	 * removes elements to stamps
	 * @param {NodeList, Array, or Element} elems
	 */
	proto.unstamp = function( elems ) {
	  elems = this._find( elems );
	  if ( !elems ){
	    return;
	  }

	  elems.forEach( function( elem ) {
	    // filter out removed stamp elements
	    utils.removeFrom( this.stamps, elem );
	    this.unignore( elem );
	  }, this );
	};

	/**
	 * finds child elements
	 * @param {NodeList, Array, Element, or String} elems
	 * @returns {Array} elems
	 */
	proto._find = function( elems ) {
	  if ( !elems ) {
	    return;
	  }
	  // if string, use argument as selector string
	  if ( typeof elems == 'string' ) {
	    elems = this.element.querySelectorAll( elems );
	  }
	  elems = utils.makeArray( elems );
	  return elems;
	};

	proto._manageStamps = function() {
	  if ( !this.stamps || !this.stamps.length ) {
	    return;
	  }

	  this._getBoundingRect();

	  this.stamps.forEach( this._manageStamp, this );
	};

	// update boundingLeft / Top
	proto._getBoundingRect = function() {
	  // get bounding rect for container element
	  var boundingRect = this.element.getBoundingClientRect();
	  var size = this.size;
	  this._boundingRect = {
	    left: boundingRect.left + size.paddingLeft + size.borderLeftWidth,
	    top: boundingRect.top + size.paddingTop + size.borderTopWidth,
	    right: boundingRect.right - ( size.paddingRight + size.borderRightWidth ),
	    bottom: boundingRect.bottom - ( size.paddingBottom + size.borderBottomWidth )
	  };
	};

	/**
	 * @param {Element} stamp
	**/
	proto._manageStamp = noop;

	/**
	 * get x/y position of element relative to container element
	 * @param {Element} elem
	 * @returns {Object} offset - has left, top, right, bottom
	 */
	proto._getElementOffset = function( elem ) {
	  var boundingRect = elem.getBoundingClientRect();
	  var thisRect = this._boundingRect;
	  var size = getSize( elem );
	  var offset = {
	    left: boundingRect.left - thisRect.left - size.marginLeft,
	    top: boundingRect.top - thisRect.top - size.marginTop,
	    right: thisRect.right - boundingRect.right - size.marginRight,
	    bottom: thisRect.bottom - boundingRect.bottom - size.marginBottom
	  };
	  return offset;
	};

	// -------------------------- resize -------------------------- //

	// enable event handlers for listeners
	// i.e. resize -> onresize
	proto.handleEvent = utils.handleEvent;

	/**
	 * Bind layout to window resizing
	 */
	proto.bindResize = function() {
	  window.addEventListener( 'resize', this );
	  this.isResizeBound = true;
	};

	/**
	 * Unbind layout to window resizing
	 */
	proto.unbindResize = function() {
	  window.removeEventListener( 'resize', this );
	  this.isResizeBound = false;
	};

	proto.onresize = function() {
	  this.resize();
	};

	utils.debounceMethod( Outlayer, 'onresize', 100 );

	proto.resize = function() {
	  // don't trigger if size did not change
	  // or if resize was unbound. See #9
	  if ( !this.isResizeBound || !this.needsResizeLayout() ) {
	    return;
	  }

	  this.layout();
	};

	/**
	 * check if layout is needed post layout
	 * @returns Boolean
	 */
	proto.needsResizeLayout = function() {
	  var size = getSize( this.element );
	  // check that this.size and size are there
	  // IE8 triggers resize on body size change, so they might not be
	  var hasSizes = this.size && size;
	  return hasSizes && size.innerWidth !== this.size.innerWidth;
	};

	// -------------------------- methods -------------------------- //

	/**
	 * add items to Outlayer instance
	 * @param {Array or NodeList or Element} elems
	 * @returns {Array} items - Outlayer.Items
	**/
	proto.addItems = function( elems ) {
	  var items = this._itemize( elems );
	  // add items to collection
	  if ( items.length ) {
	    this.items = this.items.concat( items );
	  }
	  return items;
	};

	/**
	 * Layout newly-appended item elements
	 * @param {Array or NodeList or Element} elems
	 */
	proto.appended = function( elems ) {
	  var items = this.addItems( elems );
	  if ( !items.length ) {
	    return;
	  }
	  // layout and reveal just the new items
	  this.layoutItems( items, true );
	  this.reveal( items );
	};

	/**
	 * Layout prepended elements
	 * @param {Array or NodeList or Element} elems
	 */
	proto.prepended = function( elems ) {
	  var items = this._itemize( elems );
	  if ( !items.length ) {
	    return;
	  }
	  // add items to beginning of collection
	  var previousItems = this.items.slice(0);
	  this.items = items.concat( previousItems );
	  // start new layout
	  this._resetLayout();
	  this._manageStamps();
	  // layout new stuff without transition
	  this.layoutItems( items, true );
	  this.reveal( items );
	  // layout previous items
	  this.layoutItems( previousItems );
	};

	/**
	 * reveal a collection of items
	 * @param {Array of Outlayer.Items} items
	 */
	proto.reveal = function( items ) {
	  this._emitCompleteOnItems( 'reveal', items );
	  if ( !items || !items.length ) {
	    return;
	  }
	  var stagger = this.updateStagger();
	  items.forEach( function( item, i ) {
	    item.stagger( i * stagger );
	    item.reveal();
	  });
	};

	/**
	 * hide a collection of items
	 * @param {Array of Outlayer.Items} items
	 */
	proto.hide = function( items ) {
	  this._emitCompleteOnItems( 'hide', items );
	  if ( !items || !items.length ) {
	    return;
	  }
	  var stagger = this.updateStagger();
	  items.forEach( function( item, i ) {
	    item.stagger( i * stagger );
	    item.hide();
	  });
	};

	/**
	 * reveal item elements
	 * @param {Array}, {Element}, {NodeList} items
	 */
	proto.revealItemElements = function( elems ) {
	  var items = this.getItems( elems );
	  this.reveal( items );
	};

	/**
	 * hide item elements
	 * @param {Array}, {Element}, {NodeList} items
	 */
	proto.hideItemElements = function( elems ) {
	  var items = this.getItems( elems );
	  this.hide( items );
	};

	/**
	 * get Outlayer.Item, given an Element
	 * @param {Element} elem
	 * @param {Function} callback
	 * @returns {Outlayer.Item} item
	 */
	proto.getItem = function( elem ) {
	  // loop through items to get the one that matches
	  for ( var i=0; i < this.items.length; i++ ) {
	    var item = this.items[i];
	    if ( item.element == elem ) {
	      // return item
	      return item;
	    }
	  }
	};

	/**
	 * get collection of Outlayer.Items, given Elements
	 * @param {Array} elems
	 * @returns {Array} items - Outlayer.Items
	 */
	proto.getItems = function( elems ) {
	  elems = utils.makeArray( elems );
	  var items = [];
	  elems.forEach( function( elem ) {
	    var item = this.getItem( elem );
	    if ( item ) {
	      items.push( item );
	    }
	  }, this );

	  return items;
	};

	/**
	 * remove element(s) from instance and DOM
	 * @param {Array or NodeList or Element} elems
	 */
	proto.remove = function( elems ) {
	  var removeItems = this.getItems( elems );

	  this._emitCompleteOnItems( 'remove', removeItems );

	  // bail if no items to remove
	  if ( !removeItems || !removeItems.length ) {
	    return;
	  }

	  removeItems.forEach( function( item ) {
	    item.remove();
	    // remove item from collection
	    utils.removeFrom( this.items, item );
	  }, this );
	};

	// ----- destroy ----- //

	// remove and disable Outlayer instance
	proto.destroy = function() {
	  // clean up dynamic styles
	  var style = this.element.style;
	  style.height = '';
	  style.position = '';
	  style.width = '';
	  // destroy items
	  this.items.forEach( function( item ) {
	    item.destroy();
	  });

	  this.unbindResize();

	  var id = this.element.outlayerGUID;
	  delete instances[ id ]; // remove reference to instance by id
	  delete this.element.outlayerGUID;
	  // remove data for jQuery
	  if ( jQuery ) {
	    jQuery.removeData( this.element, this.constructor.namespace );
	  }

	};

	// -------------------------- data -------------------------- //

	/**
	 * get Outlayer instance from element
	 * @param {Element} elem
	 * @returns {Outlayer}
	 */
	Outlayer.data = function( elem ) {
	  elem = utils.getQueryElement( elem );
	  var id = elem && elem.outlayerGUID;
	  return id && instances[ id ];
	};


	// -------------------------- create Outlayer class -------------------------- //

	/**
	 * create a layout class
	 * @param {String} namespace
	 */
	Outlayer.create = function( namespace, options ) {
	  // sub-class Outlayer
	  var Layout = subclass( Outlayer );
	  // apply new options and compatOptions
	  Layout.defaults = utils.extend( {}, Outlayer.defaults );
	  utils.extend( Layout.defaults, options );
	  Layout.compatOptions = utils.extend( {}, Outlayer.compatOptions  );

	  Layout.namespace = namespace;

	  Layout.data = Outlayer.data;

	  // sub-class Item
	  Layout.Item = subclass( Item );

	  // -------------------------- declarative -------------------------- //

	  utils.htmlInit( Layout, namespace );

	  // -------------------------- jQuery bridge -------------------------- //

	  // make into jQuery plugin
	  if ( jQuery && jQuery.bridget ) {
	    jQuery.bridget( namespace, Layout );
	  }

	  return Layout;
	};

	function subclass( Parent ) {
	  function SubClass() {
	    Parent.apply( this, arguments );
	  }

	  SubClass.prototype = Object.create( Parent.prototype );
	  SubClass.prototype.constructor = SubClass;

	  return SubClass;
	}

	// ----- helpers ----- //

	// how many milliseconds are in each unit
	var msUnits = {
	  ms: 1,
	  s: 1000
	};

	// munge time-like parameter into millisecond number
	// '0.4s' -> 40
	function getMilliseconds( time ) {
	  if ( typeof time == 'number' ) {
	    return time;
	  }
	  var matches = time.match( /(^\d*\.?\d*)(\w*)/ );
	  var num = matches && matches[1];
	  var unit = matches && matches[2];
	  if ( !num.length ) {
	    return 0;
	  }
	  num = parseFloat( num );
	  var mult = msUnits[ unit ] || 1;
	  return num * mult;
	}

	// ----- fin ----- //

	// back in global
	Outlayer.Item = Item;

	return Outlayer;

	}));
	});

	var __moduleExports$26 = createCommonjsModule(function (module) {
	/*!
	 * getSize v2.0.2
	 * measure size of elements
	 * MIT license
	 */

	/*jshint browser: true, strict: true, undef: true, unused: true */
	/*global define: false, module: false, console: false */

	( function( window, factory ) {
	  'use strict';

	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( function() {
	      return factory();
	    });
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS
	    module.exports = factory();
	  } else {
	    // browser global
	    window.getSize = factory();
	  }

	})( window, function factory() {
	'use strict';

	// -------------------------- helpers -------------------------- //

	// get a number from a string, not a percentage
	function getStyleSize( value ) {
	  var num = parseFloat( value );
	  // not a percent like '100%', and a number
	  var isValid = value.indexOf('%') == -1 && !isNaN( num );
	  return isValid && num;
	}

	function noop() {}

	var logError = typeof console == 'undefined' ? noop :
	  function( message ) {
	    console.error( message );
	  };

	// -------------------------- measurements -------------------------- //

	var measurements = [
	  'paddingLeft',
	  'paddingRight',
	  'paddingTop',
	  'paddingBottom',
	  'marginLeft',
	  'marginRight',
	  'marginTop',
	  'marginBottom',
	  'borderLeftWidth',
	  'borderRightWidth',
	  'borderTopWidth',
	  'borderBottomWidth'
	];

	var measurementsLength = measurements.length;

	function getZeroSize() {
	  var size = {
	    width: 0,
	    height: 0,
	    innerWidth: 0,
	    innerHeight: 0,
	    outerWidth: 0,
	    outerHeight: 0
	  };
	  for ( var i=0; i < measurementsLength; i++ ) {
	    var measurement = measurements[i];
	    size[ measurement ] = 0;
	  }
	  return size;
	}

	// -------------------------- getStyle -------------------------- //

	/**
	 * getStyle, get style of element, check for Firefox bug
	 * https://bugzilla.mozilla.org/show_bug.cgi?id=548397
	 */
	function getStyle( elem ) {
	  var style = getComputedStyle( elem );
	  if ( !style ) {
	    logError( 'Style returned ' + style +
	      '. Are you running this code in a hidden iframe on Firefox? ' +
	      'See http://bit.ly/getsizebug1' );
	  }
	  return style;
	}

	// -------------------------- setup -------------------------- //

	var isSetup = false;

	var isBoxSizeOuter;

	/**
	 * setup
	 * check isBoxSizerOuter
	 * do on first getSize() rather than on page load for Firefox bug
	 */
	function setup() {
	  // setup once
	  if ( isSetup ) {
	    return;
	  }
	  isSetup = true;

	  // -------------------------- box sizing -------------------------- //

	  /**
	   * WebKit measures the outer-width on style.width on border-box elems
	   * IE & Firefox<29 measures the inner-width
	   */
	  var div = document.createElement('div');
	  div.style.width = '200px';
	  div.style.padding = '1px 2px 3px 4px';
	  div.style.borderStyle = 'solid';
	  div.style.borderWidth = '1px 2px 3px 4px';
	  div.style.boxSizing = 'border-box';

	  var body = document.body || document.documentElement;
	  body.appendChild( div );
	  var style = getStyle( div );

	  getSize.isBoxSizeOuter = isBoxSizeOuter = getStyleSize( style.width ) == 200;
	  body.removeChild( div );

	}

	// -------------------------- getSize -------------------------- //

	function getSize( elem ) {
	  setup();

	  // use querySeletor if elem is string
	  if ( typeof elem == 'string' ) {
	    elem = document.querySelector( elem );
	  }

	  // do not proceed on non-objects
	  if ( !elem || typeof elem != 'object' || !elem.nodeType ) {
	    return;
	  }

	  var style = getStyle( elem );

	  // if hidden, everything is 0
	  if ( style.display == 'none' ) {
	    return getZeroSize();
	  }

	  var size = {};
	  size.width = elem.offsetWidth;
	  size.height = elem.offsetHeight;

	  var isBorderBox = size.isBorderBox = style.boxSizing == 'border-box';

	  // get all measurements
	  for ( var i=0; i < measurementsLength; i++ ) {
	    var measurement = measurements[i];
	    var value = style[ measurement ];
	    var num = parseFloat( value );
	    // any 'auto', 'medium' value will be 0
	    size[ measurement ] = !isNaN( num ) ? num : 0;
	  }

	  var paddingWidth = size.paddingLeft + size.paddingRight;
	  var paddingHeight = size.paddingTop + size.paddingBottom;
	  var marginWidth = size.marginLeft + size.marginRight;
	  var marginHeight = size.marginTop + size.marginBottom;
	  var borderWidth = size.borderLeftWidth + size.borderRightWidth;
	  var borderHeight = size.borderTopWidth + size.borderBottomWidth;

	  var isBorderBoxSizeOuter = isBorderBox && isBoxSizeOuter;

	  // overwrite width and height if we can get it from style
	  var styleWidth = getStyleSize( style.width );
	  if ( styleWidth !== false ) {
	    size.width = styleWidth +
	      // add padding and border unless it's already including it
	      ( isBorderBoxSizeOuter ? 0 : paddingWidth + borderWidth );
	  }

	  var styleHeight = getStyleSize( style.height );
	  if ( styleHeight !== false ) {
	    size.height = styleHeight +
	      // add padding and border unless it's already including it
	      ( isBorderBoxSizeOuter ? 0 : paddingHeight + borderHeight );
	  }

	  size.innerWidth = size.width - ( paddingWidth + borderWidth );
	  size.innerHeight = size.height - ( paddingHeight + borderHeight );

	  size.outerWidth = size.width + marginWidth;
	  size.outerHeight = size.height + marginHeight;

	  return size;
	}

	return getSize;

	});
	});

	var masonry = createCommonjsModule(function (module) {
	/*!
	 * Masonry v4.1.1
	 * Cascading grid layout library
	 * http://masonry.desandro.com
	 * MIT License
	 * by David DeSandro
	 */

	( function( window, factory ) {
	  // universal module definition
	  /* jshint strict: false */ /*globals define, module, require */
	  if ( typeof define == 'function' && define.amd ) {
	    // AMD
	    define( [
	        'outlayer/outlayer',
	        'get-size/get-size'
	      ],
	      factory );
	  } else if ( typeof module == 'object' && module.exports ) {
	    // CommonJS
	    module.exports = factory(
	      __moduleExports$21,
	      __moduleExports$26
	    );
	  } else {
	    // browser global
	    window.Masonry = factory(
	      window.Outlayer,
	      window.getSize
	    );
	  }

	}( window, function factory( Outlayer, getSize ) {

	'use strict';

	// -------------------------- masonryDefinition -------------------------- //

	  // create an Outlayer layout class
	  var Masonry = Outlayer.create('masonry');
	  // isFitWidth -> fitWidth
	  Masonry.compatOptions.fitWidth = 'isFitWidth';

	  Masonry.prototype._resetLayout = function() {
	    this.getSize();
	    this._getMeasurement( 'columnWidth', 'outerWidth' );
	    this._getMeasurement( 'gutter', 'outerWidth' );
	    this.measureColumns();

	    // reset column Y
	    this.colYs = [];
	    for ( var i=0; i < this.cols; i++ ) {
	      this.colYs.push( 0 );
	    }

	    this.maxY = 0;
	  };

	  Masonry.prototype.measureColumns = function() {
	    this.getContainerWidth();
	    // if columnWidth is 0, default to outerWidth of first item
	    if ( !this.columnWidth ) {
	      var firstItem = this.items[0];
	      var firstItemElem = firstItem && firstItem.element;
	      // columnWidth fall back to item of first element
	      this.columnWidth = firstItemElem && getSize( firstItemElem ).outerWidth ||
	        // if first elem has no width, default to size of container
	        this.containerWidth;
	    }

	    var columnWidth = this.columnWidth += this.gutter;

	    // calculate columns
	    var containerWidth = this.containerWidth + this.gutter;
	    var cols = containerWidth / columnWidth;
	    // fix rounding errors, typically with gutters
	    var excess = columnWidth - containerWidth % columnWidth;
	    // if overshoot is less than a pixel, round up, otherwise floor it
	    var mathMethod = excess && excess < 1 ? 'round' : 'floor';
	    cols = Math[ mathMethod ]( cols );
	    this.cols = Math.max( cols, 1 );
	  };

	  Masonry.prototype.getContainerWidth = function() {
	    // container is parent if fit width
	    var isFitWidth = this._getOption('fitWidth');
	    var container = isFitWidth ? this.element.parentNode : this.element;
	    // check that this.size and size are there
	    // IE8 triggers resize on body size change, so they might not be
	    var size = getSize( container );
	    this.containerWidth = size && size.innerWidth;
	  };

	  Masonry.prototype._getItemLayoutPosition = function( item ) {
	    item.getSize();
	    // how many columns does this brick span
	    var remainder = item.size.outerWidth % this.columnWidth;
	    var mathMethod = remainder && remainder < 1 ? 'round' : 'ceil';
	    // round if off by 1 pixel, otherwise use ceil
	    var colSpan = Math[ mathMethod ]( item.size.outerWidth / this.columnWidth );
	    colSpan = Math.min( colSpan, this.cols );

	    var colGroup = this._getColGroup( colSpan );
	    // get the minimum Y value from the columns
	    var minimumY = Math.min.apply( Math, colGroup );
	    var shortColIndex = colGroup.indexOf( minimumY );

	    // position the brick
	    var position = {
	      x: this.columnWidth * shortColIndex,
	      y: minimumY
	    };

	    // apply setHeight to necessary columns
	    var setHeight = minimumY + item.size.outerHeight;
	    var setSpan = this.cols + 1 - colGroup.length;
	    for ( var i = 0; i < setSpan; i++ ) {
	      this.colYs[ shortColIndex + i ] = setHeight;
	    }

	    return position;
	  };

	  /**
	   * @param {Number} colSpan - number of columns the element spans
	   * @returns {Array} colGroup
	   */
	  Masonry.prototype._getColGroup = function( colSpan ) {
	    if ( colSpan < 2 ) {
	      // if brick spans only one column, use all the column Ys
	      return this.colYs;
	    }

	    var colGroup = [];
	    // how many different places could this brick fit horizontally
	    var groupCount = this.cols + 1 - colSpan;
	    // for each group potential horizontal position
	    for ( var i = 0; i < groupCount; i++ ) {
	      // make an array of colY values for that one group
	      var groupColYs = this.colYs.slice( i, i + colSpan );
	      // and get the max value of the array
	      colGroup[i] = Math.max.apply( Math, groupColYs );
	    }
	    return colGroup;
	  };

	  Masonry.prototype._manageStamp = function( stamp ) {
	    var stampSize = getSize( stamp );
	    var offset = this._getElementOffset( stamp );
	    // get the columns that this stamp affects
	    var isOriginLeft = this._getOption('originLeft');
	    var firstX = isOriginLeft ? offset.left : offset.right;
	    var lastX = firstX + stampSize.outerWidth;
	    var firstCol = Math.floor( firstX / this.columnWidth );
	    firstCol = Math.max( 0, firstCol );
	    var lastCol = Math.floor( lastX / this.columnWidth );
	    // lastCol should not go over if multiple of columnWidth #425
	    lastCol -= lastX % this.columnWidth ? 0 : 1;
	    lastCol = Math.min( this.cols - 1, lastCol );
	    // set colYs to bottom of the stamp

	    var isOriginTop = this._getOption('originTop');
	    var stampMaxY = ( isOriginTop ? offset.top : offset.bottom ) +
	      stampSize.outerHeight;
	    for ( var i = firstCol; i <= lastCol; i++ ) {
	      this.colYs[i] = Math.max( stampMaxY, this.colYs[i] );
	    }
	  };

	  Masonry.prototype._getContainerSize = function() {
	    this.maxY = Math.max.apply( Math, this.colYs );
	    var size = {
	      height: this.maxY
	    };

	    if ( this._getOption('fitWidth') ) {
	      size.width = this._getContainerFitWidth();
	    }

	    return size;
	  };

	  Masonry.prototype._getContainerFitWidth = function() {
	    var unusedCols = 0;
	    // count unused columns
	    var i = this.cols;
	    while ( --i ) {
	      if ( this.colYs[i] !== 0 ) {
	        break;
	      }
	      unusedCols++;
	    }
	    // fit container to columns that have been used
	    return ( this.cols - unusedCols ) * this.columnWidth - this.gutter;
	  };

	  Masonry.prototype.needsResizeLayout = function() {
	    var previousWidth = this.containerWidth;
	    this.getContainerWidth();
	    return previousWidth != this.containerWidth;
	  };

	  return Masonry;

	}));
	});

	var Blog = function () {
	  function Blog() {
	    classCallCheck(this, Blog);

	    this.blogSelector = '[data-article-list]';
	    this.init();
	  }

	  createClass(Blog, [{
	    key: 'init',
	    value: function init() {
	      this.blogMasonry = document.querySelector(this.blogSelector);

	      if (this.blogMasonry) {
	        this._blogMasonry();
	      }
	    }
	  }, {
	    key: 'remove',
	    value: function remove() {
	      if (this.grid) {
	        this.grid.destroy();
	        this.grid = null;
	      }
	    }
	  }, {
	    key: '_blogMasonry',
	    value: function _blogMasonry() {
	      var _this = this;

	      // Set up masonry grid
	      this.grid = new masonry(this.blogMasonry, {
	        itemSelector: '[data-article-item]',
	        columnWidth: '.article-item-grid-sizer',
	        gutter: '.article-item-gutter-sizer',
	        percentPosition: true
	      });

	      var loader = imagesloaded(this.blogSelector);

	      loader.on('progress', function (instance, image) {
	        // Add loaded class to product
	        var $img = $(image.img);
	        var $item = $img.parents('.article-item-image');
	        $item.addClass('article-item-image-loaded');

	        // Re-layout grid
	        _this.grid.layout();
	      });
	    }
	  }]);
	  return Blog;
	}();

	var Search = function () {
	  function Search() {
	    classCallCheck(this, Search);

	    this.$search = $('[data-search-input]');

	    this._init();
	  }

	  createClass(Search, [{
	    key: '_init',
	    value: function _init() {
	      if (this.$search.length) {
	        this.$search.focus();
	      }
	    }
	  }]);
	  return Search;
	}();

	var ShareWidget = function ShareWidget() {
	  var _this = this;

	  classCallCheck(this, ShareWidget);

	  this.$body = $('body');
	  this.shareToggle = '[data-share-widget-toggle]';
	  this.shareWidget = '[data-share-widget]';

	  this.$body.on('click', this.shareToggle, function (event) {
	    event.preventDefault();
	    var $target = $(event.currentTarget);
	    var $widget = $target.next(_this.shareWidget);

	    $target.toggleClass('active');
	    $widget.revealer();
	  });
	};

	var Sidebar = function () {
	  function Sidebar() {
	    classCallCheck(this, Sidebar);

	    this.$body = $(document.body);
	    this.$sidebar = $('.main-sidebar');
	    this.$productSidebar = $('.product-sidebar');
	    this.$overlay = $('.content-overlay');
	    this.currency = window.Theme.currency;
	    this.enableConverter = this.currency.enable && window.Currency;
	    this.$switcher = null;

	    if (this.enableConverter) {
	      this.initCurrencyConverter();
	    }

	    this._bindEvents();
	  }

	  createClass(Sidebar, [{
	    key: 'isExtended',
	    value: function isExtended() {
	      return this.$body.hasClass('sidebar-extended');
	    }
	  }, {
	    key: 'toggleExtended',
	    value: function toggleExtended(callback, toggle) {
	      var state = toggle === undefined ? !this.isExtended() : toggle;

	      if (callback) {
	        var $el = this.$productSidebar.length ? this.$productSidebar : this.$sidebar;

	        $el.one('trend', callback);
	      }

	      this.$body.toggleClass('sidebar-extended', state);
	      this.toggleOverlay(state);
	    }
	  }, {
	    key: 'toggleOverlay',
	    value: function toggleOverlay(toggle) {
	      if (toggle === undefined) {
	        this.$overlay.revealer();
	      } else {
	        this.$overlay.revealer(toggle ? 'show' : 'hide');
	      }

	      this.$body.toggleClass('overlay-visible', toggle);
	    }
	  }, {
	    key: 'initCurrencyConverter',
	    value: function initCurrencyConverter() {
	      this.$converter = $('[data-currency-converter]');

	      index$1.init({
	        switcherSelector: '[data-currency-converter]',
	        priceSelector: '[data-money], span.money',
	        shopCurrency: this.currency.shopCurrency,
	        defaultCurrency: this.currency.defaultCurrency,
	        displayFormat: this.currency.displayFormat,
	        moneyFormat: this.currency.moneyFormat,
	        moneyFormatNoCurrency: this.currency.moneyFormatNoCurrency,
	        moneyFormatCurrency: this.currency.moneyFormatCurrency
	      });

	      this.$converter.on('change.converter', function (event) {
	        index$1.setCurrency(event.currentTarget.value);
	      });
	    }
	  }, {
	    key: 'removeCurrencyConverter',
	    value: function removeCurrencyConverter() {
	      if (this.enableConverter) {
	        this.$converter.off('change.converter');
	      }
	    }
	  }, {
	    key: '_bindEvents',
	    value: function _bindEvents() {
	      // Open/close nav modal
	      this.$body.on('click', '[data-overnav-open]', function (event) {
	        event.preventDefault();
	        $('[data-overnav]').revealer('show');
	        $('[data-overnav-overlay]').revealer('show');
	      });

	      this.$body.on('click', '[data-overnav-close]', function (event) {
	        event.preventDefault();
	        $('[data-overnav]').revealer('hide');
	        $('[data-overnav-overlay]').revealer('hide');
	      });

	      this.$body.on('click', '[data-overnav-overlay]', function (event) {
	        event.preventDefault();
	        $('[data-overnav]').revealer('hide');
	        $('[data-overnav-overlay]').revealer('hide');
	      });

	      // Open/close navigation submenus
	      this.$body.on('click', '.navigation-parent-link', function (event) {
	        event.preventDefault();
	        var $link = $(event.currentTarget);
	        var $submenu = $link.siblings('.navigation-menu');
	        $submenu.slideToggle(200);
	      });
	    }
	  }]);
	  return Sidebar;
	}();

	/**
	 * requestAnimationFrame version: "0.0.23" Copyright (c) 2011-2012, Cyril Agosta ( cyril.agosta.dev@gmail.com) All Rights Reserved.
	 * Available via the MIT license.
	 * see: http://github.com/cagosta/requestAnimationFrame for details
	 *
	 * http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	 * http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
	 * requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
	 * MIT license
	 *
	 */


	( function( global ) {


	    ( function() {


	        if ( global.requestAnimationFrame ) {

	            return;

	        }

	        if ( global.webkitRequestAnimationFrame ) { // Chrome <= 23, Safari <= 6.1, Blackberry 10

	            global.requestAnimationFrame = global[ 'webkitRequestAnimationFrame' ];
	            global.cancelAnimationFrame = global[ 'webkitCancelAnimationFrame' ] || global[ 'webkitCancelRequestAnimationFrame' ];
	            return;

	        }

	        // IE <= 9, Android <= 4.3, very old/rare browsers

	        var lastTime = 0;

	        global.requestAnimationFrame = function( callback ) {

	            var currTime = new Date().getTime();

	            var timeToCall = Math.max( 0, 16 - ( currTime - lastTime ) );

	            var id = global.setTimeout( function() {

	                callback( currTime + timeToCall );

	            }, timeToCall );

	            lastTime = currTime + timeToCall;

	            return id; // return the id for cancellation capabilities

	        };

	        global.cancelAnimationFrame = function( id ) {

	            clearTimeout( id );

	        };

	    } )();

	    if ( typeof define === 'function' ) {

	        define( function() {

	            return global.requestAnimationFrame;

	        } );

	    }

	} )( window );

	var selectors = [
		'iframe[src*="player.vimeo.com"]',
		'iframe[src*="youtube.com"]',
		'iframe[src*="youtube-nocookie.com"]',
		'iframe[src*="kickstarter.com"][src*="video.html"]',
		'object'
	]

	var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}'

	var index$3 = function (parentSelector, opts) {
		parentSelector = parentSelector || 'body'
		opts = opts || {}

		if (isObject(parentSelector)) {
			opts = parentSelector
			parentSelector = 'body'
		}

		opts.ignore = opts.ignore || ''
		opts.players = opts.players || ''

		var containers = queryAll(parentSelector)
		if (!hasLength(containers)) return

		if (!document.getElementById('fit-vids-style')) {
			var head = document.head || document.getElementsByTagName('head')[0]
			head.appendChild(styles())
		}

		var custom = toSelectorArray(opts.players) || []
		var ignored = toSelectorArray(opts.ignore) || []
		var selector = selectors
			.filter(notIgnored(ignored))
			.concat(custom)
			.join()

		if (!hasLength(selector)) return

		containers.forEach(function (container) {
			var videos = queryAll(container, selector)
			videos.forEach(function (video) {
				wrap(video)
			})
		})
	}

	function queryAll (el, selector) {
		if (typeof el === 'string') {
			selector = el
			el = document
		}
		return Array.prototype.slice.call(el.querySelectorAll(selector))
	}

	function toSelectorArray (input) {
		if (typeof input === 'string') {
			return input.split(',').map(trim).filter(hasLength)
		} else if (isArray(input)) {
			return flatten(input.map(toSelectorArray).filter(hasLength))
		}
		return input || []
	}

	function wrap (el) {
		if (/fluid-width-video-wrapper/.test(el.parentNode.className)) return

		var widthAttr = parseInt(el.getAttribute('width'), 10)
		var heightAttr = parseInt(el.getAttribute('height'), 10)

		var width = !isNaN(widthAttr) ? widthAttr : el.clientWidth
		var height = !isNaN(heightAttr) ? heightAttr : el.clientHeight
		var aspect = height / width

		el.removeAttribute('width')
		el.removeAttribute('height')

		var wrapper = document.createElement('div')
		el.parentNode.insertBefore(wrapper, el)
		wrapper.className = 'fluid-width-video-wrapper'
		wrapper.style.paddingTop = (aspect * 100) + '%'
		wrapper.appendChild(el)
	}

	function styles () {
		var div = document.createElement('div')
		div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>'
		return div.childNodes[1]
	}

	function notIgnored (ignored) {
		if (ignored.length < 1) {
			return function () {
				return true
			}
		}
		return function (selector) {
			return ignored.indexOf(selector) === -1
		}
	}

	function hasLength (input) {
		return input.length > 0
	}

	function trim (str) {
		return str.replace(/^\s+|\s+$/g, '')
	}

	function flatten (input) {
		return [].concat.apply([], input)
	}

	function isObject (input) {
		return Object.prototype.toString.call(input) === '[object Object]'
	}

	function isArray (input) {
		return Object.prototype.toString.call(input) === '[object Array]'
	}

	var Product = function () {
	  function Product(theme) {
	    classCallCheck(this, Product);

	    this.theme = theme;
	    this.init();
	  }

	  createClass(Product, [{
	    key: 'init',
	    value: function init() {
	      this.$body = $(document.body);
	      this.$window = $(window);
	      this.$document = $(document);
	      this.$productContainer = $('[data-product-list]');

	      this.cachedProductSelector = 'script[type="application/vnd.vogue.product+html"]';
	      this.extendedOpenSelector = '[data-product-extended-open]';
	      this.extendedCloseSelector = '[data-product-extended-close]';
	      this.extendedMobileSelector = '[data-product-extended-mobile]';
	      this.productSelector = '[data-product-id]';
	      this.productHeaderSelector = '.product-details-header';
	      this.products = [];

	      // Sidebar settings
	      this.hasSidebar = window.Theme.hasSidebar;
	      this.gridSpacing = parseInt(window.Theme.gridSpacing, 10) || 0;
	      this.sidebarWidth = 300;
	      this.enableHistory = true;

	      if (window.Shopify && window.Shopify.preview_host) {
	        this.enableHistory = false;
	      }

	      if (this.$productContainer.length) {
	        this._bindEvents();
	        this._addProducts(this.productSelector, false);
	        this._updateProductPositions();
	        this._runScrollEffect();
	        this._initProductOptions();
	        this.PriceUpdater = new PriceUpdater();
	        this._fetchProducts(1);
	      }
	    }
	  }, {
	    key: 'remove',
	    value: function remove() {
	      if (this.$extendedProduct.length) {
	        this._closeExtendedDetails(this.$extendedProduct);
	      }

	      this.$body.off('.vogue-product');
	      this.$window.off('.vogue-product');
	      this.cancelScrollEffect = true;
	    }

	    /**
	     * Load products from our in-page cache.
	     */

	  }, {
	    key: '_fetchProducts',
	    value: function _fetchProducts(limit) {
	      var _this = this;

	      $(this.cachedProductSelector).slice(0, limit).each(function (index, el) {
	        return _this._addProducts(el.innerHTML);
	      }).remove();
	    }

	    /**
	     * Register products with our scroll effects handler.
	     */

	  }, {
	    key: '_addProducts',
	    value: function _addProducts(products) {
	      var _this2 = this;

	      var addToPage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	      $(products).each(function (index, el) {
	        var $el = $(el);

	        // Add to the page
	        if (addToPage) {
	          _this2.$productContainer.find('.product:last').after($el);
	        }

	        // Cache elements so we can access them quickly in the scroll handler
	        _this2.products.push({
	          $body: $el.find('.product-body'),
	          $details: $el.find('.product-details'),
	          $gallery: $el.find('.product-gallery'),
	          $mobileHeader: $el.find('.product-details-header-mobile')
	        });
	      });

	      // Make any videos responsive
	      index$3('.product-description-full.rte');
	    }
	  }, {
	    key: '_bindEvents',
	    value: function _bindEvents() {
	      var _this3 = this;

	      // Load in new products as we scroll down the page
	      this.$window.on('scroll.vogue-product', function (event) {
	        if (_this3._isMobile()) return;

	        var percent = _this3.$window.scrollTop() / (_this3.$document.height() - _this3.$window.height());

	        if (percent < 0.6) return;
	        _this3._fetchProducts(4);
	      });

	      // Scroll down to the product details on mobile
	      this.$body.on('click.vogue-product', this.extendedMobileSelector, function (event) {
	        event.preventDefault();
	        var $target = $(event.currentTarget);
	        var $el = $target.parents(_this3.productSelector).find('.product-details');
	        _this3._ensureElementInViewport($el[0]);
	      });

	      // Open the extended details panel
	      this.$body.on('click.vogue-product', this.extendedOpenSelector, function (event) {
	        event.preventDefault();
	        var $target = $(event.currentTarget);
	        _this3.$extendedProduct = $target.parents(_this3.productSelector);
	        _this3._openExtendedDetails(_this3.$extendedProduct);
	      });

	      // Close the extended details panel
	      this.$body.on('click.vogue-product', this.extendedCloseSelector, function (event) {
	        event.preventDefault();
	        _this3._closeExtendedDetails(_this3.$extendedProduct);
	      });

	      // Close the extended details panel when resizing down to mobile
	      this.$window.on('resize.vogue-product', function (event) {
	        if (_this3.$extendedProduct && _this3._isMobile()) {
	          _this3._closeExtendedDetails(_this3.$extendedProduct);
	          _this3.theme.sidebar.toggleExtended(null, false);
	        }
	      });
	    }

	    /**
	     * Show the extended details for a product.
	     *
	     * @param {jQuery} $product
	     */

	  }, {
	    key: '_openExtendedDetails',
	    value: function _openExtendedDetails($product) {
	      var _this4 = this;

	      // Scroll product into view
	      var gallery = $product.find('.product-gallery')[0];
	      this._ensureElementInViewport(gallery);

	      // Animate open the extended details
	      $product.find('.product-description-excerpt').revealer('show');

	      $product.find('.product-options').slideDown();

	      this.theme.sidebar.toggleExtended(function () {
	        $product.addClass('product-details-extended').find('.product-description-full').revealer('show');

	        $product.find('.product-related').revealer('show');

	        // Create slideshow
	        _this4._createProductSlideshow($product);
	      }, true);

	      // Resizing the window when a product is extended causes problems
	      this.$window.on('resize.vogue-extended-resize', function (event) {
	        _this4._ensureElementInViewport(gallery, false);
	      });
	    }

	    /**
	     * Hide the extend details for a product.
	     *
	     * @param {jQuery} $product
	     */

	  }, {
	    key: '_closeExtendedDetails',
	    value: function _closeExtendedDetails($product) {
	      var _this5 = this;

	      // Remove the slideshow
	      this._destroyProductSlideshow();

	      $product.find('.product-options').slideUp();

	      $product.find('.product-related').revealer('hide');

	      // Animate out the extended details
	      $product.find('.product-description-full').revealer('hide').one('revealer-hide', function (event) {
	        $product.removeClass('product-details-extended').find('.product-description-excerpt').revealer('hide');

	        _this5.theme.sidebar.toggleExtended(null, false);
	      });

	      // Remove the resize handler--it's only needed when the details are extended
	      this.$window.off('resize.vogue-extended-resize');
	    }

	    /**
	     * Create a slideshow for the product images when the extended
	     * details are opened.
	     */

	  }, {
	    key: '_createProductSlideshow',
	    value: function _createProductSlideshow($product) {
	      var $gallery = $('.product-gallery', $product);
	      var $slideshow = $('<div>').addClass('product-slideshow').revealer('hide', true);

	      $gallery.find('.product-image img').each(function (index, el) {
	        $('<div>').addClass('product-slideshow-image').css('background-image', 'url(' + $(el).attr('src') + ')').appendTo($slideshow);
	      });

	      this.$body.append($slideshow);

	      this.slideshow = new index$2($slideshow[0], {
	        bgLazyLoad: 2,
	        pageDots: false,
	        prevNextButtons: $gallery.find('.product-image').length > 1,
	        arrowShape: svgPath('icon-flickity-arrow')
	      });

	      // Select first available variant image
	      var variant = this._getSelectedProductVariant($product);

	      if (variant.featured_image) {
	        this.slideshow.select(variant.featured_image.position - 1);
	      }

	      // Show the slideshow
	      $slideshow.revealer('show');
	    }

	    /**
	     * Remove the current product slideshow.
	     */

	  }, {
	    key: '_destroyProductSlideshow',
	    value: function _destroyProductSlideshow() {
	      var $slideshow = $('.product-slideshow');

	      $slideshow.revealer('hide').one('revealer-hide', function (event) {
	        $slideshow.remove();
	      });

	      this.slideshow = null;
	    }

	    /**
	     * Scroll to a position where the element is completely visible.
	     *
	     * @param {Element} el
	     */

	  }, {
	    key: '_ensureElementInViewport',
	    value: function _ensureElementInViewport(el) {
	      var animate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	      var productRect = el.getBoundingClientRect();
	      var offsetTop = Math.ceil(productRect.top);
	      var offsetBottom = Math.ceil(window.innerHeight - productRect.bottom);
	      var duration = animate ? 300 : 0;

	      if (offsetTop > 0) {
	        $('html, body').animate({ scrollTop: '+=' + offsetTop + 'px' }, duration);
	      } else if (offsetBottom > 0) {
	        $('html, body').animate({ scrollTop: '-=' + offsetBottom + 'px' }, duration);
	      }
	    }

	    /**
	     * As we scroll down the page, animate in the next product details sidebar.
	     */

	  }, {
	    key: '_runScrollEffect',
	    value: function _runScrollEffect() {
	      var _this6 = this;

	      if (this.cancelScrollEffect) {
	        this.cancelScrollEffect = false;
	        return;
	      }

	      window.requestAnimationFrame(function () {
	        _this6._updateProductPositions();
	        _this6._runScrollEffect();
	      });
	    }
	  }, {
	    key: '_isMobile',
	    value: function _isMobile() {
	      return window.innerWidth < 720;
	    }
	  }, {
	    key: '_updateProductPositions',
	    value: function _updateProductPositions() {
	      var _this7 = this;

	      var screenHeight = window.innerHeight;
	      var screenHeightHalf = screenHeight / 2;
	      var $header = $('[data-main-sidebar-header]');
	      var headerHeight = this.hasSidebar ? $header.outerHeight() : 0;
	      var height = screenHeight - headerHeight - this.gridSpacing * 2;
	      var sidebarOffset = this._isMobile() ? 0 : this.sidebarWidth;

	      // Activate the grid mask if needed (see _sidebar.scss)
	      $header.toggleClass('needs-grid-mask', window.scrollY > this.gridSpacing);

	      // Position products
	      this.products.forEach(function (product) {
	        var detailsHeight = product.$details.height();
	        var productRect = product.$body[0].getBoundingClientRect();
	        var pinDetailsToTop = detailsHeight + headerHeight > product.$gallery.height();

	        // Detect which product we are on, and update the URL to match
	        if (productRect.top < screenHeightHalf && productRect.bottom > screenHeightHalf) {
	          _this7._setHistory(product);
	        }

	        // Mobile positions
	        if (_this7._isMobile()) {
	          var mobileHeaderHeight = product.$mobileHeader.data('height');
	          var galleryRect = product.$gallery[0].getBoundingClientRect();
	          var isFloating = galleryRect.bottom - screenHeight + mobileHeaderHeight > 0;
	          var isInFrame = galleryRect.top - screenHeight < 0;

	          // Reset desktop positions
	          product.$details.css({
	            position: 'relative',
	            top: 'auto',
	            left: 'auto',
	            bottom: 'auto',
	            maxHeight: 'none'
	          });

	          // Position mobile header
	          product.$mobileHeader.toggleClass('is-floating', isFloating);

	          product.$mobileHeader.css({
	            position: isFloating ? 'fixed' : 'static',
	            right: _this7.gridSpacing,
	            bottom: 0,
	            left: _this7.gridSpacing,
	            display: isInFrame ? '' : 'none'
	          });
	        }

	        // Pin to bottom of the product
	        else if (!pinDetailsToTop && productRect.bottom - detailsHeight - _this7.gridSpacing * 2 - headerHeight < 0) {
	            product.$details.css({
	              position: 'absolute',
	              top: 'auto',
	              left: -sidebarOffset,
	              bottom: _this7.gridSpacing,
	              maxHeight: height
	            });
	          }

	          // Pin to the top of the viewport
	          else if (!pinDetailsToTop && productRect.top - headerHeight <= 0) {
	              product.$details.css({
	                position: 'fixed',
	                top: headerHeight + _this7.gridSpacing,
	                left: _this7.gridSpacing,
	                bottom: 'auto',
	                maxHeight: height
	              });
	            }

	            // Pin to the top of the product
	            else {
	                var topOffset = pinDetailsToTop ? Math.min(headerHeight, Math.max(0, headerHeight - productRect.top)) : 0;

	                product.$details.css({
	                  position: 'absolute',
	                  top: _this7.gridSpacing + topOffset,
	                  left: -sidebarOffset,
	                  bottom: 'auto',
	                  maxHeight: height
	                });
	              }
	      });
	    }

	    /**
	     * Update the browser's URL to match a specific product URL.
	     */

	  }, {
	    key: '_setHistory',
	    value: function _setHistory(product) {
	      if (!this.enableHistory || !window.history || !window.history.replaceState) {
	        return;
	      }

	      var productUrl = product.$details.find('meta[itemprop=url]').attr('content');

	      if (window.location.href !== productUrl) {
	        // Make sure an exception doesn't crash the whole page
	        // For example, Safari throws a `SecurityError` if the URL is updated too often
	        try {
	          window.history.replaceState({}, '', productUrl);
	        } catch (error) {}
	      }
	    }

	    /**
	     * Create our custom product option handler.
	     */

	  }, {
	    key: '_initProductOptions',
	    value: function _initProductOptions() {
	      var _this8 = this;

	      this.$body.on('change', '[data-option-input]', function (event) {
	        var $input = $(event.currentTarget);
	        var $product = $input.parents('.product-details');

	        // Update the selected variant ID
	        var variant = _this8._getSelectedProductVariant($product);
	        _this8._updateProductVariant($product, variant);
	      });
	    }
	  }, {
	    key: '_updateProductVariant',
	    value: function _updateProductVariant($product, variant) {
	      // Mark unavailable variants
	      $product.toggleClass('product-variant-unavailable', variant.id === null);
	      $product.toggleClass('product-variant-soldout', variant.id !== null && !variant.available);

	      // Update our select element, which is the source of truth for
	      // the currently selected variant
	      $product.find('[data-product-variants]').val(variant.id);

	      // Update price
	      var $priceOriginal = $product.find('[data-price-original]');
	      this.PriceUpdater.updatePrice($priceOriginal, variant.price);

	      // Update compare price
	      var $priceCompare = $product.find('[data-price-compare]');
	      this.PriceUpdater.updatePrice($priceCompare, variant.compare_at_price);
	      $priceCompare.toggleClass('product-price-has-compare', variant.compare_at_price !== null);

	      // Select slideshow slide
	      if (variant.featured_image && this.slideshow) {
	        this.slideshow.select(variant.featured_image.position - 1);
	      }
	    }

	    /**
	     * Get the selected variant details for a specific product.
	     *
	     * @param {jQuery} $product
	     * @return {Object} A variant object.
	     */

	  }, {
	    key: '_getSelectedProductVariant',
	    value: function _getSelectedProductVariant($product) {
	      var $inputs = $product.find('[data-option-input]');
	      var $select = $product.find('[data-product-variants]');
	      var product = $select.data('product');

	      // Grab the list of selected options
	      var options = {};

	      $inputs.filter(':checked').each(function (index, el) {
	        var $input = $(el);
	        var id = $input.data('option-index');
	        options['option' + id] = $input.val();
	      });

	      // Grab the variant
	      return this._getVariantFromOptions(product, options) || {
	        id: null,
	        price: 0,
	        compare_at_price: null,
	        available: false
	      };
	    }

	    /**
	     * Get variant details for a specific set of options.
	     *
	     * @param {Object} product
	     *        A product, as made available from the liquid template.
	     *
	     * @param {Object} options
	     *        A map of option keys to values. One of: option1, option2, option3.
	     *
	     * @return {Object} A variant object, or `null` if none found.
	     */

	  }, {
	    key: '_getVariantFromOptions',
	    value: function _getVariantFromOptions(product, options) {
	      if (!product) return;
	      if (!product.variants) return;

	      for (var i = 0; i < product.variants.length; i++) {
	        var variant = product.variants[i];
	        var isMatch = true;

	        for (var optionName in options) {
	          if (variant[optionName] !== options[optionName]) {
	            isMatch = false;
	            break;
	          }
	        }

	        if (isMatch) return variant;
	      }
	    }
	  }]);
	  return Product;
	}();

	var TagList = function () {
	  function TagList() {
	    classCallCheck(this, TagList);

	    this.init();
	  }

	  createClass(TagList, [{
	    key: 'init',
	    value: function init() {
	      this.$tags = $('.section-tags');
	      this.$tagList = $('.section-tag-list');

	      if (this.$tagList.length) {
	        this._bindEvents();
	        this._centerSelectedTags();
	        this._addScrollClasses();
	      }
	    }
	  }, {
	    key: '_bindEvents',
	    value: function _bindEvents() {
	      var _this = this;

	      this.$tagList.on('scroll', function (event) {
	        return _this._addScrollClasses();
	      });
	      $(window).on('resize', function (event) {
	        return _this._addScrollClasses();
	      });
	    }
	  }, {
	    key: '_centerSelectedTags',
	    value: function _centerSelectedTags() {
	      var $activeTag = this.$tags.find('.section-tag-active:first');
	      if (!$activeTag.length) return;

	      var activePosition = $activeTag.position().left;
	      var targetPosition = window.innerWidth / 2;
	      var tagOffset = $activeTag.width() / 2;
	      var delta = activePosition - targetPosition + tagOffset;
	      this.$tagList.scrollLeft(delta);
	    }
	  }, {
	    key: '_addScrollClasses',
	    value: function _addScrollClasses() {
	      var scrollLeft = this.$tagList.scrollLeft();
	      var scrollWidth = this.$tagList[0].scrollWidth;
	      var width = this.$tagList.width();

	      this.$tags.toggleClass('section-tags-overflow-left', scrollLeft > 0);
	      this.$tags.toggleClass('section-tags-overflow-right', scrollLeft < scrollWidth - width);
	    }
	  }]);
	  return TagList;
	}();

	var RTE = function RTE() {
	  classCallCheck(this, RTE);

	  index$3('.rte');
	};

	/*!
	 * Trend 0.2.0
	 *
	 * Fail-safe TransitionEnd event for jQuery.
	 *
	 * Adds a new "trend" event that can be used in browsers that don't
	 * support "transitionend".
	 *
	 * NOTE: Only supports being bound with "jQuery.one".
	 *
	 * Copyright 2014, Pixel Union - http://pixelunion.net
	 * Released under the MIT license
	 */
	;(function($){

	  // Prefixed transitionend event names
	  var transitionEndEvents =
	    "webkitTransitionEnd " +
	    "otransitionend " +
	    "oTransitionEnd " +
	    "msTransitionEnd " +
	    "transitionend";

	  // Prefixed transition duration property names
	  var transitionDurationProperties = [
	    "transition-duration",
	    "-moz-transition-duration",
	    "-webkit-transition-duration",
	    "-ms-transition-duration",
	    "-o-transition-duration",
	    "-khtml-transition-duration"
	  ];

	  // Prefixed transition delay property names
	  var transitionDelayProperties = [
	    "transition-delay",
	    "-moz-transition-delay",
	    "-webkit-transition-delay",
	    "-ms-transition-delay",
	    "-o-transition-delay",
	    "-khtml-transition-delay"
	  ];

	  // Parses a CSS time value into milliseconds.
	  var parseTime = function(s) {
	    s = s.replace(/\s/, "");
	    var v = window.parseFloat(s);

	    return s.match(/[^m]s$/i)
	      ? v * 1000
	      : v;
	  };

	  // Parses the longest time unit found in a series of CSS properties.
	  // Returns a value in milliseconds.
	  var parseProperties = function(el, properties) {
	    var duration = 0;

	    for (var i = 0; i < properties.length; i++) {
	      // Get raw CSS value
	      var value = el.css(properties[i]);
	      if (!value) continue;

	      // Multiple transitions--pick the longest
	      if (value.indexOf(",") !== -1) {
	        var values = value.split(",");
	        var durations = (function(){
	          var results = [];
	          for (var i = 0; i < values.length; i++) {
	            var duration = parseTime(values[i]);
	            results.push(duration);
	          }
	          return results;
	        })();

	        duration = Math.max.apply(Math, durations);
	      }

	      // Single transition
	      else {
	        duration = parseTime(value);
	      }

	      // Accept first vaue
	      break;
	    }

	    return duration;
	  };

	  $.event.special.trend = {
	    // Triggers an event handler when an element is done transitioning.
	    //
	    // Handles browsers that don't support transitionend by adding a
	    // timeout with the transition duration.
	    add: function(handleObj) {
	      var el = $(this);
	      var fired = false;

	      // Mark element as being in transition
	      el.data("trend", true);

	      // Calculate a fallback duration. + 20 because some browsers fire
	      // timeouts faster than transitionend.
	      var time =
	        parseProperties(el, transitionDurationProperties) +
	        parseProperties(el, transitionDelayProperties) +
	        20;

	      var cb = function(e) {
	        // transitionend events can be sent for each property. Let's just
	        // skip all but the first. Also handles the timeout callback.
	        if (fired) return;

	        // Child elements that also have transitions can be fired before we
	        // complete. This will catch and ignore those. Unfortunately, we'll
	        // have to rely on the timeout in these cases.
	        if (e && e.srcElement !== el[0]) return;

	        // Mark element has not being in transition
	        el.data("trend", false);

	        // Callback
	        fired = true;
	        if (handleObj.handler) handleObj.handler();
	      };

	      el.one(transitionEndEvents, cb);
	      el.data("trend-timeout", window.setTimeout(cb, time));
	    },

	    remove: function(handleObj) {
	      var el = $(this);
	      el.off(transitionEndEvents);
	      window.clearTimeout(el.data("trend-timeout"));
	    }
	  };

	})(jQuery);

	/*!
	 * Revealer 2.0.0
	 *
	 * Copyright 2015, Pixel Union - http://pixelunion.net
	 * Released under the MIT license
	 */
	(function($){
	  // check for trend event (make sure jquery.trend is included)
	  if (typeof $.event.special.trend !== "object") {
	    console.warn("Please make sure jquery.trend is included! Otherwise revealer won't work.");
	  }

	  // Simple requestAnimationFrame polyfill
	  var raf = window.requestAnimationFrame ||
	    window.mozRequestAnimationFrame ||
	    window.webkitRequestAnimationFrame ||
	    function(fn) { window.setTimeout(fn, 1000/60); }


	  // Public API
	  var methods = {
	    isVisible: function(el) {
	      return !!el.data("revealer-visible"); 
	    },

	    show: function(el, force) {
	      // Check state
	      if (methods.isVisible(el)) {
	        el.removeClass("animating animating-in");
	        el.off("revealer-animating revealer-show");
	        return;
	      }

	      // Remove previous event listeners
	      el.data("revealer-visible", true);
	      el.off("trend");

	      if (force) {
	        el.addClass("visible");
	        el.trigger("revealer-show");
	        return;
	      }

	      raf(function(){
	        // Start animation state transition
	        el.addClass("animating animating-in");
	        el.trigger("revealer-animating");

	        raf(function(){
	          el.addClass("visible");

	          el.one("trend", function(){
	            el.removeClass("animating animating-in");
	            el.trigger("revealer-show");
	          });
	        });
	      });
	    },

	    hide: function(el, force) {
	      // Check state
	      if (!methods.isVisible(el)) {
	        el.removeClass("animating animating-out visible");
	        el.off("revealer-animating revealer-hide");
	        return;
	      }

	      // Remove previous event listeners
	      el.data("revealer-visible", false);
	      el.off("trend");

	      if (force) {
	        el.removeClass("visible");
	        el.trigger("revealer-hide");
	        return;
	      }

	      raf(function(){
	        el.addClass("animating animating-out");
	        el.trigger("revealer-animating");

	        raf(function(){
	          el.removeClass("visible");

	          el.one("trend", function(){
	            el.removeClass("animating animating-in animating-out");
	            el.trigger("revealer-hide");
	          });
	        });
	      });
	    },

	    toggle: function(el, force) {
	      if (methods.isVisible(el)) {
	        methods.hide(el, force);
	      } else {
	        methods.show(el, force);
	      }
	    }
	  };

	  // jQuery plugin
	  $.fn.revealer = function(method, force) {
	    // Get action
	    var action = methods[method || "toggle"];
	    if (!action) return this;

	    // Run action
	    if (method === "isVisible") {
	      return action(this);
	    }

	    return this.each(function(){
	      action($(this), force);
	    });
	  };
	})(jQuery);

	var Vogue = function () {
	  function Vogue() {
	    classCallCheck(this, Vogue);

	    this._init();
	  }

	  createClass(Vogue, [{
	    key: '_init',
	    value: function _init() {
	      // Components
	      this.shareWidget = new ShareWidget();
	      this.tagList = new TagList();
	      this.sidebar = new Sidebar();
	      this.rte = new RTE();

	      // Page Templates
	      this.home = new Home();
	      this.cart = new Cart();
	      this.lookbook = new Lookbook();
	      this.product = new Product(this);
	      this.account = new Account();
	      this.blog = new Blog();
	      this.search = new Search();

	      // Theme Editor interactions
	      this.themeEditor = new _class(this);
	    }
	  }]);
	  return Vogue;
	}();

	new Vogue();

	return Vogue;

}());

//# sourceMappingURL=vogue.js.map
