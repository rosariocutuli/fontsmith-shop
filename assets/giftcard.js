var GiftCard = (function () {
  'use strict';

  var classCallCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

  var createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var GiftCard = function () {
    function GiftCard() {
      classCallCheck(this, GiftCard);

      this.$printButton = $('[data-giftcard-print]');
      this.QRCodeWrapper = document.querySelector('[data-qr-code]');
      this.giftCardCode = document.querySelector('[data-select-code]');

      if (window.QRCode && this.QRCodeWrapper) {
        this._addQRCode();
      }

      if (this.giftCardCode && this.$printButton.length) {
        this._bindEvents();
      }
    }

    createClass(GiftCard, [{
      key: '_bindEvents',
      value: function _bindEvents() {
        var _this = this;

        this.giftCardCode.addEventListener('touchstart', function (event) {
          _this._selectCode(event.currentTarget);
        });

        this.giftCardCode.addEventListener('click', function (event) {
          _this._selectCode(event.currentTarget);
        });

        this.$printButton.on('click', function (event) {
          event.preventDefault();
          window.print();
        });
      }
    }, {
      key: '_selectCode',
      value: function _selectCode(target) {
        if (document.body.createTextRange) {
          // Microsoft
          var range = document.body.createTextRange();
          range.moveToElementText(target);
          range.select();
        } else if (window.getSelection) {
          // Mozilla, Opera, Webkit
          var selection = window.getSelection();
          var _range = document.createRange();

          _range.selectNodeContents(target);
          selection.removeAllRanges();
          selection.addRange(_range);
        }
      }
    }, {
      key: '_addQRCode',
      value: function _addQRCode() {
        new QRCode(this.QRCodeWrapper, {
          text: this.QRCodeWrapper.getAttribute('data-qr-code'),
          width: 120,
          height: 120
        });
      }
    }]);
    return GiftCard;
  }();

  return GiftCard;

}());

//# sourceMappingURL=giftcard.js.map
